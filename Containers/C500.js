import React, { Component } from 'react'
import { Text, View } from 'react-native-web'
import { connect } from 'react-redux'
import styles from './Styles/ErrorStyles'
import { H1 } from '../Components/Html'

class C500 extends Component {
  render () {
    return (
      <View style={styles.container} >
        <H1>404 Not Found</H1>
      </View>
    )
  }
}

C500.propTypes = {}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(C500)
