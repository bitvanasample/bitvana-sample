// base
import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { I18nextProvider } from 'react-i18next'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

// Datepicker
import MuiPickersUtilsProvider from
  'material-ui-pickers/utils/MuiPickersUtilsProvider'
import MomentUtils from 'material-ui-pickers/utils/moment-utils'
import moment from 'moment'
// aws
// import { BatchHttpLink } from 'apollo-link-batch-http'
import AWSAppSyncClient, { createAppSyncLink } from 'aws-appsync'
import { Rehydrated } from 'aws-appsync-react'
import { AUTH_TYPE } from 'aws-appsync/lib/link/auth-link'
import { ApolloProvider } from 'react-apollo'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { persistCache } from 'apollo-cache-persist'
// import { ApolloLink } from 'apollo-link'
// import * as AWS from 'aws-sdk'
import Amplify, { Auth } from 'aws-amplify'
import ReactGA from 'react-ga'


// base
import i18n from '../I18n/I18n'
import '../Config'
import RootContainer from '../Containers/RootContainer'
import createStore from '../Redux'
import styles from './Styles/App'

// aws
import AppSync from '../Config/AppSync'
// import awsExports from '../../aws-exports'
import { amplifyConfigure } from '../Config/AppConfig'


ReactGA.initialize('UA-123756112-1')
ReactGA.pageview(window.location.pathname + window.location.search)

Amplify.configure(amplifyConfigure)

// window.LOG_LEVEL = 'DEBUG'

let auth
if (process.env.REACT_APP_AWS_AUTH === 'AMAZON_COGNITO_USER_POOLS') {
  auth = {
    type: AUTH_TYPE.AMAZON_COGNITO_USER_POOLS,
    credentials: () => Auth.currentCredentials(),
    jwtToken: async () =>
      (await Auth.currentSession()).getAccessToken().getJwtToken(),
  }
} else {
  auth = {
    type: AUTH_TYPE.AWS_IAM,
    credentials: () => Auth.currentCredentials(),
  }
}

const graphqlOptions = {
  url: AppSync.graphqlEndpoint,
  region: AppSync.region,
  auth,
  disableOffline: true,
}

const cache = new InMemoryCache()

persistCache({
  cache,
  storage: window.localStorage,
})

const link = createAppSyncLink(
  {
    ...graphqlOptions,
  // resultsFetcherLink: new BatchHttpLink({
  //   uri: AppSync.graphqlEndpoint,
  //   // fetch: (uri, options) => {
  //   //   console.tron.log(uri)
  //   //   console.tron.log(options)
  //   //   return fetch(uri, options)
  //   // },
  // }),
  },
  { cache },
)

export const client = new AWSAppSyncClient(graphqlOptions, {
  link,
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'cache-and-network',
    },
  },
  cache,
})

const store = createStore()

class App extends Component {
  shouldComponentUpdate () {
    return false
  }
  render () {
    return (
      <ApolloProvider client={client}>
        <Rehydrated>
          <Provider store={store}>
            <MuiPickersUtilsProvider moment={moment} utils={MomentUtils}>
              <I18nextProvider i18n={i18n}>
                <MuiThemeProvider
                  theme={createMuiTheme(styles.muiTheme)}
                >
                  <RootContainer />
                </MuiThemeProvider>
              </I18nextProvider>
            </MuiPickersUtilsProvider>
          </Provider>
        </Rehydrated>
      </ApolloProvider>
    )
  }
}

export default App
