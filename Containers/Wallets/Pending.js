import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native-web'
import {
  Button, H5, P, TextInput,
  Checkbox,
} from '../../Components/Html'
import I18n from '../../I18n/I18n'
import styles from '../../Containers/Styles/SettingsStyles'

class Pending extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currentPassword: null,
      newPassword: null,
      confirmNewPassword: null,
      activateTwoFactor: false,
    }
  }

  _updateCheck () {
    this.setState({
      activateTwoFactor: !this.state.activateTwoFactor,
    })
  }

  render () {
    return (
      <View style={styles.security.container}>
        <View style={{ flex: 1 }} />
        <View
          style={
            [styles.flexColumn, { flex: 1, justifyContent: 'space-between' }]
          }
        >
          <H5>{I18n.t('settings.security.mfa_title')}</H5>
          <P>{I18n.t('settings.security.mfa_description')}</P>
          <Checkbox
            label={I18n.t('settings.security.activate_mfa')}
            checked={this.state.activateTwoFactor}
            onChange={() => this._updateCheck()}
          />
          <Button>Save</Button>
        </View>
        <View style={{ flex: 1 }} />
        <View
          style={
            [styles.flexColumn, { flex: 1, justifyContent: 'space-between' }]
          }
        >
          <H5>{I18n.t('settings.security.change_password_title')}</H5>
          <TextInput
            onChangeText={currentPassword =>
              this.setState({ currentPassword })
            }
            placeholder={I18n.t('settings.security.current_password')}
            secureTextEntry
          />
          <TextInput
            onChangeText={newPassword =>
              this.setState({ newPassword })
            }
            placeholder={I18n.t('settings.security.new_password')}
            secureTextEntry
          />
          <TextInput
            onChangeText={confirmNewPassword =>
              this.setState({ confirmNewPassword })
            }
            placeholder={I18n.t('settings.security.confirm_new_password')}
            secureTextEntry
          />
          <Button
            style={{ marginTop: 20 }}
            onPress={() =>
              this.props.submit(
                this.state.currentPassword,
                this.state.newPassword,
                this.state.confirmNewPassword,
              )
            }
          >
            Save
          </Button>
        </View>
        <View style={{ flex: 1 }} />

      </View>
    )
  }
}

Pending.propTypes = {
  submit: PropTypes.func,
}

Pending.defaultProps = {
  submit: () => {},
}

export default Pending
