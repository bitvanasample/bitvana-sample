import React, { Component } from 'react'
import { View } from '../../Components/Html'
import TableBalances from '../../Components/Tables/TableBalances'

class Balances extends Component {
  render () {
    return (
      <View>
        <TableBalances />
      </View>
    )
  }
}

export default Balances
