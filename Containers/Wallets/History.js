import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native-web'
import { connect } from 'react-redux'
import CurrencyHistory from '../../Components/Tables/CurrencyHistory'
import SelectCurrency from '../../Components/CryptoForm/SelectCurrency'
import RoutesProps from '../../GeneralProps/RoutesProps'
import styles from '../../Containers/Styles/BalancesStyles'
import { redirectTo } from '../../Routes/Helpers'
import I18n from '../../I18n/I18n'

class History extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currencyCode: props.match.params.code,
    }
  }

  render () {
    return (
      <View>
        <View style={[styles.inputWrapper, { zIndex: 2 }]}>
          <SelectCurrency
            text={I18n.t('currency')}
            value={this.state.currencyCode}
            onChange={(currencyCode) => {
              this.setState({ currencyCode })
              redirectTo(`/wallets/history/${currencyCode}`)
            }}
          />
        </View>
        <CurrencyHistory
          currencyCode={this.state.currencyCode}
        />
      </View>
    )
  }
}

History.propTypes = {
  ...RoutesProps({
    currencyCode: PropTypes.string,
  }),
}

History.defaultProps = {
  ...RoutesProps({
    currencyCode: 'BTC',
  }),
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(History)
