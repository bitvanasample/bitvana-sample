import React, { Component } from 'react'
import Media from 'react-media'
import Mobile from './Mobile'
import Desktop from './Desktop'

class Landing extends Component {
  render () {
    return (
      <Media query='(max-width: 599px)'>
        {isSmall => (isSmall
          ? <Mobile {...this.props} />
          : <Desktop {...this.props} />
        )}
      </Media>
    )
  }
}


export default Landing
