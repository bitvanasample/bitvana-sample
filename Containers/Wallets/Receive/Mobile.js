import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RoutesProps from '../../../GeneralProps/RoutesProps'
import { SelectCurrency, CryptoDeposit, Qr }
  from '../../../Components/CryptoForm'
import { View } from '../../../Components/Html'
import I18n from '../../../I18n/I18n'
import styles from '../../../Containers/Styles/BalancesStyles'
import { redirectTo } from '../../../Routes/Helpers'
import LoginRequired from '../../../Components/CryptoForm/LoginRequired'
import BalanceMobile from './BalanceMobile'
import FiatDeposit from '../FiatDeposit'

class Receive extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currencyCode: props.match.params.code,
    }
  }

  render () {
    return (
      <View style={[styles.flexColumn, styles.flexStart]}>
        <View style={[styles.flexColumn,
          styles.flex, styles.flexStart, styles.fullWidth]}
        >
          <View style={[styles.inputWrapper]}>
            <SelectCurrency
              text={I18n.t('select.select_currency')}
              value={this.state.currencyCode}
              onChange={(currencyCode) => {
                this.setState({ currencyCode })
                redirectTo(`/wallets/receive/${currencyCode}`)
              }}
            />
          </View>
          <BalanceMobile
            currencyCode={this.state.currencyCode}
          />
        </View>
        <View style={[styles.flexColumn, styles.flex]}>
          <LoginRequired
            approvalLevel={1}
            style={{ border: 0 }}
            component={
              <View>
                <View style={[styles.flexRow, styles.centerContent]}>
                  <Qr currencyCode={this.state.currencyCode} />
                </View>
                <br />
                <View style={[styles.flex]}>
                  <CryptoDeposit
                    currencyCode={this.state.currencyCode}
                  />
                </View>
                <FiatDeposit
                  currencyCode={this.state.currencyCode}
                />
              </View>
            }
          />
        </View>
      </View>
    )
  }
}

Receive.propTypes = {
  ...RoutesProps({
    currencyCode: PropTypes.string,
    balance: PropTypes.func.isRequired,
  }),
}

Receive.defaultProps = {
  ...RoutesProps({
    currencyCode: 'BTC',
  }),
}

export default Receive
