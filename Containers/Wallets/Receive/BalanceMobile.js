import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from '../../../Components/Html'
import RoutesProps from '../../../GeneralProps/RoutesProps'
import AmountCurrency from '../../../Components/CryptoForm/AmountCurrency'
import I18n from '../../../Lib/I18n'
import Indicator from '../../../QuanticComponents/Indicator'
import styles from '../../../Containers/Styles/BalancesStyles'
import { withBalance } from '../../../GraphQl/Wallets/BalanceQuery'

const indicators = (
  [
    <View style={styles.balanceMobile.value}>
      <Indicator size='small' />
    </View>,
    <View style={styles.balanceMobile.value}>
      <Indicator size='small' />
    </View>,
    <View style={styles.balanceMobile.value}>
      <Indicator size='small' />
    </View>,
  ]
)

class BalanceMobile extends Component {
  render () {
    const {
      code, availableBalance,
      inactiveOrdersBalance,
      unconfirmedBalance, loading,
    } = this.props
    return (
      <View style={styles.balanceMobile.container}>
        <View style={styles.balanceMobile.headers}>
          <Text style={styles.balanceMobile.header}>
            {I18n.t('balances.available')}
          </Text>
          <Text style={styles.balanceMobile.header}>
            {I18n.t('balances.inactiveOrdersBalance')}
          </Text>
          <Text style={styles.balanceMobile.header}>
            {I18n.t('balances.unconfirmedBalance')}
          </Text>
        </View>
        <View style={styles.balanceMobile.values}>
          {
            loading
              ? indicators
              : [
                <AmountCurrency
                  amount={availableBalance}
                  currency={code}
                  style={styles.balanceMobile.value}
                />,
                <AmountCurrency
                  amount={inactiveOrdersBalance}
                  currency={code}
                  style={styles.balanceMobile.value}
                />,
                <AmountCurrency
                  amount={unconfirmedBalance}
                  currency={code}
                  style={styles.balanceMobile.value}
                />,
              ]
          }
        </View>
      </View>
    )
  }
}

BalanceMobile.propTypes = {
  ...RoutesProps({
    currencyCode: PropTypes.string,
    balance: PropTypes.func.isRequired,
    availableBalance: PropTypes.number,
  }),
}

BalanceMobile.defaultProps = {
  ...RoutesProps({
    currencyCode: 'BTC',
  }),
}

export default withBalance(BalanceMobile)
