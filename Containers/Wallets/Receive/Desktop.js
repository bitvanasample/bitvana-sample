import React, { Component } from 'react'
import PropTypes from 'prop-types'
import RoutesProps from '../../../GeneralProps/RoutesProps'
import { SelectCurrency, CryptoDeposit, Qr }
  from '../../../Components/CryptoForm'
import BalanceDetail from './../BalanceDetail'
import { A, H5, View } from '../../../Components/Html'
import I18n from '../../../I18n/I18n'
import styles from '../../../Containers/Styles/BalancesStyles'
import { redirectTo } from '../../../Routes/Helpers'
import LoginRequired from '../../../Components/CryptoForm/LoginRequired'
import FiatDeposit from '../FiatDeposit'


class Receive extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currencyCode: props.match.params.code,
    }
  }

  render () {
    return (
      <View
        style={[
          styles.sectionContainer,
          styles.flexRow,
          styles.centerContent,
          styles.flexStart,
        ]}
      >

        <View style={[styles.flexColumn, styles.flex]}>
          <View style={[styles.inputWrapper]}>
            <H5 style={{ marginBottom: 0, marginTop: 0, paddingBottom: 5 }}>
              {I18n.t('deposit.currency')}
            </H5>
            <SelectCurrency
              style={{ width: '100%' }}
              text={I18n.t('select.select_currency')}
              value={this.state.currencyCode}
              onChange={(currencyCode) => {
                this.setState({ currencyCode })
                redirectTo(`/wallets/receive/${currencyCode}`)
              }}
            />
            <BalanceDetail
              currencyCode={this.state.currencyCode}
              minimumAmountToSend
              minimumConfirmations
            />
            <A
              style={{ paddingTop: 10, paddingBottom: 5 }}
              href={`/wallets/history/${this.state.currencyCode}`}
            >
              {I18n.t('deposit.goToHistory')}
            </A>
          </View>
        </View>

        <View style={[styles.flexColumn, styles.flex]}>
          <LoginRequired
            approvalLevel={1}
            style={{ border: 0 }}
            component={
              <View style={styles.inputWrapper}>
                <View style={[styles.flexRow, styles.centerContent]}>
                  <Qr currencyCode={this.state.currencyCode} />
                </View>
                <View style={[styles.flex]}>
                  <CryptoDeposit
                    currencyCode={this.state.currencyCode}
                  />
                </View>
                <FiatDeposit
                  currencyCode={this.state.currencyCode}
                />
              </View>
            }
          />
        </View>

      </View>
    )
  }
}

Receive.propTypes = {
  ...RoutesProps({
    currencyCode: PropTypes.string,
  }),
}

Receive.defaultProps = {
  ...RoutesProps({
    currencyCode: 'BTC',
  }),
}

export default Receive
