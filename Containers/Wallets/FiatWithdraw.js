import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import { H5, View, Select, Text, A } from '../../Components/Html'
import I18n from '../../I18n/I18n'
import styles from '../../Containers/Styles/BalancesStyles'
import { Withdraw as AstropayWithdraw } from
  '../../Components/CryptoForm/Astropay'
import BankWithdraw from
  '../../Components/CryptoForm/BankWithdraw'
import { withCurrencies } from '../../GraphQl/Currencies/CurrenciesQuery'
import { withBankAccounts } from '../../GraphQl/BankAccounts/BankAccountsQuery'


class FiatWithdrawal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      withdrawalMethod: 'bank',
    }
  }
  render () {
    const data =
      [
        // { value: 'astropay', label: 'Astropay' },
        { value: 'bank', label: 'Banco' },
      ]
    const { bankAccounts } = this.props
    const { crypto, withdrawalActive } =
    this.props.findCurrency(this.props.currencyCode)
    const hasVerifiedAccount =
    bankAccounts.some(e => e.status === 1) // 1 is verified
    if (!crypto) {
      if (!withdrawalActive) {
        return (
          <View style={styles.fiatDepositContainer}>
            <span style={{ textAlign: 'center' }}>
              <Text>
                {I18n.t(
                  'withdrawals.currencyUnavailable',
                  { code: this.props.currencyCode },
                )}
              </Text>
            </span>
          </View>
        )
      }
      if (hasVerifiedAccount) {
        return (
          <View>
            <H5 style={{ marginBottom: 0, marginTop: 0, paddingBottom: 5 }}>
              {I18n.t('withdrawals.withdrawalMethod')}
            </H5>
            <View>
              <Select
                style={styles.withdrawalSelectMethod}
                placeholder={I18n.t('withdrawals.withdrawalMethod')}
                data={data}
                onChange={withdrawalMethod =>
                  this.setState({ withdrawalMethod })}
                value={this.state.withdrawalMethod}
              />
            </View>
            {this.state.withdrawalMethod === 'bank' ?
              <BankWithdraw currencyCode={this.props.currencyCode} />
              : null}

            {this.state.withdrawalMethod === 'astropay' ?
              <AstropayWithdraw currencyCode={this.props.currencyCode} />
              : null}

          </View>
        )
      }
      return (
        <View style={styles.fiatDepositContainer}>
          <span style={{ textAlign: 'center' }}>
            <br />
            <Text
              style={styles.inlineComponent}
            >{I18n.t('withdrawals.verifyAccountFirstPre')}
            </Text>
            <A
              href='/settings/bank-accounts'
              style={styles.inlineComponent}
            >
              {I18n.t('withdrawals.verifyAccountFirst')}
            </A>
            <Text
              style={styles.inlineComponent}
            >
              {I18n.t(
                'withdrawals.verifyAccountFirstPost',
                { code: this.props.currencyCode },
              )}
            </Text>
          </span>
        </View>
      )
    }
    return <View />
  }
}

FiatWithdrawal.propTypes = {
  bankAccounts: PropTypes.arrayOf(PropTypes.shape({})),
  currencyCode: PropTypes.string,
  findCurrency: PropTypes.func,
}

FiatWithdrawal.defaultProps = {
  bankAccounts: [],
  currencyCode: 'BTC',
  findCurrency: () => {},
}

export default compose(withCurrencies, withBankAccounts)(FiatWithdrawal)

