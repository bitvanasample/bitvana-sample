import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import { H5, View, Select, Text, A } from '../../Components/Html'
import I18n from '../../I18n/I18n'
import styles from '../../Containers/Styles/BalancesStyles'
import { Deposit as AstropayDeposit } from
  '../../Components/CryptoForm/Astropay'
import BankDeposit from '../../Components/CryptoForm/BankDeposit'
import { withCurrencies } from '../../GraphQl/Currencies/CurrenciesQuery'
import { withBankAccounts } from '../../GraphQl/BankAccounts/BankAccountsQuery'

class FiatDeposit extends Component {
  constructor (props) {
    super(props)
    this.state = {
      depositMethod: 'bank',
    }
  }
  render () {
    const data =
      [
        // { value: 'astropay', label: 'Astropay' },
        { value: 'bank', label: 'Banco' },
      ]
    const { bankAccounts } = this.props
    const { crypto, depositActive } =
    this.props.findCurrency(this.props.currencyCode)
    const hasVerifiedAccount =
    bankAccounts.some(e => e.status === 1) // 1 is verified
    if (!crypto) {
      if (!depositActive) {
        return (
          <View style={styles.fiatDepositContainer}>
            <span style={{ textAlign: 'center' }}>
              <Text>
                {I18n.t(
                  'deposits.currencyUnavailable',
                  { code: this.props.currencyCode },
                )}
              </Text>
            </span>
          </View>
        )
      }
      if (hasVerifiedAccount) {
        return (
          <View style={styles.fiatDepositContainer}>
            <H5 style={{ marginBottom: 0, marginTop: 0, paddingBottom: 5 }}>
              {I18n.t('deposits.depositMethod')}
            </H5>
            <Select
              style={styles.depositSelectMethod}
              placeholder={I18n.t('deposits.depositMethod')}
              data={data}
              onChange={depositMethod => this.setState({ depositMethod })}
              value={this.state.depositMethod}
            />
            {this.state.depositMethod === 'bank' ?
              <BankDeposit currencyCode={this.props.currencyCode} />
              : null}

            {this.state.depositMethod === 'astropay' ?
              <AstropayDeposit currencyCode={this.props.currencyCode} />
              : null}

          </View>
        )
      }
      return (
        <View style={styles.fiatDepositContainer}>
          <span style={{ textAlign: 'center' }}>
            <br />
            <Text
              style={styles.inlineComponent}
            >{I18n.t('deposits.verifyAccountFirstPre')}
            </Text>
            <A
              href='/settings/bank-accounts'
              style={styles.inlineComponent}
            >
              {I18n.t('deposits.verifyAccountFirst')}
            </A>
            <Text
              style={styles.inlineComponent}
            >
              {I18n.t(
                'deposits.verifyAccountFirstPost',
                { code: this.props.currencyCode },
              )}
            </Text>
          </span>
        </View>
      )
    }
    return <View />
  }
}

FiatDeposit.propTypes = {
  bankAccounts: PropTypes.arrayOf(PropTypes.shape({})),
  currencyCode: PropTypes.string,
  findCurrency: PropTypes.func,
}

FiatDeposit.defaultProps = {
  bankAccounts: [],
  currencyCode: 'BTC',
  findCurrency: () => {},
}

export default compose(withCurrencies, withBankAccounts)(FiatDeposit)

