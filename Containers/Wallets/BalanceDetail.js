import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withBalance } from '../../GraphQl/Wallets/BalanceQuery'
import RoutesProps from '../../GeneralProps/RoutesProps'
import CurrencyField from '../../Components/CryptoForm/CurrencyField'
import { View } from '../../Components/Html'
import styles from '../../Containers/Styles/BalancesStyles'
import Big from '../../Lib/Big'

class BalanceDetail extends Component {
  render () {
    return (
      <View>
        <View style={styles.balanceDetailContainer}>
          <CurrencyField
            style={styles.detailTextLeftMobile}
            currency={this.props.currency}
            loading={this.props.loading}
            field='totalBalance'
            customValue={Big(this.props.inactiveOrdersBalance || 0)
              .plus(Big(this.props.availableBalance || 0))}
            isAmountCurrency
          />
          <CurrencyField
            style={styles.detailTextRightMobile}
            currency={this.props.currency}
            loading={this.props.loading}
            field='unconfirmedBalance'
            customValue={this.props.unconfirmedBalance}
            isAmountCurrency
          />
        </View>
        <View style={styles.balanceDetailContainer}>
          <CurrencyField
            style={styles.detailTextLeftMobile}
            currency={this.props.currency}
            loading={this.props.loading}
            field='inactiveOrdersBalance'
            customValue={this.props.inactiveOrdersBalance}
            isAmountCurrency
          />
          <CurrencyField
            style={styles.detailTextRightMobile}
            currency={this.props.currency}
            loading={this.props.loading}
            field='availableBalance'
            customValue={this.props.availableBalance}
            isAmountCurrency
          />
        </View>
        <View style={styles.balanceDetailContainer}>
          { this.props.minimumAmountToSend
            ? (
              <CurrencyField
                style={styles.detailTextLeftMobile}
                currency={this.props.currency}
                loading={this.props.loading}
                field='minimumAmountToSend'
                isAmountCurrency
              />
            ) : null
          }
          { this.props.minimumConfirmations && this.props.currency.crypto
            ? (
              <CurrencyField
                style={styles.detailTextLeftMobile}
                currency={this.props.currency}
                loading={this.props.loading}
                field='minimumConfirmations'
                customValue={
                  parseInt(this.props.currency.minimumConfirmations, 10)
                }
              />
            ) : null
          }
          { this.props.withdrawalFee
            ? (
              <CurrencyField
                style={styles.displayWebOnly}
                currency={this.props.currency}
                loading={this.props.loading}
                field='withdrawalFee'
                isAmountCurrency
              />
            ) : null
          }
          <CurrencyField
            style={styles.detailTextRightMobile}
            currency={this.props.currency}
            loading={this.props.loading}
            field='waitingWithdrawalBalance'
            customValue={this.props.waitingWithdrawalBalance}
            isAmountCurrency
          />
        </View>
      </View>
    )
  }
}

BalanceDetail.propTypes = {
  ...RoutesProps({
    currencyCode: PropTypes.string,
  }),
}

BalanceDetail.defaultProps = {
  ...RoutesProps({
    currencyCode: 'BTC',
  }),
}

export default withBalance(BalanceDetail)
