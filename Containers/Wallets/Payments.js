import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View } from '../../Components/Html'
import TablePayments from '../../Components/Tables/TablePayments'

class Payments extends Component {
  render () {
    return (
      <View>
        <TablePayments />
      </View>
    )
  }
}

Payments.propTypes = {}

Payments.defaultProps = {}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Payments)
