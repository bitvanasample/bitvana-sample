import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native-web'
import { connect } from 'react-redux'
import AppBarExchange from '../../Components/AppBar/Exchange'
import AppMenu from '../../Components/AppMenu/Landing'
import { H3, Tab, Tabs } from '../../Components/Html'
import ExchangeMain from '../ExchangeMain'
import I18n from '../../I18n/I18n'
import { redirectTo, currentPath as cp } from '../../Routes/Helpers'
import { isMobile } from '../../Config/AppConfig'

class Withdrawals extends Component {
  render () {
    const { pathname } = this.props.location
    let code
    if (pathname.indexOf('/wallets/receive/') !== -1) {
      [, code] = pathname.split('/wallets/receive/')
    } else if (pathname.indexOf('/wallets/history/') !== -1) {
      [, code] = pathname.split('/wallets/history/')
    }
    return (
      <View>
        <AppBarExchange />
        <AppMenu />
        <ExchangeMain>
          {
            !isMobile
            && <H3 style={{ paddingLeft: 15 }}>{I18n.t('wallets.title')}</H3>
          }
          <Tabs
            onChange={url => redirectTo(`/wallets/${url}`)}
            value={cp(this.props, 2)}
            tabs={
              [
                <Tab iconName='dollar' value='balances'>
                  {I18n.t('wallets.balances')}
                </Tab>,
                <Tab iconName='qrcode' value={`receive/${code || ''}`}>
                  {I18n.t('wallets.receive')}
                </Tab>,
                <Tab iconName='history' value={`history/${code || ''}`}>
                  {I18n.t('wallets.history')}
                </Tab>,
                <Tab iconName='share-square' value='withdrawal'>
                  {I18n.t('wallets.withdrawal')}
                </Tab>,
              ]
            }
            tabContent={this.props.children}
          />
        </ExchangeMain>
      </View>
    )
  }
}

Withdrawals.propTypes = {
  location: {
    pathname: PropTypes.string,
  },
  children: PropTypes.arrayOf(PropTypes.shape({})),
}

Withdrawals.defaultProps = {
  children: [],
  location: {
    pathname: '',
  },
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Withdrawals)
