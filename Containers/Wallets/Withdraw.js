import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import RoutesProps from '../../GeneralProps/RoutesProps'
import { View, H5 } from '../../Components/Html'
import I18n from '../../I18n/I18n'
import styles from '../../Containers/Styles/BalancesStyles'
import BalanceDetail from './BalanceDetail'
import { withCurrencies } from '../../GraphQl/Currencies/CurrenciesQuery'
import { SelectCurrency, CryptoWithdraw } from '../../Components/CryptoForm'
import { redirectTo } from '../../Routes/Helpers'
import LoginRequired from '../../Components/CryptoForm/LoginRequired'
import FiatWithdraw from './FiatWithdraw'

class Withdrawal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currencyCode: props.match.params.code,
    }
  }
  render () {
    return (
      <View
        style={[styles.sectionContainer,
          styles.withdrawContainer,
          styles.flexBeginning,
        ]}
      >
        <View style={[styles.flexColumn, styles.flex, styles.fullWidth]}>
          <View style={[styles.inputWrapper]}>
            <H5 style={{ marginBottom: 0, marginTop: 0, paddingBottom: 5 }}>
              {I18n.t('withdrawals.currency')}
            </H5>
            <SelectCurrency
              text={I18n.t('select.select_currency')}
              value={this.state.currencyCode}
              onChange={(currencyCode) => {
                this.setState({ currencyCode })
                redirectTo(`/wallets/withdrawal/${currencyCode}`)
              }}
            />
            <BalanceDetail
              currencyCode={this.state.currencyCode}
              withdrawalFee
              minimumAmountToSend
            />
          </View>
        </View>

        <View
          style={
            [styles.flexColumn, styles.flex, styles.fullWidth]
          }
        >
          <LoginRequired
            approvalLevel={1}
            style={{ border: 0 }}
            component={
              <View style={[styles.flexColumn, styles.inputWrapper]}>
                <CryptoWithdraw currencyCode={this.state.currencyCode} />
                <FiatWithdraw currencyCode={this.state.currencyCode} />
              </View>
            }
          />
        </View>

      </View>
    )
  }
}

Withdrawal.propTypes = {
  ...RoutesProps({
    currencyCode: PropTypes.string,
  }),
}

Withdrawal.defaultProps = {
  ...RoutesProps({
    currencyCode: 'BTC',
  }),
}

export default compose(withCurrencies)(Withdrawal)

