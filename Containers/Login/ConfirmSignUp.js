import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Auth } from 'aws-amplify'
import { Button, A, TextInput } from '../../Components/Html'
import Toast from '../../Lib/Toast'
import { redirectTo } from '../../Routes/Helpers'
import styles from '../../Containers/Styles/LandingStyles'
import I18n from '../../I18n/I18n'

class ConfirmSignUp extends Component {
  constructor (props) {
    super(props)
    this.state = {
      email: props.location.state
        && props.location.state.email ? props.location.state.email : '',
      code: '',
      loading: false,
    }
  }
  _resend () {
    Auth.resendSignUp(this.state.email).then(() => {
      Toast.show({ text: I18n.t('cognito.common.email_sent') })
    })
      .catch((err) => {
        if (err.indexOf('should either be non-null') !== -1) {
          Toast.show({
            text: I18n.t('cognito.common.email_not_empty'),
            type: 'error',
          })
          this.setState({ loading: false })
        } else {
          Toast.show({ text: `Error: ${(err && err.code) || err}` })
        }
        this.setState({ loading: false })
      })
  }
  _confirmSignUp () {
    this.setState({ loading: true })
    Auth.confirmSignUp(this.state.email, this.state.code)
      .then((data) => {
        if (data === 'SUCCESS') {
          Toast.show({ text: I18n.t('cognito.confirm_signup.success') })
          redirectTo('/login/sign-in')
        } else {
          this.setState({ loading: false })
        }
      })
      .catch((err) => {
        if (err.code === 'CodeMismatchException') {
          Toast.show({
            text: I18n.t('cognito.common.code_error'),
            type: 'error',
          })
        } else if (err.code === 'NotAuthorizedException') {
          if (err.message.indexOf('UNCONFIRMED') !== -1) {
            Toast.show({
              text: I18n.t('cognito.confirm_signup.not_unconfirmed'),
              type: 'error',
            })
            redirectTo('/login/sign-in')
          } else {
            Toast.show({
              text: I18n.t('cognito.confirm_signup.not_valid'),
              type: 'error',
            })
          }
        } else if (err === 'Username cannot be empty') {
          Toast.show({
            text: I18n.t('cognito.common.email_not_empty'),
            type: 'error',
          })
        } else if (err === 'Code cannot be empty') {
          Toast.show({
            text: I18n.t('cognito.common.code_not_empty'),
            type: 'error',
          })
        } else if (err.code === 'UserNotFoundException') {
          Toast.show({
            text: I18n.t('cognito.common.email_not_found'),
            type: 'error',
          })
        } else {
          Toast.show({ text: `Error: ${(err && err.code) || err}` })
        }
        this.setState({ loading: false })
      })
  }


  render () {
    return (
      <div>
        <p style={styles.logTitle}>
          {I18n.t('cognito.confirm_signup.title')}
        </p>
        <hr />
        <br />
        <TextInput
          onChangeText={email =>
            this.setState({ email })
          }
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              this._confirmSignUp()
            }
          }}
          placeholder={I18n.t('cognito.common.email')}
          value={this.state.email}
        />
        <br />
        <TextInput
          onChangeText={code =>
            this.setState({ code })
          }
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              this._confirmSignUp()
            }
          }}
          placeholder={I18n.t('cognito.common.code')}
        />
        <Button
          style={styles.btnSignInUp}
          title='Confirm account'
          loading={this.state.loading}
          onPress={() => this._confirmSignUp()}
        >
          {I18n.t('cognito.confirm_signup.confirm_signup_btn')}
        </Button>
        <Button
          style={styles.btnSignInUp}
          title='Resend code'
          color='white'
          onPress={() => this._resend()}
        >
          {I18n.t('cognito.confirm_signup.resend_btn')}
        </Button>
        <A
          style={{ paddingTop: 10, paddingBottom: 5 }}
          href='/login/sign-in'
        >
          {I18n.t('cognito.common.signin_btn')}
        </A>
      </div>
    )
  }
}

ConfirmSignUp.propTypes = {
  location: {
    state: {
      email: PropTypes.string,
    },
  },
}

ConfirmSignUp.defaultProps = {}

export default ConfirmSignUp
