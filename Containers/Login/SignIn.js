import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Auth } from 'aws-amplify'
import styles from '../../Containers/Styles/LandingStyles'
import { Button, TextInput, A } from '../../Components/Html'
import LoginActions from '../../Redux/LoginRedux'
import Toast from '../../Lib/Toast'
import { redirectTo } from '../../Routes/Helpers'
import I18n from '../../I18n/I18n'
import { client } from '../App'
import CacheSpan from '../../GraphQl/Currencies/CacheSpan'

class SignIn extends Component {
  constructor (props) {
    super(props)
    this.state = {
      email: props.location.state
        && props.location.state.email ? props.location.state.email : '',
      loading: false,
    }
  }

  _signIn (attempt = 0) {
    const { email } = this.state
    this.setState({ loading: true })
    Auth.signIn(email, this.state.password)
      .then((data) => {
        this.setState({ loading: false })
        if (data.signInUserSession === null) {
          global.userSignInDataForConfirmSignIn = data
          redirectTo('/login/confirm-sign-in')
        } else {
          // we reset the cache when this view is opened
          client.cache.reset()
          this.props.signedIn()
          // redirectTo('/wallets/balances')
        }
      })
      .catch((err) => {
        if (err.code === 'UserNotConfirmedException') {
          Toast.show({ text: I18n.t('cognito.signin.not_confirmed') })
          Auth.resendSignUp(email).then(() => {}).catch(() => {})
          redirectTo('/login/confirm-sign-up', { email })
        } else if (err === 'Password cannot be empty') {
          Toast.show({
            text: I18n.t('cognito.common.pass_not_empty'),
            type: 'error',
          })
        } else if (err.code === 'UserNotFoundException') {
          Toast.show({
            text: I18n.t('cognito.common.email_not_found'),
            type: 'error',
          })
        } else if (err === 'Username cannot be empty') {
          Toast.show({
            text: I18n.t('cognito.common.email_not_empty'),
            type: 'error',
          })
        } else if (err.code === 'NotAuthorizedException') {
          Toast.show({
            text: I18n.t('cognito.signin.signin_failed'),
            type: 'error',
          })
        } else if (attempt <= 1) {
          this._signIn(attempt + 1)
        } else {
          Toast.show({ text: `Error: ${(err && err.code) || err}` })
        }
        this.setState({ loading: false })
      })
  }

  render () {
    return (
      <div>
        <p style={styles.logTitle}>
          {I18n.t('cognito.signin.title')}
        </p>
        <hr />
        <br />
        <TextInput
          isEmail
          onChangeText={email =>
            this.setState({ email })
          }
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              this._signIn()
            }
          }}
          placeholder={I18n.t('cognito.common.email')}
          value={this.state.email}
        />
        <br />
        <TextInput
          onChangeText={password =>
            this.setState({ password })
          }
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              this._signIn()
            }
          }}
          placeholder={I18n.t('cognito.signin.pass')}
          secureTextEntry
        />
        <Button
          style={styles.btnSignInUp}
          loading={this.state.loading}
          onPress={() => this._signIn()
          }
        >
          {I18n.t('cognito.signin.signin_btn')}
        </Button>
        <A
          style={{ paddingTop: 10, paddingBottom: 5 }}
          href='/login/sign-up'
        >
          {I18n.t('cognito.common.signup_btn')}
        </A>
        <A
          style={{ paddingTop: 5, paddingBottom: 10 }}
          href='/login/forgot-password'
        >
          {I18n.t('cognito.common.forgot_pass_btn')}
        </A>
        <CacheSpan />
      </div>
    )
  }
}

SignIn.propTypes = {
  signedIn: PropTypes.func.isRequired,
  location: {
    state: {
      email: PropTypes.string,
    },
  },
}

SignIn.defaultProps = {}

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  signedIn: () => dispatch(LoginActions.signedIn()),
})

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)
