import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Auth } from 'aws-amplify'
import styles from '../../Containers/Styles/LandingStyles'
import { Button, TextInput, A } from '../../Components/Html'
import Toast from '../../Lib/Toast'
import { redirectTo } from '../../Routes/Helpers'
import I18n from '../../I18n/I18n'

class SignUp extends Component {
  constructor (props) {
    super(props)
    this.state = {
      email: props.location.state
        && props.location.state.email ? props.location.state.email : '',
      password: '',
      loading: false,
    }
  }

  _signUp () {
    const { email, password } = this.state
    this.setState({ loading: true })
    Auth.signUp(email, password)
      .then((data) => {
        if (data.userConfirmed === false) {
          Toast.show({ text: I18n.t('cognito.signup.success') })
          redirectTo('/login/confirm-sign-up', { email })
        } else {
          redirectTo('/login/sign-in', { email })
        }
      })
      .catch((err) => {
        if (err.code === 'InvalidPasswordException' ||
          err.code === 'InvalidParameterException') {
          Toast.show({
            text: I18n.t('cognito.common.invalid_pass'),
            type: 'error',
          })
        } else if (err.toString().indexOf('should either be non-null') !== -1) {
          Toast.show({
            text: I18n.t('cognito.common.email_not_empty'),
            type: 'error',
          })
          this.setState({ loading: false })
        } else if (err === 'Password cannot be empty') {
          Toast.show({
            text: I18n.t('cognito.common.pass_not_empty'),
            type: 'error',
          })
          this.setState({ loading: false })
        } else if (err.code === 'UsernameExistsException') {
          Toast.show({
            text: I18n.t('cognito.signup.email_exists'),
            type: 'error',
          })
          redirectTo('/login/sign-in', { email })
        } else {
          Toast.show({ text: `Error: ${(err && err.code) || err}` })
        }
        this.setState({ loading: false })
      })
  }

  render () {
    return (
      <div>
        <p style={styles.logTitle}>
          {I18n.t('cognito.signup.title')}
        </p>
        <hr />
        <br />
        <TextInput
          isEmail
          onChangeText={email => this.setState({ email })}
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              this._signUp()
            }
          }}
          placeholder={I18n.t('cognito.common.email')}
          value={this.state.email}
        />
        <br />
        <TextInput
          onChangeText={password => this.setState({ password })}
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              this._signUp()
            }
          }}
          placeholder={I18n.t('cognito.signup.pass')}
          secureTextEntry
        />
        <Button
          style={styles.btnSignInUp}
          onPress={() => this._signUp()}
          loading={this.state.loading}
        >
          {I18n.t('cognito.common.signup_btn')}
        </Button>
        {!this.state.loading &&
          <div>
            <A
              style={{ paddingTop: 10, paddingBottom: 5 }}
              href='/login/sign-in'
            >
              {I18n.t('cognito.common.signin_btn')}
            </A>
            <A
              style={{ paddingTop: 5, paddingBottom: 10 }}
              href='/login/forgot-password'
            >
              {I18n.t('cognito.common.forgot_pass_btn')}
            </A>
          </div>
        }
      </div>
    )
  }
}

SignUp.propTypes = {
  location: {
    state: {
      email: PropTypes.string,
    },
  },
}

SignUp.defaultProps = {}

export default SignUp
