import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { Auth } from 'aws-amplify'
import styles from '../../Containers/Styles/LandingStyles'
import { Button, A, TextInput } from '../../Components/Html'
import Toast from '../../Lib/Toast'
import { redirectTo } from '../../Routes/Helpers'
import I18n from '../../I18n/I18n'

class ForgotPassword extends Component {
  constructor (props) {
    super(props)
    this.state = {
      email: null,
      loading: false,
      verifying: false,
      newPassword: null,
      code: null,
    }
  }

  _forgotPassword () {
    Auth.forgotPassword(this.state.email)
      .then(() => {
        Toast.show({ text: I18n.t('cognito.common.email_sent') })
        this.setState({ verifying: true })
      })
      .catch((err) => {
        if (err === 'Username cannot be empty') {
          Toast.show({
            text: I18n.t('cognito.common.email_not_empty'),
            type: 'error',
          })
        } else if (err.code === 'UserNotFoundException') {
          Toast.show({
            text: I18n.t('cognito.common.email_not_found'),
            type: 'error',
          })
        } else if (err.code === 'InvalidParameterException') {
          Toast.show({
            text: I18n.t('cognito.forgot_pass.not_confirmed'),
            type: 'error',
          })
          Auth.resendSignUp(this.state.email).then(() => {}).catch(() => {})
          redirectTo('/login/confirm-sign-up', { email: this.state.email })
        } else {
          Toast.show({ text: `Error: ${(err && err.code) || err}` })
          // console.log('err: ', err)
        }
        this.setState({ loading: false })
      })
  }

  _forgotPasswordSubmit () {
    Auth.forgotPasswordSubmit(
      this.state.email, this.state.code,
      this.state.newPassword,
    )
      .then(() => {
        Toast.show({ text: I18n.t('cognito.forgot_pass.pass_success') })
        redirectTo('/login/sign-in')
      })
      .catch((err) => {
        if (err === 'Code cannot be empty') {
          Toast.show({
            text: I18n.t('cognito.common.code_not_empty'),
            type: 'error',
          })
        } else if (err === 'Password cannot be empty') {
          Toast.show({
            text: I18n.t('cognito.common.pass_not_empty'),
            type: 'error',
          })
        } else if (err.code === 'CodeMismatchException') {
          Toast.show({
            text: I18n.t('cognito.common.code_error'),
            type: 'error',
          })
        } else if (err.code === 'InvalidPasswordException' ||
          err.code === 'InvalidParameterException') {
          Toast.show({
            text: I18n.t('cognito.common.invalid_pass'),
            type: 'error',
          })
        } else {
          Toast.show({ text: `Error: ${(err && err.code) || err}` })
        }
        this.setState({ loading: false })
      })
  }

  _inputs () {
    if (this.state.verifying) {
      return (
        <div>
          <TextInput
            key='code'
            onChangeText={code =>
              this.setState({ code })
            }
            onKeyPress={(event) => {
              if (event.key === 'Enter') {
                this._forgotPasswordSubmit()
              }
            }}
            value={this.state.code}
            placeholder={I18n.t('cognito.common.code')}
          />
          <br />
          <TextInput
            key='newPassword'
            onChangeText={newPassword =>
              this.setState({ newPassword })
            }
            onKeyPress={(event) => {
              if (event.key === 'Enter') {
                this._forgotPasswordSubmit()
              }
            }}
            value={this.state.newPassword}
            placeholder='Nueva contraseña'
            secureTextEntry
          />
        </div>
      )
    }
    return (
      <TextInput
        key='email'
        onChangeText={email =>
          this.setState({ email })
        }
        onKeyPress={(event) => {
          if (event.key === 'Enter') {
            this._forgotPassword()
          }
        }}
        value={this.state.email}
        placeholder='Email'
      />
    )
  }

  _textSubmit () {
    return this.state.verifying
      ? 'Cambiar Contraseña'
      : 'Enviar Código'
  }

  render () {
    return (
      <div>
        <p style={styles.logTitle}>
          {I18n.t('cognito.forgot_pass.title')}
        </p>
        <hr />
        <br />
        {this._inputs()}
        <Button
          style={styles.btnSignInUp}
          onPress={() =>
            (this.state.verifying
              ? this._forgotPasswordSubmit()
              : this._forgotPassword())
          }
          loading={this.state.loading}
        >
          {this._textSubmit()}
        </Button>
        <A
          style={{ paddingTop: 10, paddingBottom: 5 }}
          href='/login/sign-in'
        >
          {I18n.t('cognito.common.signin_btn')}
        </A>
      </div>
    )
  }
}

ForgotPassword.propTypes = {}

ForgotPassword.defaultProps = {}

export default ForgotPassword
