import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { View, Image } from 'react-native-web'
import { Link } from 'react-router-dom'
import { Images } from '../../Themes'
import LoginActions from '../../Redux/LoginRedux'

class Login extends React.Component {
  render () {
    return (
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          display: 'flex',
          flex: 1,
        }}
      >
        <Link to='/'>
          <Image
            source={Images.bitvanaLogo}
            resizeMode='contain'
            style={{ width: 240, height: 100 }}
          />
        </Link>
        { this.props.children }
      </View>
    )
  }
}

Login.propTypes = {
  children: PropTypes.arrayOf(PropTypes.shape({})),
}

Login.defaultProps = {
  children: [],
}

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  signedIn: () => dispatch(LoginActions.signedIn()),
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)
