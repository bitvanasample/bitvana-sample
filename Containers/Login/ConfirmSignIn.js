import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Auth } from 'aws-amplify'
import { connect } from 'react-redux'
import { Button, A, TextInput } from '../../Components/Html'
import LoginActions from '../../Redux/LoginRedux'
import Toast from '../../Lib/Toast'
import { redirectTo } from '../../Routes/Helpers'
import styles from '../../Containers/Styles/LandingStyles'
import I18n from '../../I18n/I18n'

class ConfirmSignIn extends Component {
  constructor (props) {
    super(props)
    this.state = {
      code: null,
      loading: false,
    }
  }

  _confirmSignIn () {
    const user = global.userSignInDataForConfirmSignIn
    if (!user) {
      Toast.show({ text: I18n.t('cognito.confirm_signin.session_expired') })
      redirectTo('/login/sign-in')
    } else {
      this.setState({ loading: true })
      Auth.confirmSignIn(user, this.state.code, 'SOFTWARE_TOKEN_MFA')
        .then(() => {
          this.props.signedIn()
          redirectTo('/wallets/balances')
        })
        .catch((err) => {
          if (err === 'Code cannot be empty') {
            Toast.show({
              text: I18n.t('cognito.common.code_not_empty'),
              type: 'error',
            })
          } else if (err.code === 'CodeMismatchException') {
            Toast.show({
              text: I18n.t('cognito.common.code_error'),
              type: 'error',
            })
          } else {
            Toast.show({ text: `Error: ${(err && err.code) || err}` })
          }
          this.setState({ loading: false })
        })
    }
  }

  render () {
    return (
      <div>
        <p style={styles.logTitle}>
          {I18n.t('cognito.confirm_signin.title')}
        </p>
        <hr />
        <br />
        <TextInput
          onChangeText={code =>
            this.setState({ code })
          }
          onKeyPress={(event) => {
            if (event.key === 'Enter') {
              this._confirmSignIn()
            }
          }}
          placeholder={I18n.t('cognito.confirm_signin.code')}
        />
        <Button
          style={styles.btnSignInUp}
          loading={this.state.loading}
          onPress={() => this._confirmSignIn()}
        >
          {I18n.t('cognito.confirm_signin.confirm')}
        </Button>
        <A
          style={{ paddingTop: 10, paddingBottom: 5 }}
          href='/login/sign-in'
        >
          {I18n.t('cognito.common.signin_btn')}
        </A>
      </div>
    )
  }
}

ConfirmSignIn.propTypes = {
  signedIn: PropTypes.func.isRequired,
}

ConfirmSignIn.defaultProps = {}

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  signedIn: () => dispatch(LoginActions.signedIn()),
})

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmSignIn)
