import { ApplicationStyles, Colors } from '../../Themes/'

export default {
  muiTheme: {
    palette: {
      primary1Color: Colors.primary,
      primary2Color: Colors.secondary,
      accent1Color: Colors.accent
    }
  }
}
