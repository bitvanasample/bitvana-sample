import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { View } from 'react-native-web'
import { Auth } from 'aws-amplify'
import { splashScreenLoadingTime } from '../Config/AppConfig'
import LoginActions from '../Redux/LoginRedux'
import SplashScreen from '../Components/SplashScreen'
import styles from './Styles/RootContainerStyles'
import Routes from '../Routes'
import QMetaTags from '../Components/QMetaTags'


class RootContainer extends Component {
  constructor (props) {
    super(props)
    this.state = {
      ready: false,
    }
  }
  componentDidMount () {
    setTimeout(
      () => {
        Auth.currentSession()
          .then(() => {
            this.props.signedIn()
            this.setState({ ready: true })
          })
          .catch(() => {
            this.props.signedOut()
            this.setState({ ready: true })
          })
      },
      splashScreenLoadingTime,
    )
  }

  render () {
    if (!this.state.ready) {
      return <SplashScreen />
    }
    return (
      <View style={styles.container}>
        <QMetaTags />
        <Routes />
      </View>
    )
  }
}

RootContainer.propTypes = {
  signedIn: PropTypes.func.isRequired,
  signedOut: PropTypes.func.isRequired,
}

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  signedIn: () => dispatch(LoginActions.signedIn()),
  signedOut: () => dispatch(LoginActions.signedOut()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RootContainer)
