import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import { withCancelOrder } from '../../../GraphQl/Orders/CancelOrderMutation'
import { withMyOrders } from '../../../GraphQl/Orders/MyOrdersQuery'
import { withUserInfo } from '../../../GraphQl/Users/UserInfoQuery'
import Indicator from '../../../QuanticComponents/Indicator'
import OpenOrdersList from './OpenOrdersList'

class OpenOrders extends Component {
  componentDidUpdate (prevProps) {
    const { mainCurrency: c1, secondaryCurrency: c2 } = prevProps
    const { mainCurrency: nextC1, secondaryCurrency: nextC2 } = this.props
    if (
      (c1 !== nextC1 || c2 !== nextC2)
      || (!this.unsubscribe && (nextC1 && nextC2))
    ) {
      if (this.unsubscribe) { this.unsubscribe() }
      this.unsubscribe = this.props.subscriptionToNewOrders()
    }
  }

  componentWillUnmount () {
    if (this.unsubscribe) { this.unsubscribe() }
  }

  render () {
    if (this.props.loading) {
      return <Indicator />
    }
    return [
      <OpenOrdersList
        key='orderOn'
        visible={this.props.showActives}
        dateTime={this.props.dateTimeOrdersOn}
        orders={this.props.ordersOn}
        mainCurrency={this.props.mainCurrency}
        secondaryCurrency={this.props.secondaryCurrency}
        cancelOrder={this.props.cancelOrder}
      />,
      <OpenOrdersList
        key='orderOff'
        dateTime={this.props.dateTimeOrdersOff}
        visible={!this.props.showActives}
        orders={this.props.ordersOff}
        mainCurrency={this.props.mainCurrency}
        secondaryCurrency={this.props.secondaryCurrency}
        cancelOrder={this.props.cancelOrder}
      />,
    ]
  }
}

OpenOrders.propTypes = {
  showActives: PropTypes.bool.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  ordersOn: PropTypes.arrayOf(PropTypes.shape({
    time: PropTypes.number,
    amount: PropTypes.number,
    price: PropTypes.number,
  })),
  ordersOff: PropTypes.arrayOf(PropTypes.shape({
    time: PropTypes.number,
    amount: PropTypes.number,
    price: PropTypes.number,
  })),
  loading: PropTypes.bool,
  subscriptionToNewOrders: PropTypes.func.isRequired,
  cancelOrder: PropTypes.func,
  dateTimeOrdersOn: PropTypes.string.isRequired,
  dateTimeOrdersOff: PropTypes.string.isRequired,
}

OpenOrders.defaultProps = {
  ordersOff: [],
  ordersOn: [],
  loading: false,
  cancelOrder: () => {},
}

export default compose(
  withCancelOrder,
  withUserInfo,
  withMyOrders,
)(OpenOrders)
