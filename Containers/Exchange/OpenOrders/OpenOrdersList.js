import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import { List, AutoSizer } from 'react-virtualized'
import { View } from '../../../Components/Html'
import OpenOrderItem from './OpenOrderItem'
// import { overscanRowCount } from '../../../Config/AppConfig'

// const listAutosizeStyle = {
//   display: 'flex',
//   flex: '1 1 auto',
// }

const styleVisible = {
  false: {
    display: 'none',
    flex: 1,
  },
  true: {
    display: 'inherit',
    flex: 1,
  },
}

class OpenOrdersList extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.visible !== nextProps.visible
      || this.props.dateTime !== nextProps.dateTime
  }

  render () {
    const array = this.props.orders
    const offset = array.length - 1
    return (
      <View
        style={
          styleVisible[this.props.visible]
        }
      >
        {
          array.map((_, index) => {
            const openOrder = array[offset - index]
            return (
              <div key={openOrder.id}>
                <OpenOrderItem
                  cancelOrder={this.props.cancelOrder}
                  key={openOrder.id}
                  id={openOrder.id}
                  origin={openOrder.origin}
                  sell={openOrder.sell}
                  acquiredMoney={openOrder.acquiredMoney}
                  acquiredCurrency={openOrder.acquiredCurrency}
                  limitPrice={openOrder.limitPrice}
                  totalCurrency={openOrder.totalCurrency}
                  remainingCurrency={openOrder.remainingCurrency}
                  createdAt={openOrder.createdAt}
                  state={openOrder.state}
                  secondaryCurrency={this.props.secondaryCurrency}
                  mainCurrency={this.props.mainCurrency}
                />
              </div>
            )
          })
        }
      </View>
    )
    // return (
    //   <View
    //     style={
    //       styleVisible[this.props.visible]
    //     }
    //   >
    //     <div style={listAutosizeStyle}>
    //       <AutoSizer>
    //         {({ width, height }) => (
    //           <List
    //             overscanRowCount={overscanRowCount}
    //             width={width}
    //             height={height}
    //             rowCount={array.length}
    //             rowHeight={18}
    //             rowWidth={width}
    //             scrollToIndex={0}
    //             rowRenderer={({ index, style }) => {
    //               const openOrder = array[offset - index]
    //               return (
    //                 <div key={openOrder.id} style={style}>
    //                   <OpenOrderItem
    //                     cancelOrder={this.props.cancelOrder}
    //                     key={openOrder.id}
    //                     id={openOrder.id}
    //                     origin={openOrder.origin}
    //                     sell={openOrder.sell}
    //                     acquiredMoney={openOrder.acquiredMoney}
    //                     acquiredCurrency={openOrder.acquiredCurrency}
    //                     limitPrice={openOrder.limitPrice}
    //                     totalCurrency={openOrder.totalCurrency}
    //                     remainingCurrency={openOrder.remainingCurrency}
    //                     createdAt={openOrder.createdAt}
    //                     state={openOrder.state}
    //                     secondaryCurrency={this.props.secondaryCurrency}
    //                     mainCurrency={this.props.mainCurrency}
    //                   />
    //                 </div>
    //               )
    //             }}
    //           />
    //         )}
    //       </AutoSizer>
    //     </div>
    //   </View>
    // )
  }
}

OpenOrdersList.propTypes = {
  dateTime: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  orders: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  cancelOrder: PropTypes.func.isRequired,
}

OpenOrdersList.defaultProps = {}

export default OpenOrdersList

