import React, { Component } from 'react'
import PropTypes from 'prop-types'
import TouchableOpacity from '../../../Components/TouchableOpacity'
import { View } from '../../../Components/Html'
import JustOneRender from '../../../Components/JustOneRender'
import RenderConditional from '../../../Components/RenderConditional'
import Text from '../Text'
import styles from '../../Styles/ExchangeStyles'
import I18n from '../../../I18n/I18n'
import OpenOrders from './OpenOrders'

const basicComparation = (a, b) => a.x !== b.x

const orderBookHeaderStyle = {
  ...styles.flexRow,
  ...styles.tableBorder,
  paddingLeft: 0,
  paddingRight: 0,
}

const styleOpacityShowActives = {
  true: {
    opacity: 1,
  },
  false: {
    opacity: 0.5,
  },
}

const styleFlexCenter = {
  ...styles.flex,
  ...styles.flexCenter,
}

class Index extends Component {
  constructor (props) {
    super(props)
    this.state = {
      showActives: true,
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.state.showActives !== nextState.showActives
      || this.props.mainCurrency !== nextProps.mainCurrency
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
    )
  }

  render () {
    const { mainCurrency, secondaryCurrency } = this.props
    const i18 = 'exchange.openOrders'
    return (
      <View style={styles.openOrders.container}>
        <RenderConditional
          x={this.state.showActives}
          condition={basicComparation}
        >
          <View style={styles.title.container}>
            <View style={styleFlexCenter}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({ showActives: true })
                }
                style={styleOpacityShowActives[this.state.showActives]}
              >
                <Text style={styles.title.title}>
                  {I18n.t(`${i18}.title`)}
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styleFlexCenter}>
              <TouchableOpacity
                onPress={() =>
                  this.setState({ showActives: false })
                }
                style={styleOpacityShowActives[!this.state.showActives]}
              >
                <Text style={styles.title.title}>
                  {I18n.t(`${i18}.title2`)}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </RenderConditional>
        <JustOneRender>
          {/* Order book sections */}
          <View style={styles.flexColumn} >
            {/* Order book header */}
            <View style={orderBookHeaderStyle} >
              <View
                style={[
                  styles.openOrders.tableCellHeader,
                  styles.openOrders.typeHeader,
                ]}
              >
                <Text>
                  {I18n.t(`${i18}.type`)}
                </Text>
              </View>
              <View
                style={[
                  styles.openOrders.tableCellHeader,
                  styles.openOrders.limitPriceHeader,
                ]}
              >
                <Text>
                  {I18n.t(`${i18}.limitPrice`)}
                </Text>
                <Text note style={styles.text}>
                  {` ${mainCurrency}`}
                </Text>
              </View>
              <View
                style={[
                  styles.openOrders.tableCellHeader,
                  styles.openOrders.averagePriceHeader,
                ]}
              >
                <Text>
                  {I18n.t(`${i18}.averagePrice`)}
                </Text>
                <Text note style={styles.text}>
                  {` ${mainCurrency}`}
                </Text>
              </View>
              <View
                style={[
                  styles.openOrders.tableCellHeader,
                  styles.openOrders.totalCurrencyHeader,
                ]}
              >
                <Text>
                  {I18n.t(`${i18}.totalCurrency`)}
                </Text>
                <Text note style={styles.text}>
                  {` ${secondaryCurrency}`}
                </Text>
              </View>
              <View
                style={[
                  styles.openOrders.tableCellHeader,
                  styles.openOrders.remainingCurrencyHeader,
                ]}
              >
                <Text>
                  {I18n.t(`${i18}.remainingCurrency`)}
                </Text>
                <Text note style={styles.text}>
                  {` ${secondaryCurrency}`}
                </Text>
              </View>
              <View
                style={[
                  styles.openOrders.tableCellHeader,
                  styles.openOrders.createdAtHeader,
                ]}
              >
                <Text>
                  {I18n.t(`${i18}.createdAt`)}
                </Text>
              </View>
              <View style={styles.openOrders.tableCellHeaderCancel}>
                <Text>
                  {I18n.t(`${i18}.cancel`)}
                </Text>
              </View>
            </View>
          </View>
        </JustOneRender>
        <View style={styles.openOrders.scrollBox}>
          <OpenOrders
            showActives={this.state.showActives}
            mainCurrency={this.props.mainCurrency}
            secondaryCurrency={this.props.secondaryCurrency}
          />
        </View>
      </View>
    )
  }
}

Index.propTypes = {
  mainCurrency: PropTypes.string.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
}

Index.defaultProps = {}

export default Index
