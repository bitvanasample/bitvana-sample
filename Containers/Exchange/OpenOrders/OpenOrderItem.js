import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import 'moment/locale/es'
import Big from '../../../Lib/Big'
import I18n from '../../../Lib/I18n'
import { View, A } from '../../../Components/Html'
import Text from '../Text'
import styles from '../../Styles/ExchangeStyles'
import Toast from '../../../Lib/Toast'
import AmountCurrency from '../../../Components/CryptoForm/AmountCurrency'
import Colors from '../../../Themes/Colors'
import JustOneRender from '../../../Components/JustOneRender'

const MARKET = 0

const styleSellText = {
  true: {
    ...styles.text,
    color: Colors.exchangeSell,
  },
  false: {
    ...styles.text,
    color: Colors.exchangeBuy,
  },
}
let locale = localStorage.getItem('i18nextLng') || 'en'
locale = locale.indexOf('es') !== -1 ? 'es' : 'en'
moment.locale(locale)
// const cancelString = I18n.t('exchange.openOrders.successCancel')
const errorString = I18n.t('exchange.openOrders.errorCancel')

class OpenOrderItem extends Component {
  constructor (props) {
    super(props)
    this.state = {
      loading: false,
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.props.remainingCurrency !== nextProps.remainingCurrency
      || this.state.loading !== nextState.loading
      || this.props.state !== nextProps.state
    )
  }

  render () {
    const createdAt = moment(this.props.createdAt * 1000)
      .format('DD/MM/YY HH:mm:ss')
    const averagePrice = this.props.acquiredCurrency > 0
      ? Big(this.props.acquiredMoney).div(this.props.acquiredCurrency)
      : 0
    const textCancel =
      this.state.loading
        ? (
          <Text
            style={styles.openOrders.textNote}
          >
            {I18n.t('exchange.openOrders.canceling')}
          </Text>
        )
        : (
          <A
            style={styles.text}
            onPress={() =>
              this.setState(
                { loading: true },
                () =>
                  this.props.cancelOrder(this.props.id)
                  // .then(() => {
                  //   // this.setState({ loading: false })
                  //   // Toast.show({
                  //   //   text: cancelString,
                  //   // })
                  // })
                    .catch(() => {
                      this.setState({ loading: false })
                      Toast.show({
                        text: errorString,
                      })
                    }),
              )
            }
          >
            {I18n.t('exchange.openOrders.cancel')}
          </A>
        )
    return (
      /* <HighlightOnUpdate
        style={styles.openOrders.item.container}
        keyForUpdate={this.props.remainingCurrency}
      > */
      <View style={styles.openOrders.item.container}>
        <JustOneRender>
          <View
            style={[
              styles.openOrders.tableCellText,
              styles.openOrders.typeData,
            ]}
          >
            <Text style={styles.openOrders.textNote}>
              {I18n.t(`exchange.openOrders.origin.${this.props.origin}`)} {
                this.props.sell
                  ? I18n.t('exchange.openOrders.sell')
                  : I18n.t('exchange.openOrders.buy')
              }
            </Text>
          </View>
          <View
            style={[
              styles.openOrders.tableCellAmount,
              styles.openOrders.limitPriceData,
            ]}
          >
            {
              MARKET === this.props.origin
                ? null
                : <AmountCurrency
                  fixed
                  style={styleSellText[this.props.sell]}
                  amount={this.props.limitPrice}
                  currency={this.props.mainCurrency}
                />
            }
          </View>
        </JustOneRender>
        <View
          style={[
            styles.openOrders.tableCellAmount,
            styles.openOrders.averagePriceData,
          ]}
        >
          {
            averagePrice
              ? <AmountCurrency
                fixed
                style={styleSellText[this.props.sell]}
                amount={averagePrice}
                currency={this.props.mainCurrency}
              />
              : null
          }
        </View>
        <JustOneRender>
          <View
            style={[
              styles.openOrders.tableCellAmount,
              styles.openOrders.totalCurrencyData,
            ]}
          >
            <AmountCurrency
              fixed
              style={styles.openOrders.textNote}
              amount={this.props.totalCurrency}
              currency={this.props.secondaryCurrency}
            />
          </View>
        </JustOneRender>
        <View
          style={[
            styles.openOrders.tableCellAmount,
            styles.openOrders.remainingCurrencyData,
          ]}
        >
          <AmountCurrency
            fixed
            style={styles.text}
            amount={this.props.remainingCurrency}
            currency={this.props.secondaryCurrency}
          />
        </View>
        <JustOneRender>
          <View
            style={[
              styles.openOrders.tableCell,
              styles.openOrders.createdAtData,
            ]}
          >
            <Text style={styles.openOrders.textNote}>
              {createdAt}
            </Text>
          </View>
        </JustOneRender>
        <View
          style={[
            styles.openOrders.tableCellCancel,
            styles.openOrders.cancelData,
          ]}
        >
          {
            (this.props.state === 0 || this.props.state === 2)
              ? textCancel
              : null
          }
        </View>
      </View>
      /* </HighlightOnUpdate> */
    )
  }
}

OpenOrderItem.propTypes = {
  mainCurrency: PropTypes.string.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  cancelOrder: PropTypes.func,
  id: PropTypes.string,
  origin: PropTypes.string,
  sell: PropTypes.bool,
  limitPrice: PropTypes.string,
  totalCurrency: PropTypes.string,
  remainingCurrency: PropTypes.string,
  createdAt: PropTypes.number,
  state: PropTypes.number,
  acquiredMoney: PropTypes.number,
  acquiredCurrency: PropTypes.number,
}

OpenOrderItem.defaultProps = {
  cancelOrder: () => {},
  id: '',
  origin: '',
  sell: false,
  limitPrice: 0,
  totalCurrency: 0,
  remainingCurrency: 0,
  createdAt: 0,
  state: 0,
  acquiredMoney: 0,
  acquiredCurrency: 1,
}

export default OpenOrderItem
