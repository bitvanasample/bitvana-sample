import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text } from '../../Components/Html'
import styles from '../Styles/ExchangeStyles'
import { cacheStyleGenerator } from '../../Lib/Perform'

const cacheStyle = cacheStyleGenerator(styles.text)
const cacheStyleNote = cacheStyleGenerator(styles.textNote)

class Title extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.children !== nextProps.children
  }

  render () {
    const style = this.props.note
      ? cacheStyleNote(this.props.style)
      : cacheStyle(this.props.style)
    return (
      <Text
        {...this.props}
        style={style}
      />
    )
  }
}

Title.propTypes = {
  style: PropTypes.shape({}),
  text: PropTypes.string,
  mustUpdate: PropTypes.bool,
  children: PropTypes.arrayOf(PropTypes.shape({})),
  note: PropTypes.bool,
}

Title.defaultProps = {
  note: false,
  style: {},
  text: '',
  mustUpdate: false,
  children: '',
}

export default Title
