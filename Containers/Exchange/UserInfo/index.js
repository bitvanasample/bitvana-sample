import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from '../../../Components/Html'
import Text from '../Text'
import { withMarkets } from '../../../GraphQl/Markets/MarketsQuery'
import styles from '../../Styles/ExchangeStyles'
import Title from '../Title'
import I18n from '../../../I18n/I18n'
import RenderConditional, { basicComparation }
  from '../../../Components/RenderConditional'
import Balances from './Balances'

class UserInfo extends Component {
  shouldComponentUpdate (nextProps) {
    if (this.props.loading !== nextProps.loading) {
      return true
    }
    const marketCode =
      `${this.props.secondaryCurrency}/${this.props.mainCurrency}`
    const nextMarketCode =
      `${nextProps.secondaryCurrency}/${nextProps.mainCurrency}`
    const market = this.props.findMarket(marketCode)
    const nextMarket = nextProps.findMarket(nextMarketCode)
    return (marketCode !== nextMarketCode
      || market.commissionTaker !== nextMarket.commissionTaker
      || market.commissionMaker !== nextMarket.commissionMaker
    )
  }

  _market () {
    const { mainCurrency, secondaryCurrency, findMarket } = this.props
    const market = `${secondaryCurrency}/${mainCurrency}`
    return findMarket(market)
  }

  _commissions () {
    const { commissionTaker, commissionMaker } = this._market()
    return [
      <RenderConditional
        key='commissionTaker'
        x={commissionTaker}
        condition={basicComparation}
      >
        <View style={styles.balances.row}>
          <Text style={styles.balances.note}>
            {I18n.t('exchange.balances.commissionTaker')}
          </Text>
          <Text style={styles.balances.note}>
            { commissionTaker
              ? commissionTaker * 100
              : '-'
            } %
          </Text>
        </View>
      </RenderConditional>,
      <RenderConditional
        key='commissionMaker'
        x={commissionMaker}
        condition={basicComparation}
      >
        <View style={styles.balances.row}>
          <Text style={styles.balances.note}>
            {I18n.t('exchange.balances.commissionMaker')}
          </Text>
          <Text style={styles.balances.note}>
            {
              commissionMaker
                ? commissionMaker * 100
                : '-'
            } %
          </Text>
        </View>
      </RenderConditional>,
    ]
  }

  render () {
    return (
      <View style={styles.balances.container}>
        <Title
          withHeader={this.props.withHeader}
          text={I18n.t('exchange.balances.title')}
        />
        <Balances
          mainCurrency={this.props.mainCurrency}
          secondaryCurrency={this.props.secondaryCurrency}
        />
        { this._commissions() }
      </View>
    )
  }
}

UserInfo.propTypes = {
  withHeader: PropTypes.bool,
  mainCurrency: PropTypes.string.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  findMarket: PropTypes.func.isRequired,
  loading: PropTypes.bool,
}

UserInfo.defaultProps = {
  loading: false,
  withHeader: false,
}

export default withMarkets(UserInfo)
