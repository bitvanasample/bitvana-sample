import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import { withBalances } from '../../../GraphQl/Wallets/BalancesQuery'
import { withUserInfo } from '../../../GraphQl/Users/UserInfoQuery'
import Indicator from '../../../QuanticComponents/Indicator'
import Balance from './Balance'

class Balances extends Component {
  shouldComponentUpdate (nextProps) {
    if (
      this.props.loading !== nextProps.loading
      || this.props.mainCurrency !== nextProps.mainCurrency
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
    ) {
      return true
    }
    const mainBalanceOld = this.props.balance(this.props.mainCurrency)
      .availableBalance
    const mainBalanceNew = nextProps.balance(nextProps.mainCurrency)
      .availableBalance
    const secondaryBalanceOld = this.props.balance(this.props.secondaryCurrency)
      .availableBalance
    const secondaryBalanceNew = nextProps.balance(nextProps.secondaryCurrency)
      .availableBalance
    return (
      mainBalanceOld !== mainBalanceNew
      || secondaryBalanceOld !== secondaryBalanceNew
    )
  }

  componentDidUpdate (prevProps) {
    const { loading: prevLoading } = prevProps
    const { loading: currentLoading } = this.props
    if (prevLoading && !currentLoading) {
      if (this.subscription) { this.subscription() }
      this.subscription = this.props.subscriptionToNewBalance()
    }
  }

  componentWillUnmount () {
    if (this.subscription) { this.subscription() }
  }

  render () {
    const mainBalance = this.props
      .balance(this.props.mainCurrency).availableBalance
    const secondaryBalance = this.props
      .balance(this.props.secondaryCurrency).availableBalance
    if (this.props.loading) {
      return <Indicator />
    }
    return [
      <Balance
        key='mainBalance'
        availableBalance={mainBalance}
        code={this.props.mainCurrency}
      />,
      <Balance
        key='secondaryBalance'
        availableBalance={secondaryBalance}
        code={this.props.secondaryCurrency}
      />,
    ]
  }
}

Balances.propTypes = {
  // subscribeToMarkets: PropTypes.func.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  balance: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  subscriptionToNewBalance: PropTypes.func.isRequired,
}

Balances.defaultProps = {
  loading: false,
}

export default compose(
  withUserInfo,
  withBalances,
)(Balances)
