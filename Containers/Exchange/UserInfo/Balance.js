import React, { Component } from 'react'
import PropTypes from 'prop-types'
import AmountCurrency from '../../../Components/CryptoForm/AmountCurrency'
import { View } from '../../../Components/Html'
import Text from '../Text'
import styles from '../../Styles/ExchangeStyles'

class Balance extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.code !== nextProps.code
    || this.props.availableBalance !== nextProps.availableBalance
  }

  render () {
    const { code, availableBalance } = this.props
    return (
      /* <HighlightOnUpdate keyForUpdate={availableBalance}> */
      <View style={styles.balances.row}>
        <Text style={styles.balances.text}>
          {code}
        </Text>
        <Text style={styles.balances.text}>
          <AmountCurrency
            fixed
            style={styles.balances.text}
            amount={availableBalance}
            currency={code}
          />
        </Text>
      </View>
      /* </HighlightOnUpdate> */
    )
  }
}

Balance.propTypes = {
  code: PropTypes.string.isRequired,
  availableBalance: PropTypes.number.isRequired,
}

export default Balance
