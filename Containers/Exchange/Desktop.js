import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from '../../Components/Html'
import styles from '../Styles/ExchangeStyles'
import AppBar from '../../Components/AppBar/Advanced'
import OrderBook from './OrderBook'
import OrderForm from './OrderForm'
import History from './History'
import UserInfo from './UserInfo'
import Chart from './Chart'
import OpenOrders from './OpenOrders'

const flexStyles = {
  flexColumn: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  flexRow: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
  },
  flex1: {
    flex: 1,
  },
  flex2: {
    flex: 2,
  },
  flex3: {
    flex: 3,
  },
  flex4: {
    flex: 3,
  },
  flex5: {
    flex: 5,
  },
  flex6: {
    flex: 5,
  },
  flex7: {
    flex: 7,
  },
  orderForm: {
    height: 240,
  },
}

class Desktop extends Component {
  shouldComponentUpdate (nextProps) {
    return (
      this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
    )
  }

  render () {
    const params = {
      secondaryCurrency: this.props.secondaryCurrency,
      mainCurrency: this.props.mainCurrency,
    }
    return (
      <View style={styles.mainDesktop}>
        <AppBar {...params} />
        <View style={flexStyles.flexRow}>
          <View style={[flexStyles.flexColumn, flexStyles.flex7]}>
            <View style={flexStyles.flex5}>
              <Chart {...params} />
            </View>
            <View style={flexStyles.flex3}>
              <OpenOrders {...params} />
            </View>
          </View>
          <View style={[flexStyles.flexColumn, flexStyles.flex5]}>
            <View style={[flexStyles.flexRow, flexStyles.flex1]}>
              <View style={flexStyles.flex1}>
                <View style={flexStyles.flex1}>
                  <OrderBook {...params} />
                </View>
              </View>
              <View style={flexStyles.flexColumn}>
                <View style={flexStyles.flex1}>
                  <History {...params} />
                </View>
                <View>
                  <UserInfo {...params} />
                </View>
              </View>
            </View>
            <View style={flexStyles.orderForm}>
              <OrderForm {...params} />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

Desktop.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}

export default Desktop
