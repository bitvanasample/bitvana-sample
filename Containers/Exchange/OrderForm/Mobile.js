import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Tab, Tabs } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'
import CondensedForm from './CondensedForm'
import Market from './Market'
import Limit from './Limit'

class OrderForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tabIndex: 0,
      buying: true,
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.state.tabIndex !== nextState.tabIndex
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
      || this.state.buying !== nextState.buying
    )
  }

  handleChange (value) {
    this.setState({
      tabIndex: value,
    })
  }

  render () {
    return (
      <View style={styles.orderForm.container}>
        <View style={styles.orderForm.tabWrapper}>
          <Tabs
            forceDesktopStyle
            style={styles.orderForm.tabContainer}
            onChange={value => this.handleChange(value)}
            value={this.state.tabIndex}
            tabs={[
              <Tab
                textStyle={styles.orderForm.textStyle}
                activeTabStyle={styles.orderForm.activeTab}
                tabStyle={styles.orderForm.tab}
                value={0}
              >
                Market
              </Tab>,
              <Tab
                textStyle={styles.orderForm.textStyle}
                activeTabStyle={styles.orderForm.activeTab}
                tabStyle={styles.orderForm.tab}
                value={1}
              >
                Limit
              </Tab>,
            ]}
            tabContent={[
              <CondensedForm
                comp={Market}
                onChange={() => this.setState({ buying: true })}
                secondaryCurrency={this.props.secondaryCurrency}
                mainCurrency={this.props.mainCurrency}
              />,
              <CondensedForm
                comp={Limit}
                onChange={() => this.setState({ buying: true })}
                secondaryCurrency={this.props.secondaryCurrency}
                mainCurrency={this.props.mainCurrency}
              />,
            ]}
            tabContentStyle={styles.orderForm.tabContentContainer}
            tabContentIndex={this.state.tabIndex}
          />
        </View>
      </View>
    )
  }
}


OrderForm.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}

OrderForm.defaultProps = {}

export default OrderForm
