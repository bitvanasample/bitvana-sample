import React, { Component } from 'react'
import PropTypes from 'prop-types'
import I18n from '../../../Lib/I18n'
import { H6 } from '../../../Components/Html'
import { withMutation } from '../../../GraphQl/Orders/CreateStopOrderMutation'
import styles from '../../Styles/ExchangeStyles'
import { AmountFieldInput } from '../../../Components/CryptoForm'
import _Form from './_Form'
import RenderConditional from '../../../Components/RenderConditional'

const Form = withMutation(_Form)

const fieldCondition = (a, b) => (
  a.value !== b.value
  || a.currencyCode !== b.currencyCode
)

const paddingMarginZero = { padding: 0, margin: 0 }

class Stop extends Component {
  constructor (props) {
    super(props)
    this.initialState = {
      amount: 0,
      params: [0, 0], // [limitPrice, stopPrice]
      limitPriceText: 0,
      limitStopPrice: 0,
      textAmount: 0,
    }
    this.state = this.initialState
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.state.params
        .map((x, i) => x !== nextState.params[i])
        .reduce((x, y) => x || y, false)
      || this.state.amount !== nextState.amount
      || this.state.textAmount !== nextState.textAmount
      || this.state.limitPriceText !== nextState.limitPriceText
      || this.state.limitStopPrice !== nextState.limitStopPrice
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
    )
  }

  render () {
    return (
      <Form
        onSuccess={() => this.setState(this.initialState)}
        type='stop'
        i18nExplanation='Limit'
        amount={this.state.amount}
        canCreateOrder={() =>
          this.state.amount
          && parseFloat(this.state.amount)
          && this.state.params[0]
          && parseFloat(this.state.params[0])
          && this.state.params[1]
          && parseFloat(this.state.params[1])
        }
        extraParamForUpdate={[
          this.state.limitStopPrice,
          this.state.limitPriceText,
          this.state.textAmount,
        ]}
        params={this.state.params}
        mainCurrency={this.props.mainCurrency}
        secondaryCurrency={this.props.secondaryCurrency}
        changeBuying={() => this.setState(this.initialState)}
        aproxCurrencyAndMoneyFromData={(data) => {
          const info = data.putStopOrder || {}
          return {
            aproxCurrency: info.x || 0,
            aproxMoney: info.x || 0,
          }
        }}
        textOnSuccess={I18n.t('exchange.orderForm.textOnSuccessStop')}
      >
        <RenderConditional simple x={this.props.mainCurrency}>
          <H6 style={paddingMarginZero}>
            {I18n.t(
              'exchange.orderForm.stopPrice',
              { code: this.props.mainCurrency },
            )}
          </H6>
        </RenderConditional>
        <RenderConditional
          value={this.state.limitStopPrice}
          currencyCode={this.props.mainCurrency}
          condition={fieldCondition}
        >
          <AmountFieldInput
            style={styles.flex}
            onChange={(limitStopPrice, stopPrice) =>
              this.setState({
                params: [this.state.params[0], stopPrice],
                limitStopPrice,
              })
            }
            value={this.state.limitStopPrice}
            unit={this.props.mainCurrency}
            currencyCode={this.props.mainCurrency}
          />
        </RenderConditional>
        <RenderConditional simple x={this.props.secondaryCurrency}>
          <H6 style={paddingMarginZero}>
            {I18n.t(
              'exchange.orderForm.limitPrice',
              { code: this.props.secondaryCurrency },
            )}
          </H6>
        </RenderConditional>
        <RenderConditional
          value={this.state.limitPriceText}
          currencyCode={this.props.mainCurrency}
          condition={fieldCondition}
        >
          <AmountFieldInput
            style={styles.flex}
            onChange={(limitPriceText, limitPrice) =>
              this.setState({
                params: [limitPrice, this.state.params[1]],
                limitPriceText,
              })
            }
            value={this.state.limitPriceText}
            unit={this.props.mainCurrency}
            currencyCode={this.props.mainCurrency}
          />
        </RenderConditional>
        <RenderConditional simple x={this.props.secondaryCurrency}>
          <H6 style={paddingMarginZero}>
            {I18n.t(
              'exchange.orderForm.amount',
              { code: this.props.secondaryCurrency },
            )}
          </H6>
        </RenderConditional>
        <RenderConditional
          value={this.state.textAmount}
          currencyCode={this.props.secondaryCurrency}
          condition={fieldCondition}
        >
          <AmountFieldInput
            style={styles.flex}
            onChange={(textAmount, amount) =>
              this.setState({ textAmount, amount })
            }
            value={this.state.textAmount}
            unit={this.props.secondaryCurrency}
            currencyCode={this.props.secondaryCurrency}
          />
        </RenderConditional>
      </Form>
    )
  }
}


Stop.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}

Stop.defaultProps = {
}

export default Stop
