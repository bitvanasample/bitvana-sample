import React, { Component } from 'react'
import PropTypes from 'prop-types'
import I18n from '../../../Lib/I18n'
import Text from '../Text'
import { withMutation } from '../../../GraphQl/Orders/CreateLimitOrderMutation'
import styles from '../../Styles/ExchangeStyles'
import { AmountFieldInput } from '../../../Components/CryptoForm'
import _Form from './_Form'
import RenderConditional from '../../../Components/RenderConditional'

const Form = withMutation(_Form)

const paddingMarginZero = {
  padding: 0,
  margin: 0,
  marginTop: 6,
  textAlign: 'left',
}

const containerStyle = {
  margin: 0,
  marginTop: 0,
  marginBottom: 0,
  marginLeft: 0,
  marginRight: 0,
}

const fieldCondition = (a, b) => (
  a.value !== b.value
  || a.currencyCode !== b.currencyCode
)

class Limit extends Component {
  constructor (props) {
    super(props)
    this.initialState = {
      amount: 0,
      params: [0], // [limitPrice]
      limitPriceText: 0,
      textAmount: 0,
    }
    this.state = this.initialState
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.state.params
        .map((x, i) => x !== nextState.params[i])
        .reduce((x, y) => x || y, false)
      || this.state.amount !== nextState.amount
      || this.state.textAmount !== nextState.textAmount
      || this.state.limitPriceText !== nextState.limitPriceText
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
    )
  }

  render () {
    return (
      <Form
        buying={this.props.buying}
        onSuccess={() => this.setState(this.initialState)}
        type='limit'
        i18nExplanation='Limit'
        amount={this.state.amount}
        canCreateOrder={() =>
          this.state.amount
          && parseFloat(this.state.amount)
          && this.state.params[0]
          && parseFloat(this.state.params[0])
        }
        extraParamForUpdate={[
          this.state.textAmount,
          this.state.limitPriceText,
        ]}
        params={this.state.params}
        mainCurrency={this.props.mainCurrency}
        secondaryCurrency={this.props.secondaryCurrency}
        aproxCurrencyAndMoneyFromData={(data) => {
          const info = data.putLimitOrder || {}
          return {
            aproxCurrency: info.totalCurrency,
            aproxMoney: info.limitPrice * info.totalCurrency,
          }
        }}
        textOnSuccess={I18n.t('exchange.orderForm.textOnSuccessLimit')}
      >
        <RenderConditional simple x={this.props.secondaryCurrency}>
          <Text style={paddingMarginZero}>
            {I18n.t(
              'exchange.orderForm.limitPrice',
              { code: this.props.secondaryCurrency },
            )}
          </Text>
        </RenderConditional>
        <RenderConditional
          value={this.state.limitPriceText}
          currencyCode={this.props.mainCurrency}
          condition={fieldCondition}
        >
          <AmountFieldInput
            containerStyle={containerStyle}
            style={styles.orderForm.fieldInput}
            onChange={(limitPriceText, limitPrice) =>
              this.setState({ limitPriceText, params: [limitPrice] })
            }
            value={this.state.limitPriceText}
            unit={this.props.mainCurrency}
            currencyCode={this.props.mainCurrency}
            help
          />
        </RenderConditional>
        <RenderConditional simple x={this.props.secondaryCurrency}>
          <Text style={paddingMarginZero}>
            {I18n.t(
              'exchange.orderForm.amount',
              { code: this.props.secondaryCurrency },
            )}
          </Text>
        </RenderConditional>
        <RenderConditional
          value={this.state.textAmount}
          currencyCode={this.props.secondaryCurrency}
          condition={fieldCondition}
        >
          <AmountFieldInput
            containerStyle={containerStyle}
            style={styles.orderForm.fieldInput}
            onChange={(textAmount, amount) =>
              this.setState({ textAmount, amount })
            }
            value={this.state.textAmount}
            unit={this.props.secondaryCurrency}
            currencyCode={this.props.secondaryCurrency}
          />
        </RenderConditional>
      </Form>
    )
  }
}


Limit.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  buying: PropTypes.bool.isRequired,
}

Limit.defaultProps = {
}

export default Limit
