import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Tab, Tabs } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'
import Market from './Market'
import Limit from './Limit'

class OrderForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      tabIndex: 0,
    }
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.state.tabIndex !== nextState.tabIndex
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
    )
  }

  handleChange (value) {
    this.setState({
      tabIndex: value,
    })
  }

  formFor (Comp) {
    return (
      <View style={{
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'stretch',
      }}
      >
        <View style={{ display: 'flex', flex: 1 }} />
        <View style={{
          flex: 1, display: 'flex', maxWidth: 230, minWidth: 210,
        }}
        >
          <Comp
            secondaryCurrency={this.props.secondaryCurrency}
            mainCurrency={this.props.mainCurrency}
            buying
          />
        </View>
        <View
          style={{
            display: 'flex',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            width: '100%',
          }}
        />
        <View
          style={{
            display: 'flex',
            flex: 1,
            width: 1,
            maxWidth: 1,
            backgroundColor: '#484c54',
          }}
        />
        <View
          style={{
            display: 'flex',
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            height: 'inheritance',
            width: '100%',
          }}
        />
        <View style={{
          flex: 1, display: 'flex', maxWidth: 230, minWidth: 210,
        }}
        >
          <Comp
            secondaryCurrency={this.props.secondaryCurrency}
            mainCurrency={this.props.mainCurrency}
            buying={false}
          />
        </View>
        <View style={{ display: 'flex', flex: 1 }} />
      </View>
    )
  }

  render () {
    return (
      <View style={styles.orderForm.container}>
        <View style={styles.orderForm.tabWrapper}>
          <Tabs
            forceDesktopStyle
            style={styles.orderForm.tabContainer}
            onChange={value => this.handleChange(value)}
            value={this.state.tabIndex}
            tabs={[
              <Tab
                textStyle={styles.orderForm.textStyle}
                activeTabStyle={styles.orderForm.activeTab}
                tabStyle={styles.orderForm.tab}
                value={0}
              >
                Market
              </Tab>,
              <Tab
                textStyle={styles.orderForm.textStyle}
                activeTabStyle={styles.orderForm.activeTab}
                tabStyle={styles.orderForm.tab}
                value={1}
              >
                Limit
              </Tab>,
            ]}
            tabContent={[
              this.formFor(Market),
              this.formFor(Limit),
            ]}
            tabContentStyle={styles.orderForm.tabContentContainer}
            tabContentIndex={this.state.tabIndex}
          />
        </View>
      </View>
    )
  }
}


OrderForm.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}

OrderForm.defaultProps = {}

export default OrderForm
