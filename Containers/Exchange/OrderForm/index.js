import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Media from 'react-media'
import Mobile from './Mobile'
import Desktop from './Desktop'

class Exchange extends Component {
  shouldComponentUpdate (nextProps) {
    return (this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
    )
  }

  render () {
    return (
      <Media query='(max-width: 599px)'>
        {isSmall => (isSmall
          ? <Mobile
            secondaryCurrency={this.props.secondaryCurrency}
            mainCurrency={this.props.mainCurrency}
          />
          : <Desktop
            secondaryCurrency={this.props.secondaryCurrency}
            mainCurrency={this.props.mainCurrency}
          />
        )}
      </Media>
    )
  }
}

Exchange.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}

Exchange.defaultProps = {}

export default Exchange
