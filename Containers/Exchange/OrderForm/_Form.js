import React, { Component } from 'react'
import PropTypes from 'prop-types'
import I18n from '../../../Lib/I18n'
import { View, Button } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'
import Toast from '../../../Lib/Toast'
import Indicator from '../../../QuanticComponents/Indicator'
import Explanation from './Explanation'
import RenderConditional from '../../../Components/RenderConditional'
import JustOneRender from '../../../Components/JustOneRender'
import LoginRequired from '../../../Components/CryptoForm/LoginRequired'

const styleButtonForm = {
  true: styles.orderForm.buyButton,
  false: styles.orderForm.sellButton,
}

const textButtonForm = {
  true: I18n.t('exchange.orderForm.makeBuy'),
  false: I18n.t('exchange.orderForm.makeSell'),
}

const margin5 = { margin: 5, marginBottom: 10 }

const basicComparation = (a, b) => a.x !== b.x
// const explanationComparation = (a, b) => {
//   const toreturn = a.st.buying !== b.st.buying
//   || a.st.aproxCurrency !== b.st.aproxCurrency
//   || a.st.aproxMoney !== b.st.aproxMoney
//   || a.ps.mainCurrency !== b.ps.mainCurrency
//   || a.ps.secondaryCurrency !== b.ps.secondaryCurrency
//   console.tron.log(toreturn)
//   if (toreturn) {
//     console.tron.log(a.st)
//     console.tron.log(b.st)
//   }
//   return toreturn
// }

class Form extends Component {
  constructor (props) {
    super(props)
    this.initialState = {
      loadingHelp: false,
      aproxCurrency: 0,
      aproxMoney: 0,
    }
    this.state = this.initialState
    this.props.changeBuying(this.props.buying)
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.props.extraParamForUpdate
        .map((x, i) => x !== nextProps.extraParamForUpdate[i])
        .reduce((x, y) => x || y, false)
      || this.props.params
        .map((x, i) => x !== nextProps.params[i])
        .reduce((x, y) => x || y, false)
      || this.state.loadingHelp !== nextState.loadingHelp
      || this.props.buying !== nextProps.buying
      || this.state.aproxCurrency !== nextState.aproxCurrency
      || this.state.aproxMoney !== nextState.aproxMoney
      || this.props.amount !== nextProps.amount
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
    )
  }

  componentDidUpdate (prevProps) {
    if (
      (
        prevProps.amount !== this.props.amount
        || prevProps.params !== this.props.params
      )
      && (
        this.props.amount && this.props.amount > 0
        && this.props.canCreateOrder()
      )
    ) {
      const params = [
        this.props.secondaryCurrency,
        this.props.mainCurrency,
        ...this.props.params,
        this.props.amount,
        false,
      ]
      const funcCreateOrder = this.props.buying
        ? this.props.createOrderBuy
        : this.props.createOrderSell
      clearTimeout(this.calculateAproxAmount)
      this.calculateAproxAmount = setTimeout(() => {
        this.setState({
          loadingHelp: true,
        }, () => {
          funcCreateOrder(...params)
            .then(({ data }) => {
              this._onSuccessOrderFalse(data)
            })
            .catch((e) => {
              this._onErrorOrderFalse(e)
            })
        })
      }, 500)
    }
  }

  _onSuccessOrderFalse (data) {
    this.setState({
      ...this.props.aproxCurrencyAndMoneyFromData(data),
      loadingHelp: false,
    })
  }

  _onErrorOrderFalse (e) {
    // UnauthorizedException
    if (e.message.toString().indexOf('Network error') !== -1) {
      Toast.show({ text: I18n.t('errors.unauthorized'), type: 'error' })
      this.setState({ loadingHelp: false })
    } else {
      Toast.show({ text: e, type: 'error' })
      this.setState({ loadingHelp: false })
    }
  }

  _onSuccessOrderTrue () {
    this.setState({ ...this.initialState })
    this.props.onSuccess()
    // Toast.show({ text: this.props.textOnSuccess })
  }

  _onErrorOrderTrue (e) {
    this.setState({ loadingHelp: false })
    Toast.show({ text: e, type: 'error' })
  }

  _createOrder () {
    clearTimeout(this.calculateAproxAmount)
    if (this.props.canCreateOrder()) {
      this.setState({
        loadingHelp: true,
      }, () => {
        const params = [
          this.props.secondaryCurrency,
          this.props.mainCurrency,
          ...this.props.params,
          this.props.amount,
          true,
        ]
        if (this.props.buying) {
          this.props.createOrderBuy(...params)
            .then(() => {
              clearTimeout(this.calculateAproxAmount)
              this._onSuccessOrderTrue()
            })
            .catch((e) => {
              clearTimeout(this.calculateAproxAmount)
              this._onErrorOrderTrue(e)
            })
        } else {
          this.props.createOrderSell(...params)
            .then(() => {
              clearTimeout(this.calculateAproxAmount)
              this._onSuccessOrderTrue()
            })
            .catch((e) => {
              clearTimeout(this.calculateAproxAmount)
              this._onErrorOrderTrue(e)
            })
        }
      })
    }
  }

  render () {
    return (
      <View style={styles.flexColumn}>
        {
          this.props.children
        }
        {/* <RenderConditional
          ps={this.props}
          st={this.state}
          condition={explanationComparation}
        > */}
        {
          this.state.loadingHelp
            ? (
              <JustOneRender>
                <div style={margin5}>
                  <Indicator size='small' />
                </div>
              </JustOneRender>
            )
            : <Explanation
              type={this.props.type}
              i18n={this.props.i18nExplanation}
              buying={this.props.buying}
              aproxCurrency={this.state.aproxCurrency}
              aproxMoney={this.state.aproxMoney}
              amount={this.props.amount}
              mainCurrency={this.props.mainCurrency}
              secondaryCurrency={this.props.secondaryCurrency}
            />
        }
        {/* </RenderConditional> */}
        {
          <RenderConditional
            x={this.props.buying}
            condition={basicComparation}
          >
            <LoginRequired
              approvalLevel={1}
              style={styles.orderForm.textStyle}
              component={
                <Button
                  textStyle={styles.orderForm.textStyle}
                  style={styleButtonForm[this.props.buying]}
                  onPress={() => this._createOrder()}
                >{ textButtonForm[this.props.buying] }
                </Button>
              }
            />
          </RenderConditional>
        }
      </View>
    )
  }
}

Form.propTypes = {
  onSuccess: PropTypes.func,
  type: PropTypes.oneOf(['limit', 'market', 'stop']).isRequired,
  changeBuying: PropTypes.func,
  canCreateOrder: PropTypes.func.isRequired,
  params: PropTypes.arrayOf(PropTypes.number).isRequired,
  extraParamForUpdate: PropTypes.arrayOf(PropTypes.number).isRequired,
  i18nExplanation: PropTypes.string,
  amount: PropTypes.number.isRequired,
  children: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  createOrderBuy: PropTypes.func.isRequired,
  createOrderSell: PropTypes.func.isRequired,
  // textOnSuccess: PropTypes.string.isRequired,
  aproxCurrencyAndMoneyFromData: PropTypes.func.isRequired,
  buying: PropTypes.bool.isRequired,
}

Form.defaultProps = {
  onSuccess: () => {},
  i18nExplanation: 'Market', // Market || Limit
  changeBuying: () => {},
}

export default Form
