import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Big from '../../../Lib/Big'
import I18n from '../../../Lib/I18n'
import { withCurrencies } from '../../../GraphQl/Currencies/CurrenciesQuery'
import Text from '../Text'

const styleTextAlignCenter = {
  textAlign: 'justify',
  marginBottom: 3,
  marginTop: 4,
}

Big.RM = 0
/* TODO: validation using this.props.currency */
class Explanation extends Component {
  // shouldComponentUpdate (nextProps) {
  //   return (
  //     this.props.secondaryCurrency !== nextProps.secondaryCurrency
  //     || this.props.mainCurrency !== nextProps.mainCurrency
  //     || this.props.aproxCurrency !== nextProps.aproxCurrency
  //     || this.props.aproxMoney !== nextProps.aproxMoney
  //     || this.props.buying !== nextProps.buying
  //   )
  // }

  render () {
    const codeCurrency = this.props.secondaryCurrency
    const codeMoney = this.props.mainCurrency
    const {
      units: units1,
    } = this.props.findCurrency(codeMoney)
    const {
      units: units2,
    } = this.props.findCurrency(codeCurrency)
    const units1Int = parseInt(units1, 10) || 0
    const units2Int = parseInt(units2, 10) || 0
    let aproxCurrency = 0
    let aproxMoney = 0
    if (this.props.type === 'market') {
      aproxCurrency = Big(this.props.aproxCurrency).toFixed(units2Int)
      aproxMoney = Big(this.props.aproxMoney).toFixed(units1Int)
    } else if (this.props.type === 'stop') { /* TODO */
      aproxCurrency = Big(this.props.aproxCurrency).toFixed(units2Int)
      aproxMoney = Big(this.props.aproxMoney).toFixed(units1Int)
    } else { // limit
      aproxCurrency = Big(this.props.aproxCurrency).toFixed(units2Int)
      aproxMoney = Big(this.props.aproxMoney).toFixed(units1Int)
    }
    return (
      <Text style={styleTextAlignCenter}>
        {
          this.props.buying
            ? I18n.t(
              `exchange.orderForm.explanation${this.props.i18n}Buy`,
              {
                aproxCurrency,
                aproxMoney,
                codeCurrency,
                codeMoney,
              },
            )
            : I18n.t(
              `exchange.orderForm.explanation${this.props.i18n}Sell`,
              {
                aproxCurrency,
                aproxMoney,
                codeCurrency,
                codeMoney,
              },
            )
        }
      </Text>
    )
  }
}

Explanation.propTypes = {
  type: PropTypes.oneOf(['limit', 'market', 'stop']).isRequired,
  i18n: PropTypes.string.isRequired,
  findCurrency: PropTypes.func.isRequired,
  mainCurrency: PropTypes.string,
  secondaryCurrency: PropTypes.string,
  buying: PropTypes.bool,
  aproxCurrency: PropTypes.number,
  aproxMoney: PropTypes.number,
}

Explanation.defaultProps = {
  mainCurrency: '',
  secondaryCurrency: '',
  buying: true,
  aproxCurrency: 0,
  aproxMoney: 0,
}

export default withCurrencies(Explanation)
