import React, { Component } from 'react'
import PropTypes from 'prop-types'
import I18n from '../../../Lib/I18n'
import Text from '../Text'
import { withMutation } from '../../../GraphQl/Orders/CreateMarketOrderMutation'
import styles from '../../Styles/ExchangeStyles'
import { AmountFieldInput } from '../../../Components/CryptoForm'
import _Form from './_Form'
import RenderConditional from '../../../Components/RenderConditional'

const Form = withMutation(_Form)

const h6Style = {
  padding: 0,
  margin: 0,
  marginTop: 6,
  textAlign: 'left',
}

const containerStyle = {
  margin: 0,
}

class Market extends Component {
  constructor (props) {
    super(props)
    this.initialState = {
      textAmount: 0,
      amount: 0,
      params: [],
    }
    this.state = this.initialState
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.state.params
        .map((x, i) => x !== nextState.params[i])
        .reduce((x, y) => x || y, false)
      || this.props.buying !== nextProps.buying
      || this.state.amount !== nextState.amount
      || this.state.textAmount !== nextState.textAmount
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
    )
  }

  render () {
    return (
      <Form
        buying={this.props.buying}
        onSuccess={() => this.setState({ ...this.initialState })}
        type='market'
        i18nExplanation='Market'
        amount={this.state.amount}
        canCreateOrder={() =>
          this.state.amount
          && parseFloat(this.state.amount)
        }
        extraParamForUpdate={[this.state.textAmount]}
        params={this.state.params}
        mainCurrency={this.props.mainCurrency}
        secondaryCurrency={this.props.secondaryCurrency}
        aproxCurrencyAndMoneyFromData={(data) => {
          const info = data.putMarketOrder || {}
          return {
            aproxCurrency: info.acquiredCurrency,
            aproxMoney: info.acquiredMoney,
          }
        }}
        textOnSuccess={I18n.t('exchange.orderForm.textOnSuccessMarket')}
      >
        <RenderConditional
          simple
          x={this.props.buying}
        >
          <Text style={h6Style}>
            {I18n.t(
              'exchange.orderForm.amount',
              {
                code: this.props.buying
                  ? this.props.mainCurrency
                  : this.props.secondaryCurrency,
              },
            )}
          </Text>
        </RenderConditional>
        <AmountFieldInput
          containerStyle={containerStyle}
          currencyCode={this.props.buying
            ? this.props.mainCurrency
            : this.props.secondaryCurrency}
          onChange={
            (textAmount, amount) => this.setState({ textAmount, amount })
          }
          value={this.state.textAmount}
          style={styles.orderForm.fieldInput}
          unit={this.props.buying
            ? this.props.mainCurrency
            : this.props.secondaryCurrency}
        />
      </Form>
    )
  }
}


Market.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  buying: PropTypes.bool.isRequired,
}

Market.defaultProps = {
}

export default Market
