import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Button } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'
import RenderConditional from '../../../Components/RenderConditional'
import I18n from '../../../Lib/I18n'

const basicComparation = (a, b) => a.x !== b.x

const flexBoxSpaceBetween = {
  ...styles.flexRow,
  justifyContent: 'space-between',
}

const flex1Margin5 = { flex: 1 }

const styleButtonBuy = {
  true: styles.orderForm.buyButton,
  false: {
    ...styles.orderForm.buyButton,
    ...styles.orderForm.noFocus,
  },
}

const styleButtonSell = {
  true: {
    ...styles.orderForm.sellButton,
    ...styles.orderForm.noFocus,
  },
  false: styles.orderForm.sellButton,
}

class CondensedForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
      buying: true,
    }
    this._handleChange = buying =>
      this.props.onChange(buying)
  }

  shouldComponentUpdate (nextProps, nextState) {
    return (
      this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
      || this.state.buying !== nextState.buying
    )
  }

  handleChange (buying) {
    this.setState({ buying }, this._handleChange)
  }


  tabBuySell () {
    return (
      <RenderConditional
        x={this.state.buying}
        condition={basicComparation}
      >
        <View style={flexBoxSpaceBetween}>
          <View style={flex1Margin5}>
            <Button
              textStyle={styles.orderForm.textStyle}
              style={styleButtonBuy[this.state.buying]}
              onPress={() => this.handleChange(true)}
            >
              {I18n.t('exchange.orderForm.buy')}
            </Button>
          </View>
          <View style={flex1Margin5}>
            <Button
              textStyle={styles.orderForm.textStyle}
              style={styleButtonSell[this.state.buying]}
              onPress={() => this.handleChange(false)}
            >
              {I18n.t('exchange.orderForm.sell')}
            </Button>
          </View>
        </View>
      </RenderConditional>
    )
  }

  render () {
    const Form = this.props.comp
    return (
      <View style={styles.flexColumn}>
        {this.tabBuySell()}
        <Form
          secondaryCurrency={this.props.secondaryCurrency}
          mainCurrency={this.props.mainCurrency}
          buying={this.state.buying}
        />
      </View>
    )
  }
}

CondensedForm.propTypes = {
  onChange: PropTypes.func,
  comp: PropTypes.node.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}


CondensedForm.defaultProps = {
  onChange: () => {},
}

export default CondensedForm
