import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Text } from '../../Components/Html'
import styles from '../Styles/ExchangeStyles'
import { isMobile } from '../../Config/AppConfig'

class Title extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.text !== nextProps.text
  }

  render () {
    if (isMobile && !this.props.withHeader) {
      return null
    }
    return (
      <View style={[this.props.style, styles.title.container]}>
        <Text style={styles.title.title}>{this.props.text}</Text>
      </View>
    )
  }
}


Title.propTypes = {
  withHeader: PropTypes.bool,
  style: PropTypes.shape({}),
  text: PropTypes.string,
}

Title.defaultProps = {
  withHeader: false,
  style: {},
  text: '',
}

export default Title
