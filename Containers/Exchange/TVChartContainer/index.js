/* eslint-disable */
import * as React from 'react'
// import './index.css'

function getLanguageFromURL () {
  const regex = new RegExp('[\\?&]lang=([^&#]*)')
  const results = regex.exec(window.location.search)
  return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '))
}

export class TVChartContainer extends React.PureComponent {
  static defaultProps = {
    symbol: 'BTC/USDT',
    interval: '15',
    containerId: 'tv_chart_container',
    datafeedUrl: process.env.REACT_APP_CHARTS_URI,
    clientId: 'app.bitvana.io',
    userId: 'public_user_id',
    debug: false,
  }

  componentWillUnmount () {
    this._turnOffWidget()
  }

  componentDidMount() {
    this._initialize()
  }

  componentDidUpdate (prevProps) {
    if (prevProps.symbol !== this.props.symbol) {
      this._turnOffWidget()
      this._initialize()
    }
  }

  _initialize () {
    const widgetOptions = {
      symbol: this.props.symbol,
      // BEWARE: no trailing slash is expected in feed URL
      datafeed: new window.Datafeeds.UDFCompatibleDatafeed(this.props.datafeedUrl, 10000),
      locale: getLanguageFromURL() || 'en',
      custom_css_url: '/charting_library/index.css',
      allow_symbol_change: false,
      charts_storage_url: null,
      charts_storage_api_version: null,
      snapshot_url: null,
      favorites: {
        intervals: [],
        chartTypes: [],
      },
      enabled_features: [], // study_templates
      disabled_features: ['use_localstorage_for_settings', 'save_chart_properties_to_local_storage', 'chart_crosshair_menu', 'header_symbol_search', 'left_toolbar', 'header_screenshot', 'header_compare', 'header_undo_redo', 'header_settings'],
      overrides: {
        'mainSeriesProperties.candleStyle.drawBorder': false,
        'mainSeriesProperties.showCountdown': false,
        'paneProperties.background': 'rgb(49, 55, 65)',
        'paneProperties.horzGridProperties.color': 'rgba(255,255,255,.08)',
        'paneProperties.vertGridProperties.color': 'rgba(255,255,255,.08)',
        'scalesProperties.textColor': 'rgba(255,255,255,.8)',
        'symbolWatermarkProperties.transparency': 90,
      },
      library_path: '/charting_library/',
      supports_search: false,
      fullscreen: false,
      autosize: true,
      studies_overrides: {},
      interval: this.props.interval,
      container_id: this.props.containerId,
      client_id: this.props.clientId,
      user_id: this.props.userId,
      debug: this.props.debug,
      // we use the default time frames
      // time_frames: [
      //     { text: "5y", resolution: "W" },
      //     { text: "1y", resolution: "W" },
      //     { text: "3M", resolution: "D" },
      //     { text: "1M", resolution: "D" },
      //     { text: "5d", resolution: "60" },
      //     { text: "1d", resolution: "5" },
      //    { text: "1d", resolution: "5" },
      // ],
      // drawings_access: {},
    }
    const widget = window.tvWidget = new window.TradingView.widget(widgetOptions)
  }

  _turnOffWidget() {
    if(window.tvWidget){
      try {
        window.tvWidget.remove()
      } catch (err) {}
    }
  }

  render () {
    return (
      <div
        id={this.props.containerId}
        className='TVChartContainer'
      />
    )
  }
}
