import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { View } from '../../Components/Html'
import styles from '../Styles/ExchangeStyles'
import Title from './Title'
import I18n from '../../I18n/I18n'
import { TVChartContainer } from './TVChartContainer'

class Chart extends Component {
  render () {
    return (
      <View style={styles.chart.container}>
        <Title text={I18n.t('exchange.chart.title')} />
        <TVChartContainer
          symbol={`${this.props.secondaryCurrency}/${this.props.mainCurrency}`}
        />
      </View>
    )
  }
}


Chart.propTypes = {
  mainCurrency: PropTypes.string,
  secondaryCurrency: PropTypes.string,
}

Chart.defaultProps = {
  mainCurrency: 'USDT',
  secondaryCurrency: 'BTC',
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Chart)
