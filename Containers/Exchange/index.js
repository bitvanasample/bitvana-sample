import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Media from 'react-media'
import Mobile from './Mobile'
import Desktop from './Desktop'
import RoutesProps from '../../GeneralProps/RoutesProps'

const style = { flex: 1, minHeight: '100%' }

class Exchange extends Component {
  shouldComponentUpdate (nextProps) {
    const { secondaryCurrency, mainCurrency } = this.props.match.params
    return (
      secondaryCurrency !== nextProps.match.params.secondaryCurrency
      || mainCurrency !== nextProps.match.params.mainCurrency
    )
  }

  render () {
    return (
      <Media query='(max-width: 599px)'>
        {isSmall => (isSmall
          ? <Mobile
            style={style}
            secondaryCurrency={this.props.match.params.secondaryCurrency}
            mainCurrency={this.props.match.params.mainCurrency}
          />
          : <Desktop
            style={style}
            secondaryCurrency={this.props.match.params.secondaryCurrency}
            mainCurrency={this.props.match.params.mainCurrency}
          />
        )}
      </Media>
    )
  }
}

Exchange.propTypes = {
  ...RoutesProps({
    secondaryCurrency: PropTypes.string.isRequired,
    mainCurrency: PropTypes.string.isRequired,
  }),
}

Exchange.defaultProps = {}

export default Exchange
