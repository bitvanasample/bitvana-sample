import React, { Component } from 'react'
import PropTypes from 'prop-types'
import AppBar from '../../Components/AppBar/Advanced'
import { View, Tab, Tabs } from '../../Components/Html'
import styles from '../Styles/ExchangeStyles'
import OrderBook from './OrderBook'
import OrderForm from './OrderForm'
import History from './History'
import UserInfo from './UserInfo'
import Chart from './Chart'
import OpenOrders from './OpenOrders'
import I18n from '../../I18n/I18n'

const maxHeightTab2 = {
  maxHeight: 380,
  minHeight: 340,
  height: '100%',
}

const maxHistoryHeight = {
  minHeight: 80,
  maxHeight: 150,
  height: '100%',
}

const mainHideScroll = {
  overflow: 'hidden',
}

const leftScreen = {
  flex: 5,
}

const rightScreen = {
  flex: 4,
  paddingBottom: 4,
}


class Mobile extends Component {
  render () {
    const params = {
      secondaryCurrency: this.props.secondaryCurrency,
      mainCurrency: this.props.mainCurrency,
    }
    return (
      <View>
        <AppBar {...params} />
        <Tabs
          style={styles.contentMobile}
          tabs={[
            <Tab
              iconName='star'
              value={0}
            >
              {I18n.t('exchange.trade')}
            </Tab>,
            <Tab
              iconName='signal'
              value={1}
            >
              {I18n.t('exchange.chart.title')}
            </Tab>,
            <Tab
              iconName='list'
              value={2}
            >
              {I18n.t('exchange.myorders')}
            </Tab>,
          ]}
          tabContent={[
            <View style={[styles.contentMobile, mainHideScroll]}>
              <View style={styles.contentMobile}>
                <View style={[styles.contentMobileRow, maxHeightTab2]}>
                  <View style={[styles.contentMobile, leftScreen]}>
                    <UserInfo {...params} />
                    <OrderBook {...params} />
                  </View>
                  <View style={[styles.contentMobile, rightScreen]}>
                    <OrderForm {...params} />
                  </View>
                </View>
                <View style={maxHistoryHeight}>
                  <History {...params} />
                </View>
              </View>
            </View>,
            <View style={styles.contentMobile}>
              <Chart {...params} />
            </View>,
            <View style={styles.contentMobile}>
              <OpenOrders {...params} />
            </View>,
          ]}
          tabContentStyle={styles.mainMobile}
        />
      </View>
    )
  }
}

Mobile.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}

export default Mobile
