import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import OrderBookSpread from './OrderBookSpread'
import { withOrdersBook } from '../../../GraphQl/Orders/OrderBookQuery'
import { withMyOrders } from '../../../GraphQl/Orders/MyOrdersQuery'
import OrderBuy from './OrdersBuy'
import OrderSell from './OrdersSell'
import Indicator from '../../../QuanticComponents/Indicator'

const voidShape = {
  price: 0,
}

class OrderBook extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.loading !== nextProps.loading
    || this.props.dateTimeSell !== nextProps.dateTimeSell
    || this.props.dateTimeBuy !== nextProps.dateTimeBuy
    || this.props.dateTimeOrdersOn !== nextProps.dateTimeOrdersOn
    || this.props.mainCurrency !== nextProps.mainCurrency
    || this.props.secondaryCurrency !== nextProps.secondaryCurrency
  }

  componentDidUpdate (prevProps) {
    const { mainCurrency: c1, secondaryCurrency: c2 } = prevProps
    const { mainCurrency: nextC1, secondaryCurrency: nextC2 } = this.props
    if (
      (c1 !== nextC1 || c2 !== nextC2)
      || (!this.unsubscribe && (nextC1 && nextC2))
    ) {
      if (this.unsubscribe) { this.unsubscribe() }
      this.unsubscribe = this.props.subscriptionToNewOrdersBook()
    }
  }

  componentWillUnmount () {
    if (this.unsubscribe) { this.unsubscribe() }
  }

  render () {
    if (this.props.loading) {
      return <Indicator />
    }
    const {
      ordersSell,
      dateTimeSell,
      dateTimeOrdersOn,
      mainCurrency,
      secondaryCurrency,
      hasOrdersOnByPrice,
      ordersBuy,
      dateTimeBuy,
    } = this.props
    return [
      <OrderSell
        key='ordersSell'
        orders={ordersSell}
        dateTimeOrderBook={dateTimeSell}
        dateTimeOrderOn={dateTimeOrdersOn}
        mainCurrency={mainCurrency}
        secondaryCurrency={secondaryCurrency}
        hasOrdersOnByPrice={hasOrdersOnByPrice}
      />,
      <OrderBookSpread
        key='spread'
        sellOrder={
          ordersSell[ordersSell.length - 1]
          || voidShape
        }
        buyOrder={
          ordersBuy[ordersBuy.length - 1]
          || voidShape
        }
        currency={mainCurrency}
      />,
      <OrderBuy
        key='ordersBuy'
        orders={ordersBuy}
        dateTimeOrderBook={dateTimeBuy}
        dateTimeOrderOn={dateTimeOrdersOn}
        mainCurrency={mainCurrency}
        secondaryCurrency={secondaryCurrency}
        hasOrdersOnByPrice={hasOrdersOnByPrice}
      />,
    ]
  }
}

OrderBook.propTypes = {
  ordersSell: PropTypes.arrayOf(PropTypes.shape({
    weight: PropTypes.number,
    amount: PropTypes.number,
    price: PropTypes.number,
  })).isRequired,
  ordersBuy: PropTypes.arrayOf(PropTypes.shape({
    weight: PropTypes.number,
    amount: PropTypes.number,
    price: PropTypes.number,
  })).isRequired,
  dateTimeSell: PropTypes.string.isRequired,
  dateTimeBuy: PropTypes.string.isRequired,
  dateTimeOrdersOn: PropTypes.string.isRequired,
  subscriptionToNewOrdersBook: PropTypes.func.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  loading: PropTypes.bool,
  hasOrdersOnByPrice: PropTypes.func.isRequired,
}

OrderBook.defaultProps = {
  loading: false,
}

export default compose(
  withOrdersBook,
  withMyOrders,
)(OrderBook)
