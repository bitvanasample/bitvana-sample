import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import { List, AutoSizer } from 'react-virtualized'
import { View } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'
import OrderBookItem from './OrderBookItem'
// import { overscanRowCount } from '../../../Config/AppConfig'

// const listAutosizeStyle = {
//   display: 'flex',
//   flex: '1 1 auto',
// }

class OrderBook extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.dateTimeOrderBook !== nextProps.dateTimeOrderBook
    || this.props.dateTimeOrderOn !== nextProps.dateTimeOrderOn
    || this.props.mainCurrency !== nextProps.mainCurrency
    || this.props.secondaryCurrency !== nextProps.secondaryCurrency
  }

  render () {
    const array = this.props.orders
    const len = array.length
    const offSet = len - 1
    return (
      <View style={[styles.orderBook.scrollBox]}>
        {
          array.map((_, index) => {
            const order = array[offSet - index]
            return (
              <div key={order.price}>
                <OrderBookItem
                  key={order.price}
                  amount={order.amount}
                  price={order.price}
                  number={index}
                  isSell={false}
                  secondaryCurrency={this.props.secondaryCurrency}
                  mainCurrency={this.props.mainCurrency}
                  hasOrdersOnHere={
                    this.props.hasOrdersOnByPrice(order.price)
                  }
                />
              </div>
            )
          })
        }
      </View>
    )
    // return (
    //   <View style={styles.orderBook.scrollBox}>
    //     <div style={listAutosizeStyle}>
    //       <AutoSizer>
    //         {({ width, height }) => (
    //           <List
    //             overscanRowCount={overscanRowCount}
    //             width={width}
    //             height={height}
    //             rowCount={len}
    //             rowHeight={16}
    //             rowWidth={width}
    //             scrollToIndex={0}
    //             rowRenderer={({
    //               index, style,
    //             }) => {
    //               const order = array[offSet - index]
    //               return (
    //                 <div key={order.price} style={style}>
    //                   <OrderBookItem
    //                     key={order.price}
    //                     amount={order.amount}
    //                     price={order.price}
    //                     number={index}
    //                     isSell={false}
    //                     secondaryCurrency={this.props.secondaryCurrency}
    //                     mainCurrency={this.props.mainCurrency}
    //                     hasOrdersOnHere={
    //                       this.props.hasOrdersOnByPrice(order.price)
    //                     }
    //                   />
    //                 </div>
    //               )
    //             }}
    //           />
    //         )}
    //       </AutoSizer>
    //     </div>
    //   </View>
    // )
  }
}

OrderBook.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  orders: PropTypes.arrayOf(PropTypes.shape({
    weight: PropTypes.number,
    amount: PropTypes.number,
    price: PropTypes.number,
  })),
  dateTimeOrderBook: PropTypes.string.isRequired,
  dateTimeOrderOn: PropTypes.string.isRequired,
  hasOrdersOnByPrice: PropTypes.func.isRequired,
}

OrderBook.defaultProps = {
  orders: [],
}

export default OrderBook
