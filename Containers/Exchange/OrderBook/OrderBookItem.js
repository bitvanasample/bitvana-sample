import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Big from '../../../Lib/Big'
import { View } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'
import OrderWeight from './OrderWeight'
import Colors from '../../../Themes/Colors'
import AmountCurrency from '../../../Components/CryptoForm/AmountCurrency'
import Point from './Point'

const colorStyleSell = {
  true: {
    ...styles.text,
    color: Colors.exchangeSell,
  },
  false: {
    ...styles.text,
    color: Colors.exchangeBuy,
  },
}

class OrderBookItem extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.amount !== nextProps.amount
      || this.props.hasOrdersOnHere !== nextProps.hasOrdersOnHere
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
  }

  render () {
    const {
      amount,
      price,
      isSell,
      hasOrdersOnHere,
      secondaryCurrency,
      mainCurrency,
    } = this.props
    const total = Big(amount).mul(price)
    return (
      /* <HighlightOnUpdate
        keyForUpdate={this.props.amount}
      > */
      <View style={styles.orderBook.item.container}>
        <OrderWeight
          isSell={isSell}
          amount={amount}
          price={price}
        />
        <Point
          hasOrdersOnHere={hasOrdersOnHere}
          isSell={isSell}
        />
        <View style={[
          styles.orderBook.column,
          styles.orderBook.amount,
        ]}
        >
          <AmountCurrency
            fixed
            amount={amount}
            currency={secondaryCurrency}
            style={styles.text}
          />
        </View>
        <View style={[
          styles.orderBook.column,
          styles.orderBook.price,
        ]}
        >
          <AmountCurrency
            fixed
            amount={price}
            currency={mainCurrency}
            style={colorStyleSell[isSell]}
          />
        </View>
        <View style={[
          styles.orderBook.column,
          styles.orderBook.total,
        ]}
        >
          <AmountCurrency
            fixed
            amount={total}
            currency={mainCurrency}
            currency2={secondaryCurrency}
            style={styles.normalTextNote}
          />
        </View>
      </View>
      // </HighlightOnUpdate>
    )
  }
}

OrderBookItem.propTypes = {
  mainCurrency: PropTypes.string.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  amount: PropTypes.number,
  price: PropTypes.number,
  isSell: PropTypes.bool,
  hasOrdersOnHere: PropTypes.bool,
}

OrderBookItem.defaultProps = {
  amount: 0,
  price: 0,
  isSell: true,
  hasOrdersOnHere: false,
}

export default OrderBookItem
