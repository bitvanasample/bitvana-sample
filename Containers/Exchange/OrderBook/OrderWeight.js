import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'
import Colors from '../../../Themes/Colors'
import { cacheStyleGenerator } from '../../../Lib/Perform'

const defaultStyleSell = {
  ...styles.orderBook.item.weight,
  backgroundColor: Colors.exchangeSell,
}
const defaultStyleBuy = {
  ...styles.orderBook.item.weight,
  backgroundColor: Colors.exchangeBuy,
}
const cacheStyleByIsSell = {
  true: cacheStyleGenerator(defaultStyleSell),
  false: cacheStyleGenerator(defaultStyleBuy),
}

const styleForWeight = {}

class OrderWeight extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.amount !== nextProps.amount
      || this.props.price !== nextProps.price
  }

  render () {
    // TODO: this component need to do a query for the magicNumber
    const magicNumber = 2281649512
    const weight = Math.round((this.props.amount / magicNumber) * 100)
    const cacheStyle = cacheStyleByIsSell[this.props.isSell]
    if (!styleForWeight[weight]) {
      styleForWeight[weight] = {
        width: `${weight}%`,
      }
    }
    return (
      <View style={cacheStyle(styleForWeight[weight])} />
    )
  }
}

OrderWeight.propTypes = {
  isSell: PropTypes.bool,
  amount: PropTypes.string,
  price: PropTypes.string,
}

OrderWeight.defaultProps = {
  amount: 0,
  price: 0,
  isSell: true,
}

export default OrderWeight
