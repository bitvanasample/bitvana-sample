import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'

const styleForSellByHasOrdersOnHere = {
  true: styles.orderBook.pointSellActivated,
  false: styles.orderBook.pointDeactivated,
}

const styleForBuyByHasOrdersOnHere = {
  true: styles.orderBook.pointBuyActivated,
  false: styles.orderBook.pointDeactivated,
}

const styleByIsSell = {
  true: styleForSellByHasOrdersOnHere,
  false: styleForBuyByHasOrdersOnHere,
}

const classNameTootletipWrap = {
  true: 'tooltip-wrap',
  false: '',
}

// const classNameTootletipContent = {
//   true: 'tooltip-content',
//   false: '',
// }

// const textToolTip = {
//   true: 'WIP',
//   false: '',
// }

class Title extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.isSell !== nextProps.isSell
      || this.props.hasOrdersOnHere !== nextProps.hasOrdersOnHere
  }

  render () {
    const { hasOrdersOnHere, isSell } = this.props
    return (
      <div
        className={classNameTootletipWrap[hasOrdersOnHere]}
      >
        <View
          style={
            styleByIsSell[isSell][hasOrdersOnHere]
          }
        />
        {/*
          <div
            className={classNameTootletipContent[hasOrdersOnHere]}
          >
            { textToolTip[hasOrdersOnHere] }
          </div>
        */}
      </div>
    )
  }
}

Title.propTypes = {
  isSell: PropTypes.bool.isRequired,
  hasOrdersOnHere: PropTypes.bool.isRequired,
}

Title.defaultProps = {}

export default Title
