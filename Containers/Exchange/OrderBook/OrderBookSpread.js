import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Big from '../../../Lib/Big'
import { View } from '../../../Components/Html'
import Text from '../Text'
import styles from '../../Styles/ExchangeStyles'
import AmountCurrency from '../../../Components/CryptoForm/AmountCurrency'

class OpenBook extends Component {
  shouldComponentUpdate (nextProps) {
    if (this.props.currency !== nextProps.currency) {
      return true
    }
    const sellPrice = this.props.sellOrder.price
    const buyPrice = this.props.buyOrder.price
    const nextSellPrice = nextProps.sellOrder.price
    const nextBuyPrice = nextProps.buyOrder.price
    const thisDelta = Big(sellPrice).minus(buyPrice).abs()
    const nextDelta = Big(nextSellPrice).minus(nextBuyPrice).abs()
    return thisDelta !== nextDelta
  }

  render () {
    const {
      currency,
      sellOrder: { price: sellPrice } = { price: 0 },
      buyOrder: { price: buyPrice } = { price: 0 },
    } = this.props
    const delta = Big(sellPrice).minus(buyPrice).abs()
    return (
      <View style={styles.orderBook.spread} >
        <View style={styles.orderBook.column1}>
          <AmountCurrency
            amount={delta}
            currency={currency}
            style={styles.text}
          />
          <Text style={styles.normalTextNote}>
            {`${currency} of spread`}
          </Text>
        </View>
      </View>
    )
  }
}

OpenBook.propTypes = {
  buyOrder: PropTypes.shape({
    price: PropTypes.number,
  }),
  sellOrder: PropTypes.shape({
    price: PropTypes.number,
  }),
  currency: PropTypes.string,
}

OpenBook.defaultProps = {
  sellOrder: {
    price: 0,
  },
  buyOrder: {
    price: 0,
  },
  currency: '',
}

export default OpenBook
