import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from '../../../Components/Html'
import JustOneRender from '../../../Components/JustOneRender'
import Text from '.././Text'
import styles from '../../Styles/ExchangeStyles'
import Title from '.././Title'
import I18n from '../../../I18n/I18n'
import Orders from './Orders'
import Point from './Point'

class OrderBook extends Component {
  shouldComponentUpdate (nextProps) {
    return (
      this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
    )
  }

  render () {
    return (
      <View style={styles.orderBook.container}>
        <JustOneRender key='title'>
          <Title text={I18n.t('exchange.orderBook.title')} />
          <View style={styles.tableBorder} >
            <Point hasOrdersOnHere={false} isSell={false} />
            <View style={styles.orderBook.column1}>
              <Text>Amount</Text>
              <Text note>{` ${this.props.secondaryCurrency}`}</Text>
            </View>
            <View style={styles.orderBook.column2}>
              <Text>Price</Text>
              <Text note>{` ${this.props.mainCurrency}`}</Text>
            </View>
            <View style={styles.orderBook.column3}>
              <Text>Total</Text>
              <Text note>{` ${this.props.mainCurrency}`}</Text>
            </View>
          </View>
        </JustOneRender>
        <Orders
          mainCurrency={this.props.mainCurrency}
          secondaryCurrency={this.props.secondaryCurrency}
        />
      </View>
    )
  }
}

OrderBook.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}

OrderBook.defaultProps = {}

export default OrderBook
