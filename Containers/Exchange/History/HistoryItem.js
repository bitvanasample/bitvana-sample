import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import Icon from 'react-fontawesome'
import 'moment/locale/es'
import { View } from '../../../Components/Html'
import Text from '../Text'
import styles from '../../Styles/ExchangeStyles'
import Colors from '../../../Themes/Colors'
import AmountCurrency from '../../../Components/CryptoForm/AmountCurrency'

const styleSellText = {
  true: {
    ...styles.text,
    color: Colors.exchangeSell,
    textAlign: 'right',
  },
  false: {
    ...styles.text,
    color: Colors.exchangeBuy,
    textAlign: 'right',
  },
}

const styleSellIcon = {
  true: {
    ...styleSellText.true,
    fontSize: 10,
    marginBottom: 1,
    color: Colors.exchangeSell,
  },
  false: {
    ...styleSellText.false,
    fontSize: 10,
    marginBottom: 1,
    color: Colors.exchangeBuy,
  },
}


let locale = localStorage.getItem('i18nextLng') || 'en'
locale = locale.indexOf('es') !== -1 ? 'es' : 'en'
moment.locale(locale)
class HistoryItem extends Component {
  shouldComponentUpdate (nextProps) {
    return this.props.amount !== nextProps.amount
      || this.props.price !== nextProps.price
      || this.props.createdAt !== nextProps.createdAt
  }

  render () {
    const createdAt = moment(this.props.createdAt * 1000).format('HH:mm:ss')
    return (
      // <HighlightOnCreate
      //  turnOn={this.props.turnOnHighlight}
      //  style={styles.history.item.container}
      // >
      <View
        style={styles.history.item.container}
      >
        <View style={styles.history.row1}>
          <AmountCurrency
            fixed
            style={styles.text}
            amount={this.props.amount}
            currency={this.props.secondaryCurrency}
          />
        </View>
        <View style={styles.history.row2}>
          <Icon
            name={this.props.sell ? 'arrow-down' : 'arrow-up'}
            style={styleSellIcon[this.props.sell]}
          />
          <AmountCurrency
            fixed
            style={styleSellText[this.props.sell]}
            amount={this.props.price}
            currency={this.props.mainCurrency}
          />
        </View>
        <View style={styles.history.row3}>
          <Text style={styles.normalTextNote}>{createdAt}</Text>
        </View>
      </View>
      // </HighlightOnCreate>
    )
  }
}


HistoryItem.propTypes = {
  mainCurrency: PropTypes.string.isRequired,
  secondaryCurrency: PropTypes.string.isRequired,
  createdAt: PropTypes.number,
  amount: PropTypes.number,
  price: PropTypes.number,
  sell: PropTypes.bool,
  // turnOnHighlight: PropTypes.bool.isRequired,
}

HistoryItem.defaultProps = {
  createdAt: 0,
  amount: 0,
  price: 0,
  sell: true,
}

export default HistoryItem
