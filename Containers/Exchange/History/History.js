import React, { Component } from 'react'
import PropTypes from 'prop-types'
// import debounceRender from 'react-debounce-render'
// import { List, AutoSizer } from 'react-virtualized'
import { View } from '../../../Components/Html'
import styles from '../../Styles/ExchangeStyles'
import HistoryItem from './HistoryItem'
import { withMarkeTransactions } from
  '../../../GraphQl/Markets/MarketTransactionsQuery'
import Indicator from '../../../QuanticComponents/Indicator'
// import { overscanRowCount } from '../../../Config/AppConfig'

// const listAutosizeStyle = {
//   display: 'flex',
//   flex: '1 1 auto',
// }

class History extends Component {
  shouldComponentUpdate (nextProps) {
    return (
      this.props.loading !== nextProps.loading
      || this.props.secondaryCurrency !== nextProps.secondaryCurrency
      || this.props.mainCurrency !== nextProps.mainCurrency
      || (
        (this.props.transactions[0] || {}).id
        !== (nextProps.transactions[0] || {}).id
      )
    )
  }

  componentDidUpdate (prevProps) {
    const { mainCurrency: c1, secondaryCurrency: c2 } = prevProps
    const { mainCurrency: nextC1, secondaryCurrency: nextC2 } = this.props
    if (
      (c1 !== nextC1 || c2 !== nextC2)
      || (!this.unsubscribe && (nextC1 && nextC2))
    ) {
      if (this.unsubscribe) { this.unsubscribe() }
      this.unsubscribe = this.props.subscriptionToNewOrders()
    }
  }

  componentWillUnmount () {
    if (this.unsubscribe) { this.unsubscribe() }
  }

  render () {
    if (this.props.loading) {
      return <Indicator />
    }
    const array = this.props.transactions
    // return (
    //   <View style={styles.history.scrollBox}>
    //     <div style={listAutosizeStyle}>
    //       <AutoSizer>
    //         {({ width, height }) => (
    //           <List
    //             overscanRowCount={overscanRowCount}
    //             width={width}
    //             height={height}
    //             rowCount={array.length}
    //             rowHeight={18}
    //             rowWidth={width}
    //             rowRenderer={({ index, style }) => {
    //               const hist = array[index]
    //               return (
    //                 <div key={hist.id} style={style}>
    //                   <HistoryItem
    //                     key={hist.id}
    //                     createdAt={hist.createdAt}
    //                     amount={hist.amount}
    //                     price={hist.price}
    //                     sell={hist.sell}
    //                     secondaryCurrency={this.props.secondaryCurrency}
    //                     mainCurrency={this.props.mainCurrency}
    //                   />
    //                 </div>
    //               )
    //             }}
    //           />
    //         )}
    //       </AutoSizer>
    //     </div>
    //   </View>
    // )
    return (
      <View style={styles.history.scrollBox}>
        {
          array.map(hist => (
            <div key={hist.id}>
              <HistoryItem
                key={hist.id}
                createdAt={hist.createdAt}
                amount={hist.amount}
                price={hist.price}
                sell={hist.sell}
                secondaryCurrency={this.props.secondaryCurrency}
                mainCurrency={this.props.mainCurrency}
              />
            </div>
          ))
        }
      </View>
    )
  }
}


History.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
  transactions: PropTypes.arrayOf(PropTypes.shape({
    time: PropTypes.number,
    amount: PropTypes.number,
    price: PropTypes.number,
  })),
  loading: PropTypes.bool,
  subscriptionToNewOrders: PropTypes.func.isRequired,
}

History.defaultProps = {
  transactions: [],
  loading: false,
}

export default withMarkeTransactions(History)
