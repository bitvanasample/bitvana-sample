import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from '../../../Components/Html'
import JustOneRender from '../../../Components/JustOneRender'
import Text from '../Text'
import styles from '../../Styles/ExchangeStyles'
import Title from '../Title'
import I18n from '../../../I18n/I18n'
import History from './History'

class Index extends Component {
  render () {
    return (
      <View style={styles.history.container}>
        <JustOneRender>
          <Title text={I18n.t('exchange.history.title')} />
          {/* Order book sections */}
          <View style={styles.flexColumn} >
            {/* Order book header */}
            <View style={[styles.flexRow, styles.tableBorder]} >
              <View style={styles.history.header1}>
                <Text>Trade Size</Text>
                <Text note>{` ${this.props.secondaryCurrency}`}</Text>
              </View>
              <View style={styles.history.header2}>
                <Text>Price</Text>
                <Text note>{` ${this.props.mainCurrency}`}</Text>
              </View>
              <View style={styles.history.header3}>
                <Text>Time</Text>
              </View>
            </View>
          </View>
        </JustOneRender>
        <History
          mainCurrency={this.props.mainCurrency}
          secondaryCurrency={this.props.secondaryCurrency}
        />
      </View>
    )
  }
}


Index.propTypes = {
  secondaryCurrency: PropTypes.string.isRequired,
  mainCurrency: PropTypes.string.isRequired,
}

Index.defaultProps = {}

export default Index
