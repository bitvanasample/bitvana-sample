import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import AppBarExchange from '../../Components/AppBar/Exchange'
import AppMenu from '../../Components/AppMenu/Landing'
import { View, H4, Tab, Tabs } from '../../Components/Html'
import ExchangeMain from '../ExchangeMain'
import I18n from '../../I18n/I18n'
import { redirectTo, currentPath as cp } from '../../Routes/Helpers'
import { isMobile } from '../../Config/AppConfig'

class Settings extends Component {
  render () {
    return (
      <View>
        <AppBarExchange />
        <AppMenu />
        <ExchangeMain>
          {
            !isMobile
            && (
              <H4 style={{ padding: 20, margin: 0 }}>
                {I18n.t('settings.title')}
              </H4>
            )
          }
          <Tabs
            onChange={url => redirectTo(`/settings/${url}`)}
            value={cp(this.props, 2)}
            tabs={[
              <Tab iconName='user' value='profile'>
                {I18n.t('settings.profile.tabName')}
              </Tab>,
              <Tab iconName='university' value='bank-accounts'>
                {I18n.t('settings.bankAccounts.tabName')}
              </Tab>,
              <Tab iconName='lock' value='security'>
                {I18n.t('settings.security.tabName')}
              </Tab>,
            ]}
            tabContent={this.props.children}
          />
        </ExchangeMain>
      </View>
    )
  }
}

Settings.propTypes = {
  children: PropTypes.arrayOf(PropTypes.shape({})),
}

Settings.defaultProps = {
  children: [],
}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Settings)
