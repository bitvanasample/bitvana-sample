import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import { Dialog, B, H5, H4, View } from '../../../Components/Html'
import I18n from '../../../I18n/I18n'
import styles from '../../../Containers/Styles/DialogStyles'
import { withBankAccounts }
  from '../../../GraphQl/BankAccounts/BankAccountsQuery'
import { toCurrency } from '../../../Lib/Text'


class Verify extends Component {
  _getAccountRow (key) {
    const { currency } = this.props
      .findBankAccount(this.props.id) || {}

    return `${
      I18n.t(`settings.bankAccounts.verifyForm.bankInfo.${key}`)}
              : 
              ${I18n.t(`settings.bankAccounts.verifyForm.bankInfo.${currency
    .code}.${key}`)}`
  }

  render () {
    const {
      name, currency, verifyAmount,
    } = this.props
      .findBankAccount(this.props.id) || {}
    if (this.props.id) {
      return (
        <div>
          <Dialog
            style={styles.verifyDialogContainer}
            open={this.props.open}
            title={I18n.t('settings.bankAccounts.verifyForm.title', { name })}
            onClose={() => this.props.handleClose()}
            onSubmit={() => this.props.handleClose()}
          >
            <View style={[styles.flexColumn, styles.flex]}>
              <H5 style={[styles.noMargins, styles.flexCenter]}>
                {I18n.t('settings.bankAccounts.verifyForm.amountToDeposit')}
              </H5>
              <H4 style={[styles.verifyAmount, styles.flexCenter]}>
                <B>{` $${toCurrency(verifyAmount)} 
              ${currency.code}`}
                </B>
              </H4>
            </View>
            <View style={styles.verifyInfo}>
              <H5 style={styles.noMargins}>
                {this._getAccountRow('accountName')}
              </H5>
              <H5 style={styles.noMargins}>
                {this._getAccountRow('dni')}
              </H5>
              <H5 style={styles.noMargins}>
                {this._getAccountRow('bankName')}
              </H5>
              <H5 style={styles.noMargins}>
                {this._getAccountRow('accountType')}
              </H5>
              <H5 style={styles.noMargins}>
                {this._getAccountRow('accountNumber')}
              </H5>
            </View>

          </Dialog>
        </div>
      )
    }
    return (<Dialog />)
  }
}

Verify.propTypes = {
  id: PropTypes.number,
  findBankAccount: PropTypes.func,
  open: PropTypes.bool,
  handleClose: PropTypes.func,
}

Verify.defaultProps = {
  id: null,
  findBankAccount: () => {},
  open: false,
  handleClose: () => {},
}


export default compose(withBankAccounts)(Verify)
