import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { RowDefinition, ColumnDefinition } from 'griddle-react'
import { View, Button, H5 } from '../../../Components/Html'
import I18n from '../../../I18n/I18n'
import ExchangeMain from '../../../Containers/ExchangeMain'
import { I18nCell, QGriddle, enhancedWithData }
  from '../../../Components/Tables'
import ActionsCell from './ActionsCell'
import styles from '../../../Containers/Styles/SettingsStyles'
import { withBankAccounts }
  from '../../../GraphQl/BankAccounts/BankAccountsQuery'
import Form from './Form'
import Verify from './Verify'
import { isMobile } from '../../../Config/AppConfig'


class BankAccounts extends Component {
  constructor (props) {
    super(props)
    this.state = {
      open: false,
      verifyId: null,
      verifyOpen: false,
    }
  }

  shouldComponentUpdate () {
    return true// nextProps.bankAccounts !== this.props.bankAccounts
  }


  _refetchList () {
    this.props.refetch()
  }

  _verifyAccount (verifyId) {
    this.setState({ verifyId, verifyOpen: true })
  }

  render () {
    const { bankAccounts } = this.props
    return (
      <View>
        <View style={[styles.flexColumn, { width: '100%' }]}>
          <H5>{I18n.t('settings.bankAccounts.title')}</H5>
          <View style={styles.bankAccounts.addBankAccount}>
            <Button
              onPress={() => this.setState({ open: true })}
            >
              {I18n.t('settings.bankAccounts.addAccount')}
            </Button>
          </View>
        </View>
        <ExchangeMain style={{ paddingTop: 60 }}>
          <QGriddle
            data={bankAccounts}
          >
            <RowDefinition>
              <ColumnDefinition
                id='name'
                title={I18n.t('settings.bankAccounts.name')}
              />
              <ColumnDefinition
                id='accountNumber'
                visible={!isMobile}
                title={I18n.t('settings.bankAccounts.accountNumber')}
              />
              <ColumnDefinition
                id='bank.name'
                visible={!isMobile}
                title={I18n.t('settings.bankAccounts.bankName')}
              />
              <ColumnDefinition
                visible={!isMobile}
                customComponent={I18nCell}
                id='accountType'
                i18Path='settings.bankAccounts.typeCodes'
                title={I18n.t('settings.bankAccounts.accountType')}
              />
              <ColumnDefinition
                customComponent={I18nCell}
                id='status'
                i18Path='settings.bankAccounts.statusCodes'
                title={I18n.t('settings.bankAccounts.status')}
              />
              <ColumnDefinition
                customComponent={enhancedWithData(ActionsCell)}
                onVerifyClick={({ id }) => this._verifyAccount(id)}
                id='id'
                label='Eliminar'
                title={I18n.t('settings.bankAccounts.actions')}
              />
            </RowDefinition>
          </QGriddle>
        </ExchangeMain>
        <Form
          refetchList={() => this._refetchList()}
          open={this.state.open}
          handleClose={() => this.setState({ open: false })}
        />
        <Verify
          open={this.state.verifyOpen}
          id={this.state.verifyId}
          handleClose={() => this.setState({ verifyOpen: false }, () => {})}
        />
      </View>
    )
  }
}

BankAccounts.propTypes = {
  bankAccounts: PropTypes.arrayOf(PropTypes.shape({})),
  refetch: PropTypes.func,
}

BankAccounts.defaultProps = {
  bankAccounts: [],
  refetch: () => {},
}

export default withBankAccounts(BankAccounts)
