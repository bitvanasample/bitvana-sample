import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import { Dialog, InputWithLabel, View } from '../../../Components/Html'
import { SelectBank, SelectCountry, SelectBankAccountType }
  from '../../../Components/CryptoForm'
import I18n from '../../../I18n/I18n'
import styles from '../../../Containers/Styles/DialogStyles'
import { withUserInfo } from '../../../GraphQl/Users/UserInfoQuery'
import { withBankAccounts }
  from '../../../GraphQl/BankAccounts/BankAccountsQuery'
import { withUpdateBankAccount2 }
  from '../../../GraphQl/BankAccounts/BankAccountsMutation'
// import { redirectTo } from '../../../Routes/Helpers'


class Form extends Component {
  constructor (props) {
    super(props)
    const countryId = this.props.country ? this.props.country.id : ''
    this.state = {
      name: '',
      accountType: null,
      accountNumber: '',
      countryId,
      bankId: '',
    }
  }

  render () {
    return (
      <div>
        <Dialog
          open={this.props.open}
          title='Nueva cuenta bancaria'
          onClose={() => this.props.handleClose()}
          onSubmit={() => this.props.updateBankAccount({
            ...this.state,
          }).then(() => {
            this.props.handleClose()
            // redirectTo('/settings/bank-accounts')
            this.props.refetchList()
          })}
        >
          <View style={styles.bankSection}>
            <InputWithLabel
              label={I18n.t('settings.bankAccounts.name')}
              style={{ flex: 1 }}
              onChangeText={name =>
                this.setState({ name })
              }
              value={this.state.name}
              placeholder={I18n.t('settings.bankAccounts.name')}
            />
          </View>
          <View style={[styles.bankSection, { zIndex: 3 }]} >
            <View style={[styles.bankSection, { zIndex: 4 }]} >
              <InputWithLabel
                label={I18n.t('select.selectCountry')}
                style={{ flex: 1, zIndex: 4 }}
                customInput={
                  <SelectCountry
                    style={{ flex: 1, zIndex: 4 }}
                    value={this.state.countryId}
                    onChange={
                      countryId => this.setState({ countryId })
                    }
                  />
                }
              />
            </View>
            <View style={[styles.bankSection, { zIndex: 3 }]} >
              <InputWithLabel
                label={I18n.t('select.selectBank')}
                style={{ flex: 1, zIndex: 3 }}
                customInput={
                  <SelectBank
                    style={{ flex: 1, zIndex: 3 }}
                    value={this.state.bankId}
                    countryId={this.state.countryId}
                    onChange={
                      bankId => this.setState({ bankId })
                    }
                  />
                }
              />
            </View>
          </View>
          <View style={[styles.bankSection, { zIndex: 2 }]} >
            <InputWithLabel
              label={I18n.t('settings.bankAccounts.accountType')}
              style={{ flex: 1 }}
              customInput={
                <SelectBankAccountType
                  style={{ flex: 1 }}
                  value={this.state.accountType}
                  onChange={
                    accountType => this.setState({ accountType })
                  }
                />
              }
            />
            <InputWithLabel
              label={I18n.t('settings.bankAccounts.accountNumber')}
              style={{ flex: 1 }}
              onChangeText={accountNumber =>
                this.setState({ accountNumber })
              }
              value={this.state.accountNumber}
              placeholder={I18n.t('settings.bankAccounts.accountNumber')}
            />
          </View>
        </Dialog>
      </div>
    )
  }
}

Form.propTypes = {
  open: PropTypes.bool,
  handleClose: PropTypes.func,
  country: PropTypes.shape({
    id: PropTypes.number,
  }),
  updateBankAccount: PropTypes.func,
  refetchList: PropTypes.func,
}

Form.defaultProps = {
  open: false,
  handleClose: () => {},
  country: {},
  updateBankAccount: () => {},
  refetchList: () => {},
}


export default compose(
  withUserInfo, withUpdateBankAccount2,
  withBankAccounts,
)(Form)
