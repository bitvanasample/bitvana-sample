import React, { Component } from 'react'
import PropTypes from 'prop-types'
import I18n from '../../../I18n/I18n'
import Toast from '../../../Lib/Toast'
import { View, A, Confirm } from '../../../Components/Html'
import styles from '../../../Components/Styles/DefaultStyles'
import { withDeleteBankAccount }
  from '../../../GraphQl/BankAccounts/DeleteBankAccountsMutation'
import { serverError } from '../../../Lib/ServerErrors'

class ActionsCell extends Component {
  _deleteBankAccount (id) {
    this.props.deleteBankAccount(id)
      .then(() => Toast
        .show({ text: I18n.t('settings.bankAccounts.deleteSuccess') }))
      .catch(err => Toast.show({
        text: I18n.t(serverError(err.message)),
        type: 'error',
      }))
  }

  render () {
    const { id, status } = this.props.customProps
    const { onVerifyClick } = this.props.cellProperties
    return (
      <View style={[styles.flexRow, { justifyContent: 'space-between' }]}>
        {status === 0 ? (
          <A
            style={styles.link}
            onPress={() => onVerifyClick({ id })}
          >
            {I18n.t('settings.bankAccounts.verify')}
          </A>
        ) : null}
        <A
          style={[styles.link, styles.marginLeft]}
          onPress={() => Confirm().then(() => {
            this._deleteBankAccount({ bankAccountId: id })
          }, () => {})}
        >
          {I18n.t('settings.bankAccounts.delete')}
        </A>
      </View>
    )
  }
}

ActionsCell.propTypes = {
  deleteBankAccount: PropTypes.func.isRequired,
  customProps: PropTypes.shape({
    id: PropTypes.number,
    status: PropTypes.number,
  }),
  cellProperties: PropTypes.shape({
    onVerifyClick: PropTypes.func,
  }),
}

ActionsCell.defaultProps = {
  customProps: {
    id: undefined,
    status: 0,
  },
  cellProperties: {
    onVerifyClick: () => {},
  },
}

export default withDeleteBankAccount(ActionsCell)
