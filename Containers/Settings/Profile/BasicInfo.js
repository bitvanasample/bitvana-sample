import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Text, A, Checkbox, View, H6, InputWithLabel, DatePicker, H5 } from
  '../../../Components/Html'
import { SelectCountry } from '../../../Components/CryptoForm'
import I18n from '../../../I18n/I18n'
import styles from '../../../Containers/Styles/SettingsStyles'

class BasicInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      firstName: null,
      birthdate: null,
      lastName: null,
      address: null,
      city: null,
      countryId: null,
      terms: null,
    }
  }

  _getInfo (param) {
    if (this.state[param] !== null) {
      return this.state[param]
    } else if (this.props.user) {
      return this.props.user[param]
    }
    return null
  }


  _setState (newState) {
    this.setState(newState)
    this.props.changeParams(newState)
  }

  _renderTerms () {
    if (this.props.user && this.props.user.terms) {
      return <View />
    }
    return (
      <View style={[styles.flexRow, styles.flexCenter]}>
        <Checkbox
          label={
            <View style={{ display: 'block' }}>
              <Text style={styles.profile.termsPre}>
                {I18n.t('settings.profile.termsPre')}
              </Text>
              <A
                href='https://www.antennahouse.com/XSLsample/pdf/sample-link_1.pdf'
                target='_blank'
                style={styles.profile.termsPost}
              >
                {I18n.t('settings.profile.termsPost')}
              </A>
            </View>}
          checked={this._getInfo('terms')}
          onChange={() =>
            this._setState({
              terms: !this.state.terms,
            })
          }
        />
      </View>
    )
  }

  _validationMessage () {
    if (this.props.user.validationStatus === 1) {
      return (
        <View>
          <H6 style={styles.profile.pendingMessage}>
            {I18n.t('settings.validationMessage.basicPending')}
          </H6>
        </View>
      )
    } else if (this.props.user.validationStatus === 2) {
      return (
        <View>
          <H6 style={styles.profile.approvedMessage}>
            {I18n.t('settings.validationMessage.basicApproved')}
          </H6>
        </View>
      )
    } else if (this.props.user.validationStatus === 3) {
      return (
        <View>
          <H6 style={styles.profile.rejectedMessage}>
            {I18n.t('settings.validationMessage.basicRejected')}
          </H6>
        </View>
      )
    }
    return null
  }


  render () {
    // console.log(`birthdate: ${this._getInfo('birthdate')}`)
    return (
      <View style={styles.profile.infoContainer}>
        <H5 style={styles.sectionHeading}>
          Personal information
          <hr style={{ opacity: 0.5 }} />
          {this._validationMessage()}
        </H5>
        <View style={styles.flexRow} >
          <InputWithLabel
            required
            label={I18n.t('settings.profile.firstName')}
            style={{ flex: 1 }}
            onChangeText={firstName =>
              this._setState({ firstName })
            }
            value={this._getInfo('firstName')}
            placeholder={I18n.t('settings.profile.firstName')}
          />
          <InputWithLabel
            label={I18n.t('settings.profile.lastName')}
            style={{ flex: 1 }}
            onChangeText={lastName =>
              this._setState({ lastName })
            }
            value={this._getInfo('lastName')}
            placeholder={I18n.t('settings.profile.lastName')}
          />
        </View>
        <View style={styles.flexRow} >
          <InputWithLabel
            label={I18n.t('settings.profile.country')}
            style={{ flex: 1, zIndex: 2 }}
            customInput={
              <SelectCountry
                style={{ flex: 1 }}
                onChange={countryId => this._setState({ countryId })}
                value={this._getInfo('countryId')}
              />
            }
          />
          <InputWithLabel
            label={I18n.t('settings.profile.city')}
            style={{ flex: 1 }}
            value={this._getInfo('city')}
            onChangeText={city =>
              this._setState({ city })
            }
            placeholder={I18n.t('settings.profile.city')}
          />
        </View>
        <View style={styles.flexRow} >
          <InputWithLabel
            label={I18n.t('settings.profile.address')}
            style={{ flex: 1 }}
            value={this._getInfo('address')}
            onChangeText={address =>
              this._setState({ address })
            }
            placeholder={I18n.t('settings.profile.address')}
          />
          <InputWithLabel
            label={I18n.t('settings.profile.birthdate')}
            style={{ flex: 1 }}
            customInput={
              <DatePicker
                style={{ flex: 1 }}
                onChange={birthdate => this._setState({ birthdate })}
                value={this._getInfo('birthdate')}
              />
            }
          />
        </View>
        {this._renderTerms()}
      </View>
    )
  }
}

BasicInfo.propTypes = {
  changeParams: PropTypes.func,
  user: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    address: PropTypes.string,
    city: PropTypes.string,
    countryId: PropTypes.string,
    birthdate: PropTypes.number,
    validationStatus: PropTypes.number,
    terms: PropTypes.bool,
  }),
}

BasicInfo.defaultProps = {
  changeParams: () => {},
  user: {},
}

export default BasicInfo
