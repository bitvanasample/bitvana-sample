import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Image } from 'react-native-web'
import Icon from 'react-fontawesome'
import { View, H6, P, UploadButton, S3InputFile } from
  '../../../Components/Html'
import I18n from '../../../I18n/I18n'
import styles from '../../../Containers/Styles/SettingsStyles'
import { Images } from '../../../Themes'


class PassportBack extends Component {
  constructor (props) {
    super(props)
    this.state = {
      imageProcess: 0,
    }
  }

  _imageUploadedRender () {
    return (
      <View style={styles.flexColumn} >
        <H6 style={styles.sectionHeading}>
          {I18n.t('settings.id_info.front_side')}
        </H6>
        <P style={styles.flex}>
          {I18n.t('settings.id_info.front_side_detail')}
        </P>
        <P style={styles.flex}>
          {I18n.t(`settings.validationMessage.${this.props.validation}`)}
        </P>
        <View style={[styles.flexColumn, styles.idCardSection]}>
          <Image
            style={{
              height: 202,
              width: 316,
            }}
            source={`${this.props.imageUrl
               || Images.photoPlaceholder}?time=${this.props.date}`}
          />
        </View>
      </View>
    )
  }


  render () {
    if (this.props.imageUrl && this.props.validation !== 0) {
      return this._imageUploadedRender()
    }
    return (
      <View style={styles.flexColumn} >
        <H6 style={styles.sectionHeading}>
          {I18n.t('settings.id_info.back_side')}
        </H6>
        <P style={styles.flex}>
          {I18n.t('settings.id_info.back_side_detail')}
        </P>
        <UploadButton
          onPress={() => this.coverClick()}
          style={styles.uploadButton}
          progress={this.state.imageProcess}
        >
          {I18n.t('cryptoForm.uploadFile')}
        </UploadButton>
        <S3InputFile
          name='passportBack'
          click={click => this.coverClick = click}
          handleFinish={imageUrl => this.props.handleFinish(imageUrl)
          }
          handleProgress={imageProcess =>
            this.setState({ imageProcess })}
        />
        <View style={[styles.flexRow, styles.idCardSection]} >
          <Image
            style={{
              height: 202,
              width: 316,
            }}
            source={`${this.props.imageUrl
               || Images.photoPlaceholder}?time=${this.state.backUncacheDate}`}
          />
          <P style={styles.exampleParagraph}>
            Example
            <Icon
              name='caret-right'
              style={{
                fontSize: 12, height: '100%', paddingLeft: 5,
              }}
            />
          </P>
          <Image
            style={styles.exampleImage}
            source={Images.idCardBack}
          />
        </View>
      </View>
    )
  }
}

PassportBack.propTypes = {
  handleFinish: PropTypes.func,
  imageUrl: PropTypes.string,
  validation: PropTypes.number,
  date: PropTypes.string,
}

PassportBack.defaultProps = {
  handleFinish: () => {},
  imageUrl: null,
  validation: 0,
}

export default PassportBack
