import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import I18n from '../../../I18n/I18n'
import { FormButton, View } from '../../../Components/Html'
import styles from '../../../Containers/Styles/SettingsStyles'
import BasicInfo from './BasicInfo'
import IDInfo from './IDInfo'
import UserLevels from './UserLevels'
import { withSaveUserInfo } from '../../../GraphQl/Users/UserInfoMutation'
import { withUserInfo } from '../../../GraphQl/Users/UserInfoQuery'


class Profile extends Component {
  constructor (props) {
    super(props)
    this.state = {
      basicInfo: {
        country: null,
      },
      advancedInfo: {},
    }
  }

  _getInfo (param) {
    if (this.state.basicInfo[param] !== undefined
      && this.state.basicInfo[param] !== null) {
      return this.state.basicInfo[param]
    } else if (this.state.advancedInfo[param] !== undefined
      && this.state.advancedInfo[param] !== null) {
      return this.state.advancedInfo[param]
    } else if (this.props.user && this.props.user[param] !==
      undefined && this.props.user[param] !== null) {
      return this.props.user[param]
    } else if (this.props.user && this.props.user.advancedInfo
      !== undefined && this.props.user.advancedInfo !== null) {
      return this.props.user.advancedInfo[param]
    }
    return null
  }


  _basicInfo (params) {
    this.setState({
      basicInfo: {
        ...this.state.basicInfo,
        ...params,
      },
    }, () => {})
  }

  _advancedInfo (params) {
    this.setState({
      advancedInfo: {
        ...this.state.advancedInfo,
        ...params,
      },
    })
  }

  _formValidation () {
    if (!this._getInfo('countryId')) {
      return {
        valid: false,
        message:
        I18n.t('settings.profile.errors.countryMissing'),
      }
    } else if (!this._getInfo('firstName') || !this._getInfo('lastName')) {
      return {
        valid: false,
        message:
        I18n.t('settings.profile.errors.infoMissing'),
      }
    }
    return { valid: true }
  }

  render () {
    return (
      <View style={styles.profile.container}>
        <UserLevels />
        <BasicInfo
          user={this.props.user}
          changeParams={params => this._basicInfo(params)}
        />
        <FormButton
          onSubmit={() => this.props.saveUserInfo(
            this.state.basicInfo,
            this.state.advancedInfo,
          )}
          validateForm={() => this._formValidation()}
          successMsg={I18n.t('settings.profile.update_success')}
          style={{
            marginTop: 30,
            marginBottom: 30,
          }}
        >
          {I18n.t('settings.profile.saveBasic')}
        </FormButton>
        <IDInfo
          user={this.props.user}
          changeParams={params => this._advancedInfo(params)}
          countryId={this._getInfo('countryId')}
        />
        <FormButton
          onSubmit={() => this.props.saveUserInfo(
            this.state.basicInfo,
            this.state.advancedInfo,
          )}
          validateForm={() => this._formValidation()}
          successMsg={I18n.t('settings.profile.update_success')}
          style={{
            marginTop: 30,
            marginBottom: 30,
          }}
        >
          {I18n.t('settings.profile.saveAdvanced')}
        </FormButton>
      </View>
    )
  }
}

Profile.propTypes = {
  saveUserInfo: PropTypes.func,
  user: PropTypes.shape({
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    address: PropTypes.string,
    city: PropTypes.string,
    countryId: PropTypes.string,
    birthdate: PropTypes.number,
    validationStatus: PropTypes.number,
    terms: PropTypes.bool,
    advancedInfo: PropTypes.shape({
      validationStatus: PropTypes.number,
    }),
  }),
}

Profile.defaultProps = {
  saveUserInfo: () => {},
  user: {},
}

export default compose(
  withSaveUserInfo,
  withUserInfo,
)(Profile)
