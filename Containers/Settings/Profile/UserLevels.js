import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Icon from 'react-fontawesome'
import { withUserInfo } from '../../../GraphQl/Users/UserInfoQuery'
import I18n from '../../../I18n/I18n'
import { View, H5, H6 } from '../../../Components/Html'
import styles from '../../../Containers/Styles/SettingsStyles'
import { isMobile } from '../../../Config/AppConfig'

class UserLevels extends Component {
  render () {
    return (
      <View style={styles.profile.userLevels.container}>
        <View style={styles.profile.userLevels.levelContainer}>
          <H5 style={(this.props.user.approvalLevel === 0 ?
            styles.profile.headersHighlighted :
            styles.profile.headersSublighted)}
          >
            {isMobile ? I18n.t('settings.userLevels.basic.titleMobile') :
              I18n.t('settings.userLevels.basic.title')}
          </H5>
          <H6 style={styles.profile.userLevels.headers}>
            {this.props.user.approvalLevel >= 0
              ? I18n.t('settings.userLevels.complete')
              : I18n.t('settings.userLevels.incomplete')}
          </H6>
          <H6 style={styles.profile.userLevels.headers}>
            {I18n.t('settings.userLevels.basic.features')}
          </H6>
        </View>
        <Icon
          name='angle-double-right'
          style={styles.profile.userLevels.icon}
        />
        <View style={styles.profile.userLevels.levelContainer}>
          <H5 style={(this.props.user.approvalLevel === 1 ?
            styles.profile.headersHighlighted :
            styles.profile.headersSublighted)}
          >
            {isMobile ? I18n.t('settings.userLevels.trader.titleMobile') :
              I18n.t('settings.userLevels.trader.title')}
          </H5>
          <H6 style={styles.profile.userLevels.headers}>
            {this.props.user.approvalLevel >= 1
              ? I18n.t('settings.userLevels.complete')
              : I18n.t('settings.userLevels.incomplete')}
          </H6>
          <H6 style={styles.profile.userLevels.headers}>
            {I18n.t('settings.userLevels.trader.features')}
          </H6>
        </View>
        <Icon
          name='angle-double-right'
          style={styles.profile.userLevels.icon}
        />
        <View style={styles.profile.userLevels.levelContainer}>
          <H5 style={(this.props.user.approvalLevel === 2 ?
            styles.profile.headersHighlighted :
            styles.profile.headersSublighted)}
          >
            {isMobile ? I18n.t('settings.userLevels.unlimited.titleMobile') :
              I18n.t('settings.userLevels.unlimited.title')}
          </H5>
          <H6 style={styles.profile.userLevels.headers}>
            {this.props.user.approvalLevel >= 2
              ? I18n.t('settings.userLevels.complete')
              : I18n.t('settings.userLevels.incomplete')}
          </H6>
          <H6 style={styles.profile.userLevels.headers}>
            {I18n.t('settings.userLevels.unlimited.features')}
          </H6>
        </View>
      </View>
    )
  }
}

UserLevels.propTypes = {
  user: PropTypes.shape({
    approvalLevel: PropTypes.number,
  }),
}

UserLevels.defaultProps = {
  user: {
    approvalLevel: 0,
  },
}

export default withUserInfo(UserLevels)
