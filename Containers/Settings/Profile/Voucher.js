import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, H6, P, UploadButton, S3InputFile } from
  '../../../Components/Html'
import I18n from '../../../I18n/I18n'
import styles from '../../../Containers/Styles/SettingsStyles'


class Voucher extends Component {
  constructor (props) {
    super(props)
    this.state = {
      imageProcess: 0,
    }
  }

  /* eslint-disable class-methods-use-this */
  _voucherUploadedRender () {
    return (
      <View style={styles.flexColumn} >
        <H6 style={styles.sectionHeading}>
          {I18n.t('settings.id_info.voucher')}
        </H6>
        <P style={styles.flex}>
          {I18n.t('settings.id_info.voucher_detail')}
        </P>
        <View style={[styles.flexColumn, styles.idCardSection]}>
          <P>
            {I18n.t('settings.id_info.voucher_success')}
          </P>
        </View>
      </View>
    )
  }


  render () {
    if (this.props.imageUrl) {
      return this._voucherUploadedRender()
    }
    return (
      <View style={styles.flexColumn} >
        <H6 style={styles.sectionHeading}>
          {I18n.t('settings.id_info.voucher')}
        </H6>
        <P style={styles.flex}>
          {I18n.t('settings.id_info.voucher_detail')}
        </P>
        <UploadButton
          onPress={() => this.buttonClick()}
          style={styles.uploadButton}
          progress={this.state.imageProcess}
        >
          {I18n.t('cryptoForm.uploadFile')}
        </UploadButton>
        <S3InputFile
          name='Voucher'
          click={click => this.buttonClick = click}
          handleFinish={fileUrl => this.props.handleFinish(fileUrl)
          }
          handleProgress={imageProcess =>
            this.setState({ imageProcess })}
        />
        <View style={[styles.flexRow, styles.idCardSection]} >
          <P>
            {I18n.t('settings.id_info.voucher_description')}
          </P>
        </View>
      </View>
    )
  }
}

Voucher.propTypes = {
  handleFinish: PropTypes.func,
  imageUrl: PropTypes.string,
}

Voucher.defaultProps = {
  handleFinish: () => {},
  imageUrl: null,
}

export default Voucher
