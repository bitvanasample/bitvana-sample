import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { compose } from 'react-apollo'
import { View, H5, H6, InputWithLabel, DniInput } from
  '../../../Components/Html'
import I18n from '../../../I18n/I18n'
import styles from '../../../Containers/Styles/SettingsStyles'
import { withCountries } from '../../../GraphQl/CountriesQuery'
import PassportCover from './PassportCover'
import PassportBack from './PassportBack'
import Voucher from './Voucher'


class IDInfo extends Component {
  constructor (props) {
    super(props)
    this.state = {
      dni: null,
      secondaryDni: null,
      countryId: null,
      passportCover: null,
      voucher: null,
      passportBack: null,
      imageOneProgress: 0,
      coverUncacheDate: new Date(),
      voucherUncacheDate: new Date(),
      backUncacheDate: new Date(),
    }
  }

  _getInfo (param) {
    if (this.state[param] !== null) {
      return this.state[param]
    } else if (this.props.user && this.props.user.advancedInfo) {
      return this.props.user.advancedInfo[param]
    }
    return null
  }


  _setState (newState, callback) {
    this.setState(newState, callback)
    this.props.changeParams(newState)
    // console.log('newState')
    // console.log(this.state)
  }


  _validationMessage () {
    if (this.props.user && this.props.user.advancedInfo) {
      if (this.props.user.advancedInfo.validationStatus === 1) {
        return (
          <View>
            <H6 style={styles.profile.pendingMessage}>
              {I18n.t('settings.validationMessage.advancedPending')}
            </H6>
          </View>
        )
      } else if (this.props.user.advancedInfo.validationStatus === 2) {
        return (
          <View>
            <H6 style={styles.profile.approvedMessage}>
              {I18n.t('settings.validationMessage.advancedApproved')}
            </H6>
          </View>
        )
      } else if (this.props.user.advancedInfo.validationStatus === 3) {
        return (
          <View>
            <H6 style={styles.profile.rejectedMessage}>
              {I18n.t('settings.validationMessage.advancedRejected')}
            </H6>
          </View>
        )
      }
    }
    return null
  }


  render () {
    return (
      <View style={styles.profile.infoContainer}>
        <H5 style={styles.sectionHeading}>
          ID information
          <hr style={{ opacity: 0.5 }} />
          {this._validationMessage()}
        </H5>

        <View style={styles.flexRow} >
          <InputWithLabel
            label={I18n.t('settings.profile.dni')}
            style={{ flex: 1 }}
            customInput={
              <DniInput
                countryId={this.props.countryId}
                style={{ flex: 1 }}
                onChangeText={dni =>
                  this._setState({ dni })}
                value={this._getInfo('dni')}
              />
            }
            placeholder={I18n.t('settings.profile.dni')}
          />
          <InputWithLabel
            label={I18n.t('settings.profile.secondaryDni')}
            style={{ flex: 1 }}
            onChangeText={secondaryDni =>
              this._setState({ secondaryDni })
            }
            value={this._getInfo('secondaryDni')}
            placeholder={I18n.t('settings.profile.secondaryDni')}
          />
        </View>
        <PassportCover
          imageUrl={this._getInfo('passportCover')}
          date={this.state.coverUncacheDate}
          handleFinish={passportCover => this._setState({
            passportCover,
            coverUncacheDate: new Date(),
          })}
        />
        <PassportBack
          imageUrl={this._getInfo('passportBack')}
          date={this.state.backUncacheDate}
          handleFinish={passportBack => this._setState({
            passportBack,
            backUncacheDate: new Date(),
          })}
        />
        <Voucher
          imageUrl={this._getInfo('voucher')}
          date={this.state.voucherUncacheDate}
          handleFinish={voucher => this._setState({
            voucher,
            voucherUncacheDate: new Date(),
          })}
        />
        <View>
          <H5 style={styles.sectionHeading}>
            <hr style={{ opacity: 0.2 }} />
          </H5>
        </View>
      </View>
    )
  }
}

IDInfo.propTypes = {
  changeParams: PropTypes.func,
  user: PropTypes.shape({
    advancedInfo: PropTypes.shape({
      validationStatus: PropTypes.number,
    }),
  }),
  countryId: PropTypes.number,
}

IDInfo.defaultProps = {
  countryId: 0,
  changeParams: () => {},
  user: null,
}

export default compose(withCountries)(IDInfo)
