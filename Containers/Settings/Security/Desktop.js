import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { View } from 'react-native-web'
import styles from '../../../Containers/Styles/SettingsStyles'
import TwoFactor from './_TwoFactorAuth'
import ChangePassword from './_ChangePassword'

class Security extends Component {
  render () {
    return (
      <View style={styles.security.container}>
        <TwoFactor
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
          }}
        />
        <ChangePassword
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            padding: '0px',
            paddingLeft: 20,
            borderLeft: '1px solid rgba(151,151,151,0.2)',
          }}
        />
      </View>
    )
  }
}

Security.propTypes = {
  // submit: PropTypes.func,
}

Security.defaultProps = {
  // submit: () => {},
}

export default Security
