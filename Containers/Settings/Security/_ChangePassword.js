import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native-web'
import { Auth } from 'aws-amplify'
import {
  Button, H4,
  InputWithLabel,
} from '../../../Components/Html'
import Toast from '../../../Lib/Toast'
import I18n from '../../../I18n/I18n'
import styles from '../../Styles/SettingsStyles'

class Security extends Component {
  constructor (props) {
    super(props)
    this.state = {
      currentPassword: null,
      newPassword: null,
      confirmNewPassword: null,
      changingPassword: false,
    }
  }

  _changePassword () {
    if (this.state.changingPassword) { return null }
    if (this.state.newPassword !== this.state.confirmNewPassword) {
      return Toast.show({
        text: I18n.t('settings.security.passwordsNoEquals'),
        type: 'error',
      })
    }
    return this.setState(
      { changingPassword: true },
      () =>
        Auth.currentAuthenticatedUser()
          .then(user =>
            Auth.changePassword(
              user,
              this.state.currentPassword,
              this.state.newPassword,
            ).then(() => {
              this.setState({
                currentPassword: '',
                newPassword: '',
                confirmNewPassword: '',
                changingPassword: false,
              })
              Toast.show({ text: I18n.t('settings.security.passwordChanged') })
            }))
          .catch((err) => {
            this.setState({
              currentPassword: '',
              newPassword: '',
              confirmNewPassword: '',
              changingPassword: false,
            })
            if (err.code === 'NotAuthorizedException') {
              Toast.show({
                text: I18n.t('settings.security.current_password_error'),
                type: 'error',
              })
            } else if (err.code === 'InvalidPasswordException' ||
              err.code === 'InvalidParameterException') {
              Toast.show({
                text: I18n.t('cognito.common.invalid_pass'),
                type: 'error',
              })
            } else {
              Toast.show({ text: `Error: ${(err && err.code) || err}` })
            }
          }),
    )
  }

  render () {
    return (
      <View style={this.props.style}>
        <H4
          style={{
            marginTop: 0,
            marginBottom: 8,
          }}
        >
          {I18n.t('settings.security.change_password_title')}
        </H4>
        <View style={[styles.flexRow, { width: '100%' }]}>
          <InputWithLabel
            label={I18n.t('settings.security.current_password')}
            style={styles.flex}
            value={this.state.currentPassword}
            onChangeText={currentPassword =>
              this.setState({ currentPassword })
            }
            onKeyPress={(event) => {
              if (event.key === 'Enter') {
                this._changePassword()
              }
            }}
            placeholder={I18n.t('settings.security.current_password')}
            secureTextEntry
          />
        </View>

        <View style={[styles.flexRow, { width: '100%' }]}>
          <InputWithLabel
            label={I18n.t('settings.security.new_password')}
            style={styles.flex}
            value={this.state.newPassword}
            onChangeText={newPassword =>
              this.setState({ newPassword })
            }
            onKeyPress={(event) => {
              if (event.key === 'Enter') {
                this._changePassword()
              }
            }}
            placeholder={I18n.t('settings.security.new_password')}
            secureTextEntry
          />
        </View>
        <View style={[styles.flexRow, { width: '100%' }]}>
          <InputWithLabel
            label={I18n.t('settings.security.confirm_new_password')}
            style={styles.flex}
            value={this.state.confirmNewPassword}
            onChangeText={confirmNewPassword =>
              this.setState({ confirmNewPassword })
            }
            onKeyPress={(event) => {
              if (event.key === 'Enter') {
                this._changePassword()
              }
            }}
            placeholder={I18n.t('settings.security.confirm_new_password')}
            secureTextEntry
          />
        </View>
        <Button
          style={{ marginTop: 20 }}
          loading={this.state.changingPassword}
          onPress={() => this._changePassword()}
        >
          {I18n.t('settings.security.change_password')}
        </Button>
      </View>
    )
  }
}

Security.propTypes = {
  style: PropTypes.shape({}),
}

Security.defaultProps = {
  style: {},
}

export default Security
