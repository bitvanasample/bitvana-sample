import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View, Image } from 'react-native-web'
import { Auth } from 'aws-amplify'
import QRCode from 'qrcode.react'
import { Button, H4, H5, P, Checkbox, TextInput }
  from '../../../Components/Html'
import Toast from '../../../Lib/Toast'
import I18n from '../../../I18n/I18n'
// import styles from '../../Styles/LandingStyles'
import { Images } from '../../../Themes'

class Security extends Component {
  constructor (props) {
    super(props)
    this.state = {
      qr: '',
      codeForDesactive: '',
      codeForActivate: '',
      wantActivateTwoFactor: '',
      status: null, // activating, activated, deactivated
    }
    Auth.currentAuthenticatedUser().then(user =>
      user.getUserData((err, userData) => {
        if (userData.PreferredMfaSetting === 'SOFTWARE_TOKEN_MFA') {
          this.setState({
            status: 'activated',
          })
        } else {
          this.setState({
            status: 'deactivated',
          })
        }
      }))
  }

  _verifyTwoFactor () {
    this.setState({ loadingButton: true })
    Auth.currentAuthenticatedUser()
      .then(user =>
        Auth.verifyTotpToken(user, this.state.codeForActivate).then(() => {
          // setPreferredMFA to TOTP
          const totpMfaSettings = {
            PreferredMfa: true,
            Enabled: true,
          }
          user.setUserMfaPreference(
            null,
            totpMfaSettings,
            (err) => {
              if (err) {
                this.setState({
                  loadingButton: false,
                })
              } else {
                this.setState({
                  loadingButton: false,
                  status: 'activated',
                })
                Toast.show({ text: I18n.t('settings.security.mfa_activated') })
              }
            },
          )
        })
          .catch((err) => {
            this.setState({ loadingButton: false })
            if (err.code === 'InvalidParameterException' ||
            err.code === 'EnableSoftwareTokenMFAException') {
              Toast.show({
                text: I18n.t('cognito.common.code_error'),
                type: 'error',
              })
            } else {
              Toast.show({ text: `Error: ${(err && err.code) || err}` })
            }
          }))
  }

  _deactivateTwoFactor () {
    this.setState({ loadingButton: true })
    Auth.currentAuthenticatedUser()
      .then(user =>
        Auth.verifyTotpToken(user, this.state.codeForDesactive).then(() => {
          const totpMfaSettings = {
            PreferredMfa: false,
            Enabled: false,
          }
          user.setUserMfaPreference(
            null,
            totpMfaSettings,
            (err) => {
              if (err) {
                this.setState({
                  loadingButton: false,
                })
              }
              this.setState({
                status: 'deactivated',
                loadingButton: false,
              })
              Toast.show({ text: I18n.t('settings.security.mfa_deactivated') })
            },
          )
        }))
      .catch((err) => {
        this.setState({ loadingButton: false })
        if (err.code === 'InvalidParameterException' ||
            err.code === 'EnableSoftwareTokenMFAException') {
          Toast.show({
            text: I18n.t('cognito.common.code_error'),
            type: 'error',
          })
        } else {
          Toast.show({ text: `Error: ${(err && err.code) || err}` })
        }
      })
  }

  _createTwoFactor () {
    if (this.state.loadingButton) { return null }
    return this.setState(
      { loadingButton: true },
      () => Auth.currentAuthenticatedUser()
        .then(user =>
          Auth.setupTOTP(user).then((code) => {
            const issuer = 'Bitvana'
            const username = user.signInUserSession.idToken.payload.email
            this.setState({
              qr: `otpauth://totp/${issuer}:${username}?secret=${code}&issuer=${issuer}`,
              status: 'verifing',
              loadingButton: false,
            })
          }))
        .catch((err) => {
          this.setState({ loadingButton: false })
          Toast.show({ text: err.message })
        }),
    )
  }

  _deactivated () {
    return (
      <View style={this.props.style}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
          }}
        >
          <H4
            style={{
              marginTop: 0,
              marginBottom: 8,
            }}
          >
            {I18n.t('settings.security.mfa_title')}
          </H4>
          <Image
            style={{
              backgroundColor: 'red',
              height: 215,
              width: 215,
              opacity: '0.2',
            }}
            source={Images.qrCode}
          />
          <P style={{ textAlign: 'center' }}>
            {I18n.t('settings.security.mfa_deactivated_description')}
          </P>
          <Checkbox
            style={{ paddingBottom: 10 }}
            label={I18n.t('settings.security.deactivated_mfa')}
            checked={this.state.wantActivateTwoFactor}
            onChange={() =>
              this.setState({
                wantActivateTwoFactor: !this.state.wantActivateTwoFactor,
              })
            }
          />
          <Button
            disabled={!this.state.wantActivateTwoFactor}
            onPress={() => this._createTwoFactor()}
            loading={this.state.loadingButton}
          >
            {I18n.t('settings.security.generate_mfa')}
          </Button>
        </View>
      </View>
    )
  }

  _verifing () {
    return (
      <View
        style={this.props.style}
      >
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            margin: 5,
            padding: 10,
            backgroundColor: 'white',
          }}
        >
          <QRCode
            value={this.state.qr}
            size={200}
          />
        </View>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
          }}
        >
          <H5
            style={{
              // margin: 0,
              marginBottom: 0,
            }}
          >
            {I18n.t('settings.security.mfa_title')}
          </H5>
          <P
            style={{
              // margin: 0,
              marginBottom: 0,
            }}
          >
            {I18n.t('settings.security.mfa_verified_activate_description')}
          </P>
          <View>
            <TextInput
              onChangeText={codeForActivate =>
                this.setState({ codeForActivate })
              }
              onKeyPress={(event) => {
                if (event.key === 'Enter') {
                  this._verifyTwoFactor()
                }
              }}
              placeholder={I18n.t('settings.security.code_to_deactivate_mfa')}
            />
          </View>
          <Button
            disabled={!this.state.wantActivateTwoFactor}
            onPress={() => this._verifyTwoFactor()}
            loading={this.state.loadingButton}
          >
            {I18n.t('settings.security.verify_and_activate')}
          </Button>
        </View>
      </View>
    )
  }

  _activated () {
    return (
      <View style={this.props.style}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
          }}
        >
          <H5>{I18n.t('settings.security.mfa_title_verified_deactivate')}</H5>
          <P>
            {I18n.t('settings.security.mfa_verified_deactivate_description')}
          </P>
          <Checkbox
            label={I18n.t('settings.security.deactivate_mfa')}
            checked={this.state.wantDeactivateTwoFactor}
            onChange={() =>
              this.setState({
                wantDeactivateTwoFactor: !this.state.wantDeactivateTwoFactor,
              })
            }
          />
          <TextInput
            onChangeText={codeForDesactive =>
              this.setState({ codeForDesactive })
            }
            placeholder={I18n.t('settings.security.code_to_deactivate_mfa')}
          />
          <Button
            disabled={!this.state.wantDeactivateTwoFactor}
            onPress={() => this._deactivateTwoFactor()}
            loading={this.state.loadingButton}
          >
            {I18n.t('settings.security.verify_and_deactivate')}
          </Button>
        </View>
      </View>
    )
  }

  render () {
    if (this.state.status === 'deactivated') {
      return this._deactivated()
    } else if (this.state.status === 'activated') {
      return this._activated()
    } else if (this.state.status === 'verifing') {
      return this._verifing()
    }
    return (
      <View style={this.props.style}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            textAlign: 'center',
          }}
        >
          <H5>{I18n.t('settings.security.loading')}</H5>
        </View>
      </View>
    )
  }
}

Security.propTypes = {
  style: PropTypes.shape({}),
}

Security.defaultProps = {
  style: {},
}

export default Security
