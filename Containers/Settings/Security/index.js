import React, { Component } from 'react'
import Media from 'react-media'
import Mobile from './Mobile'
import Desktop from './Desktop'

class Security extends Component {
  render () {
    return (
      <Media query='(max-width: 599px)'>
        {isSmall => (isSmall
          ? <Mobile />
          : <Desktop />
        )}
      </Media>
    )
  }
}

export default Security
