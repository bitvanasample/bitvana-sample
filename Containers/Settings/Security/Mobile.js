import React, { Component } from 'react'
import { Tab, Tabs } from '../../../Components/Html'
import ExchangeMain from '../../ExchangeMain'
import TwoFactor from './_TwoFactorAuth'
import I18n from '../../../I18n/I18n'
import ChangePassword from './_ChangePassword'

class Security extends Component {
  render () {
    return (
      <ExchangeMain>
        <Tabs
          forceDesktopStyle
          tabs={
            [
              <Tab value={0}>
                {I18n.t('settings.security.change_password_tab')}
              </Tab>,
              <Tab value={1}>
                {I18n.t('settings.security.two_factor_tab')}
              </Tab>,
            ]
          }
          tabContent={[
            <ChangePassword
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
                padding: '0px',
                paddingLeft: 20,
                borderLeft: 'none',
              }}
            />,
            <TwoFactor
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                flexDirection: 'column',
              }}
            />,
          ]}
        />
      </ExchangeMain>
    )
  }
}

Security.propTypes = {
  // submit: PropTypes.func,
}

Security.defaultProps = {
  // submit: () => {},
}

export default Security
