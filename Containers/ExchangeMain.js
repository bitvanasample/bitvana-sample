import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { View } from 'react-native-web'
import styles from './Styles/ExchangeMainStyles'

/* eslint-disable react/jsx-indent, indent */
class ExchangeMain extends Component {
  render () {
    return (
      <View style={[styles, this.props.style]} >
        { this.props.children }
      </View>
    )
  }
}

ExchangeMain.propTypes = {
  style: PropTypes.shape({}),
  children: PropTypes.arrayOf(PropTypes.shape({})),
}

ExchangeMain.defaultProps = {
  style: {},
  children: [],
}

export default ExchangeMain
