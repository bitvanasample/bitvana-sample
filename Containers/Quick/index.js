import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import AppBarExchange from '../../Components/AppBar/Exchange'
import AppMenu from '../../Components/AppMenu/Landing'
import { View } from '../../Components/Html'
import ExchangeMain from '../ExchangeMain'
import I18n from '../../I18n/I18n'
import Market from '../Exchange/OrderForm/Market'
import CondensedForm from '../Exchange/OrderForm/CondensedForm'
import UserInfo from '../Exchange/UserInfo'
import SelectMarket from '../../Components/CryptoForm/SelectMarket'
import styles from '../Styles/QuickStyles'
import AssetBox from '../../Components/AssetsInfo/AssetBox'
import Title from '../Exchange/Title'

class Quick extends Component {
  constructor (props) {
    super(props)
    this.state = {
      market: null,
    }
  }

  render () {
    let secondaryCurrency = null
    let mainCurrency = null
    if (this.state.market && this.state.market.split) {
      [secondaryCurrency, mainCurrency] = this.state.market.split('/')
    }
    return (
      <View>
        <AppBarExchange />
        <AppMenu />
        <ExchangeMain>
          <View style={styles.container}>
            <View style={styles.box}>
              <Title text={I18n.t('quick.title')} />
              <SelectMarket
                wrapperStyle={styles.selectMarket}
                onChange={market =>
                  this.setState({ market })
                }
                size='large'
                value={this.state.market}
              />
              <View style={styles.formContainer}>
                <View style={styles.userInfoForm}>
                  <View style={styles.userInfo}>
                    <UserInfo
                      withHeader
                      secondaryCurrency={secondaryCurrency}
                      mainCurrency={mainCurrency}
                    />
                  </View>
                  <View style={styles.form}>
                    <CondensedForm
                      comp={Market}
                      secondaryCurrency={secondaryCurrency}
                      mainCurrency={mainCurrency}
                    />
                  </View>
                </View>
                <View style={styles.assetsBox}>
                  <AssetBox
                    name={`${secondaryCurrency}/${mainCurrency}`}
                    secondaryCurrency={secondaryCurrency}
                    currency={mainCurrency}
                    column
                  />
                </View>
              </View>
            </View>
          </View>
        </ExchangeMain>
      </View>
    )
  }
}

Quick.propTypes = {}

Quick.defaultProps = {}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Quick)
