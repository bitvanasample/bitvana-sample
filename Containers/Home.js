import React, { Component } from 'react'
import Media from 'react-media'
import MobileMain from './MobileMain'
import DesktopMain from './DesktopMain'

class Home extends Component {
  render () {
    return (
      <Media query='(max-width: 599px)'>
        {isSmall => (isSmall
          ? <MobileMain />
          : <DesktopMain />
        )}
      </Media>
    )
  }
}


export default Home
