import React, { Component } from 'react'
import { View } from 'react-native-web'
import styles from './Styles/ErrorStyles'
import { H1 } from '../Components/Html'
import AppBarExchange from '../Components/AppBar/Exchange'

class C404 extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
        <AppBarExchange />
        <View style={styles.container} >
          <H1>Oops, URL not found</H1>
        </View>
      </View>
    )
  }
}

C404.propTypes = {}

export default C404
