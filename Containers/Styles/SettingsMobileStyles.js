// import Colors from '../../Themes/Colors'
import { ApplicationStyles } from '../../Themes/'
// import styles from './SettingsStyles'

export default{
  ...ApplicationStyles.screen,
  tabContainer: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#262C35',
    borderTop: '1px solid rgba(151,151,151,0.3)',
    borderBottom: '1px solid rgba(151,151,151,0.3)',
    boxShadow: '2px 2px 15px #000',
    // padding: 10,
  },
  profile: {
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    headers: {
      padding: 0,
      margin: 0,
      fontSize: '1em',
    },
  },
  security: {
    container: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin: 10,
    },
  },
  idCardSection: {
    // padding: 50,
    alignItems: 'center',
  },
}
