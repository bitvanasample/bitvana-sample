import { Metrics, Fonts, ApplicationStyles, Colors } from '../../Themes/'

export default {
  ...ApplicationStyles.screen,
  ...ApplicationStyles.modal,
  background: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.background
  },
  bold: {
    fontWeight: 'bold',
    color: Colors.snow,
    marginTop: 3
  },
  listitem: {
    backgroundColor: 'transparent'
  },
  locked: {
    opacity: 0.7,
    fontSize: Fonts.size.h7,
    color: Colors.primary,
    fontWeight: 'bold',
    marginBottom: Metrics.doubleBaseMargin,
  },
  convertion: {
    opacity: 0.7,
    backgroundColor: Colors.darkBackground,
    padding: 5,
    color: Colors.primary
  },
  lowContainer: {
    // zIndex: -1, // back drawer
    height: '100%',
    flex: 1,
    overflow: 'scroll',
    backgroundColor: Colors.background
  },
  container: {
    // zIndex: -1, // back drawer
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttons: {
    flex: 1,
    alignSelf: 'stretch',
    flexDirection: 'column',
    justifyContent: 'center',
    backgroundColor: Colors.background2
  },
  info: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
    backgroundColor: Colors.background,
    paddingTop: Metrics.baseMargin,
    paddingBottom: Metrics.baseMargin
  },
  rowButtons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 10
  },
  balanceTitle: {
    fontSize: Fonts.size.h5,
    color: Colors.text
  },
  amount: {
    fontSize: Fonts.size.h1,
    color: Colors.primary,
    fontWeight: 'bold'
  },
  factor: {
    color: Colors.snow,
    fontSize: Fonts.size.h4
  },
  factor2: {
    color: Colors.snow,
    fontSize: Fonts.size.h6
  },
  lightButton: {
    color: Colors.dark
  },
  button: {
    flex: 1,
    alignSelf: 'stretch'
  },
  checkboxContainer: {
    backgroundColor: 'transparent'
  },
  checkbox: {
    // backgroundColor: 'transparent'
  },
  icon: {
    fontSize: 30,
    marginTop: 15,
    textAlign: 'center'
  },
  iconMargin: {
    fontSize: 30,
    marginTop: 30,
    textAlign: 'center'
  },
  setInputs: {
    minWidth: 256,
    marginTop: 10,
    marginLeft: 50,
    marginRight: 50
  },
  setInputsCentered: {
    minWidth: 256,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    maxWidth: "75%"
  },
  qrCode: {
    alignItems: 'center',
    marginTop: 20,
    marginBottom: 10
  },
  brandTextContainer: {
    height: '100%',
    alignItems: 'center',
    marginTop: 20
  },
  brandTextPrimary: {
    opacity: 0.8,
    textAlign: 'center'
  },
  brandTextSecundary: {
    opacity: 0.6,
    fontSize: 13,
    textAlign: 'center'
  },
  listPrimaryText: {
    fontSize: 16
  },
  listItemContainer: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  listItemTextRight: {
    color: Colors.primary,
    fontSize: 14,
    display: 'flex'
  },
  listSecondaryText: {
    display: 'flex'
  },
  footer: {
    width: '100%',
    padding: Metrics.baseMargin,
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: Colors.background2,
    paddingBottom: Metrics.doubleBasePadding
  },
  note: {
    fontSize: 12,
    color: Colors.text
  }, 
  footerNote: {
    marginLeft: Metrics.baseMargin,
    marginRight: Metrics.baseMargin,
    marginTop: Metrics.baseMargin,
    color: Colors.text
  },
  explanation: {
    textAlign: 'justify',
    padding: 20,
    color: Colors.text
  },
  blackText: {
    color: Colors.text
  },
  textCenter: {
    textAlign: 'center',
    color: Colors.text
  },
  upBalance: {
    color: Colors.success
  },
  downBalance: {
    color: Colors.error
  },
  radioButton: {
    color: Colors.text
  },
  textRadioButton: {
    color: Colors.text
  },
  form: {
    paddingRight: Metrics.basePadding
  },
  formItem: {
    borderBottomWidth: 1,
    borderColor: Colors.separator
  },
  formLabel: {
    color: Colors.text
  },
  textCheckBox: {
    color: Colors.text
  },
  textHelpCheckBox: {
    color: Colors.text,
    fontSize: Fonts.size.small
  },
  input: {
    color: Colors.inputFontColor
  },
  qListItem: {
    backgroundColor: Colors.background
  }
}
