// import Colors from '../../Themes/Colors'
import { ApplicationStyles } from '../../Themes/'
import { isMobile } from '../../Config/AppConfig'
import Colors from '../../Themes/Colors'

export default{
  ...ApplicationStyles.screen,
  tabContainer: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#262C35',
    borderTop: '1px solid rgba(151,151,151,0.3)',
    borderBottom: '1px solid rgba(151,151,151,0.3)',
    boxShadow: '2px 2px 15px #000',
    padding: 10,
  },
  profile: {
    infoContainer: {
      width: '100%',
    },
    userLevels: {
      headers: {
        ...(isMobile ?
          {
            display: 'none',
          } :
          {
            padding: 0,
            margin: 0,
          }
        ),
      },
      container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
      },
      levelContainer: {
        ...(isMobile ?
          {
            flex: 2,
            flexDirection: 'column',
            // wordWrap: 'break-word',
          } :
          {
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }

        ),
      },
      icon: {
        ...(isMobile ?
          {
            fontSize: 30,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            textAlign: 'center',
          } :
          {
            fontSize: 80,
            height: '100%',
            marginLeft: 30,
            marginRight: 30,
          }
        ),
      },
    },
    termsPre: {
      display: 'inline-block',
    },
    termsPost: {
      display: 'inline-block',
      marginLeft: 5,
    },
    pendingMessage: {
      color: '#FFEB3B',
    },
    rejectedMessage: {
      color: '#FF5722',
    },
    approvedMessage: {
      color: '#8BC34A',
    },
    container: {
      ...(isMobile ?
        {
          width: '100%',
        } :
        {
          width: '66%',
          margin: 'auto',
        }
      ),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    headers: {
      padding: 0,
      margin: 0,
    },
    headersHighlighted: {
      padding: 0,
      margin: 0,
      fontWeight: 'bold',
      fontSize: '1.4em',
      textAlign: 'center',
      color: Colors.link,
    },
    headersSublighted: {
      padding: 0,
      margin: 0,
      opacity: 0.7,
      textAlign: 'center',
    },
  },
  security: {
    container: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin: 10,
    },
  },
  idCardSection: {
    padding: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bankAccounts: {
    addBankAccount: {
      display: 'flex',
      alignItems: 'flex-end',
      marginRight: 5,
    },
  },
  uploadButton: {
    width: 200,
    backgroundColor: 'white',
    color: '#569EEA',
    border: '2px solid #569EEA',
    ...(isMobile ?
      {
        margin: 'auto',
      } :
      {

      }
    ),
  },
  exampleParagraph: {
    ...(isMobile ?
      {
        display: 'none',
      } :
      {
        padding: 20,
      }
    ),
  },
  exampleImage: {
    ...(isMobile ?
      {
        display: 'none',
      } :
      {
        height: 202,
        width: 316,
      }
    ),
  },
}

