import { ApplicationStyles } from '../../Themes/'
import styles from './LandingStyles'

export default {
  ...ApplicationStyles.screen,
  ...styles,
  subsection: {
    ...styles.subsection,
    paddingLeft: 0,
  },
  imagePreview: {
    ...styles.imagePreview,
    height: 230,
    marginRight: 0,
    marginTop: 0,
  },
  main: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  header: {
    display: 'flex',
    flexDirection: 'column',
  },
  body: {
    display: 'flex',
    flexDirection: 'column',
  },
  footer: {
    display: 'flex',
  },
  mainTitle: {
    fontSize: 22,
    textAlign: 'center',
    margin: 0,
    marginTop: 5,
  },
  btnDefault: {
    backgroundColor: '#539CED',
    borderRadius: 30,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 40,
    paddingRight: 40,
    padding: '20px 60px',
    textAlign: 'center',
    marginRight: 5,
    marginBottom: 20,
  },
  btnTransparent: {
    backgroundColor: 'transparent',
    borderRadius: 30,
    border: '1px solid white',
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 40,
    paddingRight: 40,
    padding: '20px 60px',
    textAlign: 'center',
    marginLeft: 5,
    marginBottom: 20,
  },
  mobileButtons: {
    padding: 10,
  },
}
