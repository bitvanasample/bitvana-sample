import { Fonts, Metrics, Colors } from '../../Themes/'

const isStandalone = window.navigator.standalone

export default {
  webStatusBar: {
    height: isStandalone ? 0 : Metrics.webStatusBarHeight,
    backgroundColor: Colors.primary,
  },
  applicationView: {
    height: '100%',
    width: '100%',
  },
  container: {
    display: 'flex',
    flex: 1,
    color: 'white',
    // color: 'white',
    // justifyContent: 'center',
    // alignItem: 'center',
    backgroundColor: Colors.background,
    width: '100vw',
    minHeight: '100vh',
    height: '100%',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: Fonts.type.base,
    margin: Metrics.baseMargin,
  },
  myImage: {
    width: 200,
    height: 200,
    alignSelf: 'center',
  },
  statusBar: {
    height: Metrics.webStatusBarHeight,
    width: '100%',
    backgroundColor: Colors.primary,
    position: 'absolute',
    top: 0,
  },
}
