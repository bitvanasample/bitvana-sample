import { Metrics, Colors, ApplicationStyles, Fonts } from '../../Themes/'

// const containerHeight = `cal(100vh - ${navTabs}px)`
// const containerHeight = '100%'
const containerHeight
= Metrics.screenHeight - (Metrics.navBarHeight + Metrics.tabsHeight)

export default {
  ...ApplicationStyles.screen,
  main: {
    backgroundColor: Colors.background,
  },
  container: {
    display: 'flex',
    maxHeight: containerHeight,
    height: containerHeight,
    flexDirection: 'row',
  },
  row: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    paddingTop: Metrics.section,
    paddingBottom: Metrics.section,
  },
  tab: {
    paddingTop: Metrics.section,
    maxHeight: '100%',
    height: '100%',
    width: '100%',
    display: 'flex',
    flex: 1,
  },
  col: {
    display: 'flex',
    flexDirection: 'column',
    marginRight: 'auto',
    marginLeft: 'auto',
    position: 'relative',
  },
  info: {
    marginLeft: Metrics.doubleBaseMargin,
  },
  titleContainer: {
    width: '100%',
    display: 'flex',
  },
  title: {
    marginLeft: 'auto',
    marginRight: 'auto',
    fontSize: Fonts.size.h3,
  },
  paperBody: {
    overflowY: 'scroll',
    position: 'relative',
    display: 'flex',
    flexShrink: '1 !important',
  },
  paper: {
    width: '100%',
    maxWidth: 512,
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingTop: Metrics.doubleBasePadding,
    paddingBottom: Metrics.doubleBasePadding,
  },
  paperScroll: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width: '100%',
    maxWidth: 512,
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingTop: Metrics.doubleBasePadding,
    paddingBottom: Metrics.doubleBasePadding,
    position: 'relative',
  },
  infoKey: {
    width: '100%',
    marginLeft: Metrics.doubleBaseMargin,
    fontWeight: 'bold',
    fontSize: Fonts.size.regular,
  },
  infoValue: {
    width: '100%',
    marginLeft: Metrics.doubleBaseMargin,
  },
}
