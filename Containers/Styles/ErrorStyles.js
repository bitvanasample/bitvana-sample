import { ApplicationStyles } from '../../Themes/'

export default{
  ...ApplicationStyles.screen,
  mainContainer: {
    height: '100vh',
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
}
