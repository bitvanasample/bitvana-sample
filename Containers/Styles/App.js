import { Colors } from '../../Themes/'
import DefaultStyles from '../../Components/Styles/DefaultStyles'

export default {
  muiTheme: {
    palette: {
      primary1Color: Colors.primary,
      primary2Color: Colors.secondary,
      accent1Color: Colors.accent,
    },
    overrides: {
      // This is for datepickers
      MuiInput: {
        root: {
          ...DefaultStyles.textInputContainerDatePicker,
        },
      },
      MuiInputAdornment: {
        root: {
          height: '100%',
        },
      },
      MuiIconButton: {
        root: {
          // color: Colors.snow,
          // height: 'auto',
        },
      },
      MuiIcon: {
        root: {
          height: 'auto',
        },
      },
    },
  },
}
