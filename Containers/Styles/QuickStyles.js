import { ApplicationStyles, Metrics, Colors } from '../../Themes/'
import { isMobile } from '../../Config/AppConfig'

export default{
  ...ApplicationStyles.screen,
  tabContainer: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#262C35',
    borderTop: '1px solid rgba(151,151,151,0.3)',
    borderBottom: '1px solid rgba(151,151,151,0.3)',
    boxShadow: '2px 2px 15px #000',
    padding: 10,
  },
  inputWrapper: {
    margin: 10,
    flex: 1,
  },
  userInfoForm: {
    maxWidth: 240,
    display: 'flex',
    flexDirection: 'column',
  },
  fee: {
    fontSize: 22,
    paddingLeft: 5,
  },
  noMarginBottom: {
    marginBottom: 0,
  },
  container: {
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 20,
  },
  selectMarket: {
    display: 'flex',
    flex: 1,
    width: '100%',
    padding: 10,
    maxWidth: 556,
  },
  formContainer: {
    display: 'flex',
    ...(isMobile
      ? {
        flexDirection: 'column',
        marginBottom: 20,
      }
      : {
        flexDirection: 'row',
      }
    ),
  },
  box: {
    ...(isMobile
      ? {}
      : {
        marginTop: 40,
        margin: 2,
        backgroundColor: Colors.exchangeItem,
      }
    ),
  },
  address: {
    width: '80%',
    padding: 10,
  },
  firstSection: {
    width: '80%',
  },
  headers: {
    padding: 0,
    margin: 0,
  },
  security: {
    container: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
  },
  userInfo: {
    border: '2px solid #242831',
    margin: Metrics.baseMargin,
    marginBottom: 0,
    flex: 0,
  },
  form: {
    flex: 1,
    maxWidth: 400,
    margin: Metrics.baseMargin,
  },
  assetsBox: {
    flex: 1,
    margin: Metrics.baseMargin,
  },
}
