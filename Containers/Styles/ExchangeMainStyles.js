import Colors from '../../Themes/Colors'
import { isMobile } from '../../Config/AppConfig'

export default
!isMobile
  ? {
    border: `0px solid ${Colors.border}`,
    display: 'flex',
    marginTop: 0,
    paddingTop: 0,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
    flex: 1,
  }
  : {
    border: 'none',
    display: 'flex',
    paddingTop: 0,
    paddingBottom: 0,
    flex: 1,
  }
