import { ApplicationStyles, Metrics } from '../../Themes/'

export default{
  ...ApplicationStyles.screen,
  tabContainer: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#262C35',
    borderTop: '1px solid rgba(151,151,151,0.3)',
    borderBottom: '1px solid rgba(151,151,151,0.3)',
    boxShadow: '2px 2px 15px #000',
    padding: 10,
  },
  inputWrapper: {
    margin: 10,
    flex: 1,
  },
  fee: {
    fontSize: 22,
    paddingLeft: 5,
  },
  noMarginBottom: {
    marginBottom: 0,
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 20,
  },
  address: {
    width: '80%',
    padding: 10,
  },
  firstSection: {
    width: '80%',
  },
  headers: {
    padding: 0,
    margin: 0,
  },
  security: {
    container: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
  },
  userInfo: {
    margin: Metrics.doubleBaseMargin,
    flex: 1,
  },
  userInfoMobile: {
    padding: 100,
  },
  selectMarket: {
    margin: Metrics.doubleBaseMargin,
    flex: 1,
    width: '100%',
  },
  form: {
    flex: 1,
    maxWidth: 400,
    margin: Metrics.doubleBaseMargin,
  },
  assetsBox: {
    flex: 1,
    margin: Metrics.doubleBaseMargin,
  },
}
