// import Colors from '../../Themes/Colors'
import { ApplicationStyles } from '../../Themes/'
import { isMobile } from '../../Config/AppConfig'

export default{
  ...ApplicationStyles.screen,
  bankSection: {
    display: 'flex',
    ...(isMobile ?
      {
        flexDirection: 'column',
        width: '100%',
      } :
      {
        flexDirection: 'row',
        flex: 1,
      }
    ),
  },
  verifyInfo: {
    display: 'flex',
    flexDirection: 'column',
    ...(isMobile ?
      {
        marginTop: 20,
      } :
      {

      }
    ),
  },
  verifyDialogContainer: {
    display: 'flex',
    ...(isMobile ?
      {
        flexDirection: 'column',
      } :
      {
        flexDirection: 'row',
      }
    ),
  },
  tabContainer: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#262C35',
    borderTop: '1px solid rgba(151,151,151,0.3)',
    borderBottom: '1px solid rgba(151,151,151,0.3)',
    boxShadow: '2px 2px 15px #000',
    padding: 10,
  },
  profile: {
    container: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
    },
    headers: {
      padding: 0,
      margin: 0,
    },
  },
  security: {
    container: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin: 10,
    },
  },
  idCardSection: {
    padding: 50,
    alignItems: 'center',
  },
  noMargins: {
    margin: 0,
  },
  verifyAmount: {
    marginTop: 5,
    marginLeft: 0,
    marginRight: 0,
    marginBottom: 0,
  },
}
