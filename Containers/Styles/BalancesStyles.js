import { ApplicationStyles } from '../../Themes/'
import { isMobile } from '../../Config/AppConfig'

export default{
  ...ApplicationStyles.screen,
  tabContainer: {
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: '#262C35',
    borderTop: '1px solid rgba(151,151,151,0.3)',
    borderBottom: '1px solid rgba(151,151,151,0.3)',
    boxShadow: '2px 2px 15px #000',
    padding: 40,
  },
  inlineComponent: {
    display: 'inline',
    marginRight: 3,
  },
  fiatDepositContainer: {
    ...(
      isMobile ?
        {
          width: '100%',
          padding: 10,
        }
        : {
          width: '70%',
          margin: 'auto',
        }
    ),
  },
  withdrawContainer: {
    ...(
      isMobile ?
        {
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }
        : {
          flexDirection: 'row',
        }
    ),
  },
  detailTextLeftMobile: {
    ...(
      isMobile ?
        {
          textAlign: 'left',
        }
        : {
        }
    ),
  },
  detailTextRightMobile: {
    ...(
      isMobile ?
        {
          textAlign: 'right',
        }
        : {
        }
    ),
  },
  displayWebOnly: {
    ...(
      isMobile ?
        {
          display: 'none',
        }
        : {
        }
    ),
  },
  balanceDetailContainer: {
    ...(
      isMobile ?
        {
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'space-between',
        }
        : {
          flexDirection: 'column',
        }
    ),
  },
  inputWrapper: {
    padding: 10,
    width: '100%',
  },
  noMarginBottom: {
    marginBottom: 0,
  },
  sectionContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  withdrawalSelectMethod: {
    marginBottom: 10,
  },
  deposit: {
    address: {
      fontSize: 200,
    },
  },
  fieldLabel: {
    marginBottom: 5,
    marginTop: 5,
    fontWeight: '300',
    paddingLeft: 5,
  },
  fieldResult: {
    fontSize: 18,
    marginBottom: 0,
    marginTop: 0,
    fontWeight: '300',
    paddingLeft: 5,
    paddingTop: 5,
  },
  withdrawal: {
    sendMaxAndInfo: {
      flex: 1,
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    fee: {
      margin: 0,
      textAlign: 'right',
    },
    headers: {
      padding: 0,
      margin: 0,
    },
  },
  security: {
    container: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
  },
  centerContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  balance: {
    ...(isMobile
      ? {
        fontSize: 24,
        textAlign: 'center',
      }
      : {}
    ),
  },
  balanceMobile: {
    container: {
      width: '100%',
      paddingLeft: 10,
      paddingRight: 10,
      display: 'flex',
      flexDirection: 'column',
      marginBottom: 10,
      borderTop: '1px solid rgba(151,151,151,0.3)',
      borderBottom: '1px solid rgba(151,151,151,0.3)',
    },
    headers: {
      width: '100%',
      display: 'flex',
      flex: 1,
      flexDirection: 'row',
    },
    values: {
      width: '100%',
      display: 'flex',
      flex: 1,
      flexDirection: 'row',
    },
    header: {
      display: 'flex',
      flex: 1,
      fontSize: 12,
      justifyContent: 'center',
      alignItems: 'center',
    },
    value: {
      display: 'flex',
      flex: 1,
      fontSize: 12,
      justifyContent: 'center',
      alignItems: 'center',
      opacity: 0.7,
    },
  },
}
