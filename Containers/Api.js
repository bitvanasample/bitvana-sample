import React, { Component } from 'react'
import { View } from 'react-native-web'
import { connect } from 'react-redux'
import styles from './Styles/ErrorStyles'
import { H1 } from '../Components/Html'
import AppBarLanding from '../Components/AppBar/Landing'

class Api extends Component {
  render () {
    return (
      <View style={styles.mainContainer}>
        <AppBarLanding />
        <View style={styles.container} >
          <H1>Aquí irá la documentación de la API</H1>
        </View>
      </View>
    )
  }
}

Api.propTypes = {}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(Api)
