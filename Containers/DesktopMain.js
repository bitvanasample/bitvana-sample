import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { View } from 'react-native-web'
import { connect } from 'react-redux'
import TableBalances from '../Components/Tables/TableBalances'
// import styles from './Styles/DesktopMainStyles'
import AppBarExchange from '../Components/AppBar/Exchange'

class DesktopMain extends Component {
  render () {
    return (
      <View>
        <AppBarExchange />
        <TableBalances />
      </View>
    )
  }
}

DesktopMain.propTypes = {}

DesktopMain.defaultProps = {}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(DesktopMain)
