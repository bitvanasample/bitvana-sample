import React, { Component } from 'react'
import { View } from 'react-native-web'
import { connect } from 'react-redux'
import TableMarkets from '../../Components/Tables/TableMarkets'
import { H4 } from '../../Components/Html'
import I18n from '../../I18n/I18n'
// import styles from './Styles/DesktopMarketsStyles'
import AppBarExchange from '../../Components/AppBar/Exchange'
import AppMenu from '../../Components/AppMenu/Landing'
import { isMobile } from '../../Config/AppConfig'

class DesktopMarkets extends Component {
  render () {
    return (
      <View>
        <AppBarExchange />
        <AppMenu />
        {
          !isMobile
          && (
            <H4 style={{ padding: 20, margin: 0, paddinBottom: 0 }}>
              {I18n.t('markets.title')}
            </H4>
          )
        }
        <TableMarkets />
      </View>
    )
  }
}

DesktopMarkets.defaultProps = {}

const mapStateToProps = () => ({})

const mapDispatchToProps = () => ({})

export default connect(mapStateToProps, mapDispatchToProps)(DesktopMarkets)
